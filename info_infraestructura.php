<?php 
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
?>
<!DOCTYPE html>
<?php
$id_componente = $_GET['id_componente']; //SE INSTANCIA LA CLASE DE LA BASE DATOS
require_once './controlador/Db.class.php';
$db = new Db();
$datos_sql_select_infraestructura = $db->query("SELECT *,date_format(componente_fecha_termino,'%d-%m-%Y') as componente_fecha_termino FROM "
        . "Componentes C, "
        . "EquiposComponentes EC, "
        . "Direcciones_IPs dIPs,"
        . "Servicios S,"
        . "Servicios_has_Componentes ShC WHERE "
        . "C.componente_id = '$id_componente' AND "
        . "dIPs.Componentes_componente_id = '$id_componente' AND "
        . "C.EquiposComponentes_equipos_compon_id = EC.equipos_compon_id AND "
        . "S.serv_id = Servicios_serv_id "
        . "AND ShC.Componentes_componente_id = '$id_componente'");

if ($datos_sql_select_infraestructura == false) {
    header('Location: ./error404.php');
}
foreach ($datos_sql_select_infraestructura as $row_componente):

endforeach;
session_start();
$_SESSION["IDcomponente"] = $row_componente["componente_id"];
?>
<html lang="en">    
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>

        <div id="wrapper" class="bg-white preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div class="padding-md">
                    <div class="clearfix">
                        <div class="row text-center">
                                <div class="col-md-12"><h4><strong>Información Detallada de Infraestructura</strong></h4></div>
                            </div>
                            <hr>
                            <div class="row text-center">
                                <div class="col-md-6" style="border-right: 1px solid #D2D2D2;"><h4 class="m-bottom-xs m-top-xs"><strong>Proveedor Asociado: </strong><?php echo $db->single("SELECT provee_nombre_empresa FROM Servicios S, Componentes C, Servicios_has_Componentes SHC, Proveedores P, ResponsProveeServicio RPS WHERE SHC.Componentes_componente_id = $id_componente AND SHC.Servicios_serv_id = RPS.Servicios_serv_id AND RPS.Proveedores_provee_id = P.provee_id"); ?></h4></div>
                                <div class="col-md-6"><h4 class="m-bottom-xs m-top-xs"><strong>Servicio Asociado: </strong><?php echo $db->single("SELECT serv_nombre FROM Servicios S, Componentes C, Servicios_has_Componentes SHC, Proveedores P, ResponsProveeServicio RPS WHERE SHC.Componentes_componente_id = '$id_componente' AND SHC.Servicios_serv_id = RPS.Servicios_serv_id AND RPS.Proveedores_provee_id = P.provee_id"); ?></h4></div>
                            </div>
                            <hr>
                        <div class="pull-left">
                            <!--
                            <span class="">
                                <img class="img-demo img-responsive" style="width: 150px; height: 150px;" src="img/logos_servicios/logo_entel_200x200.jpg" >
                            </span>-->
                            <div class="pull-left m-left-sm">
                                <h3 class="m-bottom-xs m-top-xs"><?php echo $row_componente["componente_nombre"]; ?> <a href="#Modal_UpdateNombreComponente" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h3>
                                <span class="text-muted"><strong>Tipo: </strong> <?php echo $row_componente["equipo_compon_nombre_tipo"]; ?></span></br>
                                <span class="text-muted"><strong>Modelo: </strong> <?php echo $row_componente["componente_modelo"]; ?> - <a href="#Modal_UpdateModeloComponente" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                <span class="text-muted"><strong>Marca: </strong> <?php echo $row_componente["componente_marca"]; ?> - <a href="#Modal_UpdateMarcaComponente" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                <span class="text-muted"><strong>N° de Serie: </strong> <?php echo $row_componente["componente_num_serie"]; ?> - <a href="#Modal_UpdateNSerieComponente" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                <span class="text-muted"><strong>N° de Parte: </strong> <?php echo $row_componente["componente_num_parte"]; ?> - <a href="#Modal_UpdateNParteComponente" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                <span class="text-muted"><strong>Sistema Operativo: </strong> <?php echo $row_componente["componente_system_operativo"]; ?> - <a href="#Modal_UpdateSOComponente" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>                                    
                                <span class="text-muted"><strong>Ubicación Física DataCenter: </strong> <?php echo $row_componente["componente_ubica_fisica_datacenter"]; ?> - <a href="#Modal_UpdateUbicaFisicaDataCenterComponente" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>                                    
                                <span class="text-muted"><strong>Ubicación Física Detalle: </strong> <?php echo $row_componente["componente_ubica_fisica_detalle"]; ?> - <a href="#Modal_UpdateUbicaFisicaDetalleComponente" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>                                    
                            </div>
                        </div>
                        <div class="pull-right">
                            <h4><strong>Fecha Termino Garantía: </strong> <?php echo $row_componente["componente_fecha_termino"]; ?> - <a href="#formModal_UpdateFechaTerminoGarantiaComponente" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h4>
                            <h3><strong>Caducidad de la Garantía en: </strong><?php
                                $fecha = $row_componente["componente_fecha_termino"];
                                $segundos = strtotime($fecha) - strtotime('now');
                                $diferencia_dias = intval($segundos / 60 / 60 / 24);
                                echo $diferencia_dias . " días";
                                ?></h3>
                            <span><strong>Ingreso del componente al Inventario: </strong>9 de Enero del 2015 - No BD</span>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix">
                        <div class="row">
                            <div class="col-md-4"> 
                                <h5>Aplicaciones</h5>
                                <span class="text-muted"><strong></strong><p align="justify"> <?php echo $row_componente["componente_aplicaciones"]; ?> - <a href="#Modal_UpdateAplicaciones" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></p></span></br>
                            </div>
                            <div class="col-md-4"> 
                                <h5>Especificaciones Técnicas</h5>
                                <span class="text-muted"><strong></strong> <?php echo $row_componente["componente_espf_tecnicas"]; ?> - <a href="#Modal_UpdateEspfTecnicas" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                            </div>
                            <div class="col-md-4"> 
                                <h5>Especificaciones de Energía</h5>
                                <span class="text-muted"><strong></strong> <?php echo $row_componente["componentes_espf_energia"]; ?> - <a href="#Modal_UpdateEspfEnergia" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                            </div>                                
                        </div>
                        <div class="row">
                            <div class="col-md-4"> 
                                <h5>Servicios Asociados - <a href="#Modal_UpdateServiciosAsociados" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                <span class="text-muted"><strong></strong><p align="justify"> <?php
                                        $servicios_asociados = $db->query("SELECT DISTINCT serv_nombre,serv_id FROM Servicios_has_Componentes ShC, Servicios S, Componentes C WHERE "
                                                . "ShC.Componentes_componente_id = '" . $row_componente["componente_id"] . "' AND "
                                                . "ShC.Servicios_serv_id = S.serv_id");
                                        
                                        if ($servicios_asociados) {
                                            foreach ($servicios_asociados as $row_serv_asoci):
                                                echo $row_serv_asoci["serv_nombre"] . "</br>";
                                            endforeach;
                                        }else {
                                            echo "Sin Servicio Asociado";
                                        }
                                        //echo $row_componente["componente_aplicaciones"];
                                        ?>
                                    </p></span>
                                <a href="#Modal_AsociarOtroServicio" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-plus-circle"></i> Asociar Otro</a>
                            </div>
                                    <div class="col-md-4"> 
                                        <h5>IP Asociadas - <a href="#formModal_UpdateIPAsociadas" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br></h5>
                                        <span class="text-muted"><strong></strong> <?php
                                            $ips = $db->query("SELECT DISTINCT direcc_ips_num,direcc_ips_id FROM Direcciones_IPs WHERE Componentes_componente_id = '" . $row_componente["componente_id"] . "'");
                                            if ($ips) {
                                                foreach ($ips as $row_ips):
                                                    echo $row_ips["direcc_ips_num"] . "</br>";
                                                endforeach;
                                            }else {
                                                echo "Sin IP Asociada";
                                            }
                                            //echo $row_componente["componente_espf_tecnicas"];                                     
                                            ?> 
                                            </p></span>
                                    </div>
                                    <div class="col-md-4"> 
                                        <h5>Usuarios y Contraseñas - <a href="#Modal_UpdateUsuariosContraseñas" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                        <span class="text-muted"><strong></strong><p align="justify"> <?php echo $row_componente["componente_usuario_contraseñas"]; ?> </p></span></br>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4"> 
                                    <h5>Etiquetado Físico</h5>
                                    <span class="text-muted"><strong></strong><p align="justify"> <?php echo $row_componente["componente_etique_fisico"]; ?> - <a href="#Modal_UpdateEtiquetaFisico" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></p></span></br>
                                </div>
                                <div class="col-md-4"> 
                                    <h5>Garantía Proveedor</h5>
                                    <span class="text-muted"><strong></strong> <?php echo $row_componente["componente_garantia_provee"]; ?> - <a href="#Modal_UpdateGarantiaProvee" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                </div>
                                <div class="col-md-4"> 
                                    <h5>Garantía Detalle</h5>
                                    <span class="text-muted"><strong></strong> <?php echo $row_componente["componente_garantia_detalle"]; ?> - <a href="#Modal_UpdateGarantiaDetalle" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12"> 
                                    <h4>Diagrama Red - <a href="#Modal_UpdateDiagramaRed" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h4>
                                    <div class="" align="center"><img src="<?php echo $row_componente["componente_url_img_diagrama_red"]; ?>" class="img-responsive border"/></div>                                    
                                </div>                            
                            </div>
                        </div>
                    </div><!-- /.padding20 -->
                </div><!-- /main-container -->
            </div><!-- /wrapper -->

            <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

            <!-- Logout confirmation -->
            <?php require_once './includes/confirmation.logout.inc.php'; ?> 
            <!-- ######################################## -->
            <!-- MODAL'S -->
            <!-- ########################################################### -->
            <form id="form_update_NombreComponente" method="POST">
                <div class="modal fade" id="Modal_UpdateNombreComponente">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="servicio_nombre">Nombre del Componente</label>
                                            <input type="text" id="componente_nombre_update" name="componente_nombre_update" value="<?php echo $row_componente["componente_nombre"]; ?>" placeholder="" class="form-control input-sm" required="required">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_ModeloComponente" method="POST">
                <div class="modal fade" id="Modal_UpdateModeloComponente">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Alias del Componente</label>
                                            <input type="text" id="componente_modelo_update" name="componente_modelo_update" value="<?php echo $row_componente["componente_modelo"]; ?>" placeholder="" class="form-control input-sm">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_MarcaComponente" method="POST">
                <div class="modal fade" id="Modal_UpdateMarcaComponente">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" id="componente_marca_update" name="componente_marca_update" value="<?php echo $row_componente["componente_marca"]; ?>" placeholder="" class="form-control input-sm">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_NSerieComponente" method="POST">
                <div class="modal fade" id="Modal_UpdateNSerieComponente">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" id="componente_NSerie_update" name="componente_NSerie_update" value="<?php echo $row_componente["componente_num_serie"]; ?>" class="datepicker form-control input-sm" placeholder="">
                                            <div class="input-group">
                                                <input type="text" value="" id="infra_fecha_termino_garantia" name="infra_fecha_termino_garantia" class="form-control input-sm" placeholder="">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_NParteComponente" method="POST">
                <div class="modal fade" id="Modal_UpdateNParteComponente">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input id="componente_NParte_update" name="componente_NParte_update" value="<?php echo $row_componente["componente_num_parte"]; ?>" class="form-control input-sm" type="text" placeholder="ej: 2 Semanas, 3 Semanas ó más."/>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_Modal_UbicaFisicaDataCenter" method="POST">
                <div class="modal fade" id="Modal_UpdateUbicaFisicaDataCenterComponente">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <!--<input type="text" id="componente_Ubica_Fisica_Datacenter_update" name="componente_Ubica_Fisica_Datacenter_update" value="<?php echo $row_componente["componente_ubica_fisica_datacenter"]; ?>" class="datepicker form-control input-sm" placeholder="Fecha Oficial de Inicio en Operaciones">-->
                                            <select class="form-control input-sm" id="infra_ubica_fisica_datacenter" name="infra_ubica_fisica_datacenter" required="required">
                                                <option value=""> Seleccionada: <?php echo $row_componente["componente_ubica_fisica_datacenter"]; ?></option>
                                                <option value="CDLV Sala 1.1.1">CDLV Sala 1.1.1</option>
                                                <option value="CDLV Sala 1.2.1">CDLV Sala 1.2.1</option>
                                                <option value="CDLV Sala 2.1.4">CDLV Sala 2.1.4</option>
                                                <option value="CNT Sala Will">CNT Sala Will</option>
                                                <option value="CNT Sala Telrad">CNT Sala Telrad</option>
                                                <option value="CNT Sala MPLS">CNT Sala MPLS</option>
                                                <option value="CNT Sala Internet Sur">CNT Sala Internet Sur</option>
                                            </select>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_UbicaFisicaDetalle" method="POST">
                <div class="modal fade" id="Modal_UpdateUbicaFisicaDetalleComponente">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" id="componente_UbicaFisicaDetalle_update" name="componente_UbicaFisicaDetalle_update" value="<?php echo $row_componente["componente_ubica_fisica_detalle"]; ?>" class="datepicker form-control input-sm" placeholder="Fecha Oficial de Inicio en Operaciones">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_FechaTerminoGarantia" method="POST">
                <div class="modal fade" id="formModal_UpdateFechaTerminoGarantiaComponente">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" value="<?php echo $row_componente["componente_fecha_termino"]; ?>" id="componente_FechaTerminoGarantia_update" name="componente_FechaTerminoGarantia_update" class="form-control input-sm " placeholder="">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_Aplicaciones" method="POST">
                <div class="modal fade" id="Modal_UpdateAplicaciones">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" id="componente_Aplicaciones_update" name="componente_Aplicaciones_update" value="<?php echo $row_componente["componente_aplicaciones"]; ?>" class="datepicker form-control input-sm" placeholder="">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_EspfTecnicas" method="POST">
                <div class="modal fade" id="Modal_UpdateEspfTecnicas">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" id="componente_EpsfTecnicas_update" name="componente_EpsfTecnicas_update" value="<?php echo $row_componente["componente_espf_tecnicas"]; ?>" class="datepicker form-control input-sm" placeholder="">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_EspfEnergia" method="POST">
                <div class="modal fade" id="Modal_UpdateEspfEnergia">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" id="componente_EpsfEnergia_update" name="componente_EpsfEnergia_update" value="<?php echo $row_componente["componentes_espf_energia"]; ?>" class="form-control input-sm" placeholder="">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### SERVICIOS -->
            <form id="form_update_ServiciosAsociados" method="POST">
                <div class="modal fade" id="Modal_UpdateServiciosAsociados">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <?php
                                    foreach ($servicios_asociados as $row_serv_asoci):
                                        $resultado_serv_asoci = count($servicios_asociados);
                                        $_SESSION["resultado_serv_asoci"] = $resultado_serv_asoci;
                                        while ($num_serv_asoci <= $resultado_serv_asoci) {
                                            $summa_serv_asoci = $suma_serv_asoci + $num_serv_asoci;
                                            $num_serv_asoci = $num_serv_asoci + 1;
                                            echo '<div class="col-md-4">'
                                            . '<div class="form-group">';
                                            echo '<h4>' . $num_serv_asoci.".- ".$row_serv_asoci["serv_nombre"] . '</h4>';
                                            echo '<input class="hidden" type="text" id="componente_serv_asociado'.$num_serv_asoci.'" name="componente_serv_asociado'.$num_serv_asoci.'" value="'.$row_serv_asoci["serv_id"].'">';
                                            echo '</div>'
                                            . '</div>';

                                            echo '<div class="col-md-4">'
                                            . '<div class="form-group">';
                                            echo '<h5>Cambiar por</h5>';
                                            echo '</div>'
                                            . '</div>';

                                            echo '<div class="col-md-4">'
                                            . '<div class="form-group">';
                                            echo'<select class="form-control input-sm" id="infra_select_servicio_componente_update' . $num_serv_asoci . '" name="infra_select_servicio_componente_update' . $num_serv_asoci . '">';
                                                echo "<option value='".$row_serv_asoci["serv_id"]."'>Seleccione Servicio</option>";
                                            $datos_sql_select_servicios = $db->query("SELECT serv_id,serv_nombre FROM Servicios");
                                            foreach ($datos_sql_select_servicios as $row):
                                                echo "<option value=" . $row["serv_id"] . ">" . $row["serv_id"].$row["serv_nombre"] . "</option>";
                                            endforeach;
                                            echo'</select>';
                                            echo '</div>'
                                            . '</div>';
                                            break;
                                        }
                                    endforeach;
                                    ?>                                
                                </div>
                            </div>
                            <div class="modal-footer ">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### IP ASOCIADAS -->
            <form id="form_update_IPAsociadas" method="POST">
                <div class="modal fade" id="formModal_UpdateIPAsociadas">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?php
                                            foreach ($ips as $row_ips):
                                                $resultado_ips = count($ips);
                                                $_SESSION["resultado_ips"] = $resultado_ips;
                                                if ($ips) {
                                                    while ($num_ips <= $resultado_ips) {
                                                        $summa_ips = $suma_ips + $num_ips;
                                                        $num_ips = $num_ips + 1;
                                                        echo '<input class="hidden" type="text" id="componente_ips'.$num_ips.'" name="componente_ips'.$num_ips.'" value="'.$row_ips["direcc_ips_id"].'">';
                                                        echo '<label class="control-label">IP '.$num_ips.'</label>'
                                                        . '<input type="text" id="componente_IpAsociadas_update' . $num_ips . '" name="componente_IpAsociadas_update' . $num_ips . '" value="' . $row_ips["direcc_ips_num"] . '" class="datepicker form-control input-sm" placeholder="">';
                                                        break;
                                                    }
                                                } else {
                                                    echo "Sin IP Asociada";
                                                }
                                            endforeach;
                                            ?>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_AsociarOtroServicio" method="POST">
                <div class="modal fade" id="Modal_AsociarOtroServicio">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Servicio</label>
                                            <select class="form-control input-sm" id="infra_select_sAsociarOtroServicioComponente" name="infra_select_sAsociarOtroServicioComponente" required="required">
                                                <option value=""> Seleccione Servicio</option>
                                                <?php
                                                $datos_sql_select_servicios = $db->query("SELECT serv_id,serv_nombre FROM Servicios");
                                                foreach ($datos_sql_select_servicios as $row):
                                                    echo "<option value=" . $row["serv_id"] . ">" . $row["serv_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>            
            <!-- ########################################################### -->
            <form id="form_update_UsuariosContraseñas" method="POST">
                <div class="modal fade" id="Modal_UpdateUsuariosContraseñas">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control input-sm" rows="3" id="componente_UsuariosContraseñas_update" name="componente_UsuariosContraseñas_update" placeholder=""><?php echo $row_componente["componente_usuario_contraseñas"]; ?></textarea>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_EtiquetaFisico" method="POST">
                <div class="modal fade" id="Modal_UpdateEtiquetaFisico">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" id="componente_EtiquetaFisico_update" name="componente_EtiquetaFisico_update" value="<?php echo $row_componente["componente_etique_fisico"]; ?>" class="datepicker form-control input-sm" placeholder="">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_GarantiaProvee" method="POST">
                <div class="modal fade" id="Modal_UpdateGarantiaProvee">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" id="componente_GarantiaProvee_update" name="componente_GarantiaProvee_update" value="<?php echo $row_componente["componente_garantia_provee"]; ?>" class="datepicker form-control input-sm" placeholder="">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_GarantiaDetalle" method="POST">
                <div class="modal fade" id="Modal_UpdateGarantiaDetalle">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" id="componente_GarantiaDetalle_update" name="componente_GarantiaDetalle_update" value="<?php echo $row_componente["componente_garantia_detalle"]; ?>" class="form-control input-sm" placeholder="">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_DiagramaRed" method="POST">
                <div class="modal fade" id="Modal_UpdateDiagramaRed">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="file" id="archivos_img_infraestructura">
                                            <div class="mensage"></div>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <!-- Le javascript -->
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="bootstrap/js/bootstrap.min.js"></script>
            <script type="text/javascript">
            $(document).ready(function () {
            //Iniciamos nuestra función jquery.
            $(function () {
                $('#enviar').click(SubirFotos); //Capturamos el evento click sobre el boton con el id=enviar	y ejecutamos la función seleccionado.
            });

            function SubirFotos() {
                var archivos_img_infraestructura = document.getElementById("archivos_img_infraestructura");//Creamos un objeto con el elemento que contiene los archivos_img_infraestructura: el campo input file, que tiene el id = 'archivos_img_infraestructura'
                var archivo = archivos_img_infraestructura.files; //Obtenemos los archivos_img_infraestructura seleccionados en el imput
                //Creamos una instancia del Objeto FormDara.
                var archivos_img_infraestructura = new FormData();
                /* Como son multiples archivos_img_infraestructura creamos un ciclo for que recorra la el arreglo de los archivos_img_infraestructura seleccionados en el input
                 Este y añadimos cada elemento al formulario FormData en forma de arreglo, utilizando la variable i (autoincremental) como 
                 indice para cada archivo, si no hacemos esto, los valores del arreglo se sobre escriben*/
                for (i = 0; i < archivo.length; i++) {
                    archivos_img_infraestructura.append('archivos_img_infraestructura' + i, archivo[i]); //Añadimos cada archivo a el arreglo con un indice direfente
                }

                /*Ejecutamos la función ajax de jQuery*/
                $.ajax({
                    url: 'modelo/img_update_diagrama_red.php', //Url a donde la enviaremos
                    type: 'POST', //Metodo que usaremos
                    contentType: false, //Debe estar en false para que pase el objeto sin procesar
                    data: archivos_img_infraestructura, //Le pasamos el objeto que creamos con los archivos_img_infraestructura
                    processData: false, //Debe estar en false para que JQuery no procese los datos a enviar
                    cache: false //Para que el formulario no guarde cache
                }).done(function (msg) {//Escuchamos la respuesta y capturamos el mensaje msg
                    MensajeFinal(msg);
                });
            }

            function MensajeFinal(msg) {
                $('.mensage').html(msg);//A el div con la clase msg, le insertamos el mensaje en formato  thml
                $('.mensage').show('slow');//Mostramos el div.
            }
            });
        </script>
            <script src="js/jquery.validate.min.js"></script>
            <script src="js/custom_jquery_validate.js"></script>
            <script>
                $(document).ready(function () {
                    $("textarea").keydown(function (e) {
                            if (e.keyCode === 9) { // tab was pressed
                                // get caret position/selection
                                var start = this.selectionStart;
                                end = this.selectionEnd;

                                var $this = $(this);

                                // set textarea value to: text before caret + tab + text after caret
                                $this.val($this.val().substring(0, start) + "\t" + $this.val().substring(end));

                                // put caret at right position again
                                this.selectionStart = this.selectionEnd = start + 1;

                                // prevent the focus lose
                                return false;
                            }
                        });
                });
            </script>
            <script src='js/datepicker_js/bootstrap-datepicker.js'></script>
            <script src='js/datepicker_js/locales/bootstrap-datepicker.es.js'></script>
            <script>
                $(document).ready(function () {
                    $('#componente_FechaTerminoGarantia_update').datepicker({
                        language: "es",
                        format: "dd-mm-yyyy",
                        autoclose: true,
                        todayHighlight: true
                    });
                });
            </script>
            <script src="js/menu-active-class.js"></script>
            <script src='js/modernizr.min.js'></script>
            <script src='js/pace.min.js'></script>
            <script src='js/jquery.popupoverlay.min.js'></script>
            <script src='js/jquery.slimscroll.min.js'></script>
            <script src='js/jquery.cookie.min.js'></script>
            <script src="js/endless/endless.js"></script>
            <script>
                $(function () {
                    $('#invoicePrint').click(function () {
                        window.print();
                    });
                });
            </script>
    </body>
</html>
