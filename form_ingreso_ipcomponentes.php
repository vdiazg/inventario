<?php 
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
?>
<?php
//SE INSTANCIA LA CLASE DE LA BASE DATOS
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<!-- ######################################## -->
		<?php require_once './includes/head.inc.php'; ?> 
		<!-- ######################################## -->
	</head>
	<body class="overflow-hidden">
		<!-- Overlay Div -->
		<div id="overlay" class="transparent"></div>
		<div id="wrapper" class="preload">
			<!-- ######################################## -->
			<?php require_once './includes/header.inc.php'; ?>
			<!-- ######################################## -->
			<?php require_once './includes/menubar.inc.php'; ?>
			<!-- ######################################## -->
			<div id="main-container">
				<div id="breadcrumb">
					<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="index.php"> Home</a></li>
						<li class="active">Ingreso de Infraestructura</li>	 
					</ul>
				</div><!-- breadcrumb -->
				</br>
				<div class="col-md-12">
					<div class="panel panel-default">
						<form class="no-margin" id="form_ingreso_ip_componentes">
							<div class="panel-heading">
								<h4>Ingreso de Componentes</h4>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">Seleccione Componente</label>
											<select class="form-control input-sm" id="infra_select_ip_componente" name="infra_select_ip_componente" required="required">
												<option value=""> Seleccione Componente</option>
												<?php
												$datos_sql_select_componente = $db->query("SELECT * FROM Componentes C, EquiposComponentes EC WHERE C.EquiposComponentes_equipos_compon_id = EC.equipos_compon_id");
												foreach ($datos_sql_select_componente as $row):
													echo "<option value=" . $row["componente_id"] . ">" . $row["componente_nombre"] . " => ".$row["equipo_compon_nombre_tipo"]."</option>";
												endforeach
												?>
											</select>
										</div><!-- /form-group -->
									</div>                                    
								</div>
								<hr>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">Dirección IPV4</label>
											<input type="text" placeholder="" id="infra_num_ip_componente" name="infra_num_ip_componente" class="form-control input-sm" >
										</div><!-- /form-group -->
									</div>
									<div class="col-md-8">
										<div class="form-group">
											<label class="control-label">Plan IP</label>
											<textarea class="form-control input-sm" id="infra_plan_ip_componente" name="infra_plan_ip_componente" rows="3" placeholder="" maxlength="141" rows="3"></textarea>
										</div><!-- /form-group -->
									</div>
								</div>
							</div>
							<div class="panel-footer">
									<div class="row">                                        
										<div class="col-md-8 text-left">
											<div id="msj_respuesta_ingreso_IP"></div>
										</div>
										<div class="col-md-4 text-right">
											<button type="submit" class="btn btn-info">Guardar</button>
										</div>
									</div>
								</div>
						</form>
					</div><!-- /panel -->
				</div><!-- /.col-->
			</div><!-- /main-container -->
		</div><!-- /wrapper -->
		<a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
		<!-- Logout confirmation -->
		<?php require_once './includes/confirmation.logout.inc.php'; ?>
		<!-- Le javascript-->
		<script src="js/jquery-1.10.2.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>      
		<script src="js/jquery.validate.min.js"></script>
		<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
		<script src="js/custom_jquery_validate.js"></script>
                <script>
                    $(document).ready(function () {
                        $("textarea").keydown(function (e) {
                                if (e.keyCode === 9) { // tab was pressed
                                    // get caret position/selection
                                    var start = this.selectionStart;
                                    end = this.selectionEnd;

                                    var $this = $(this);

                                    // set textarea value to: text before caret + tab + text after caret
                                    $this.val($this.val().substring(0, start) + "\t" + $this.val().substring(end));

                                    // put caret at right position again
                                    this.selectionStart = this.selectionEnd = start + 1;

                                    // prevent the focus lose
                                    return false;
                                }
                            });
                    });
                </script>
		<script src="js/menu-active-class.js"></script>
		<script src='js/chosen.jquery.min.js'></script>
		<script src='js/jquery.maskedinput.min.js'></script>
		<script src='js/bootstrap-datepicker.min.js'></script>
		<script src='js/bootstrap-timepicker.min.js'></script>
		<script src='js/bootstrap-slider.min.js'></script>
		<script src='js/jquery.tagsinput.min.js'></script>
		<script src='js/wysihtml5-0.3.0.min.js'></script>	
		<script src='js/uncompressed/bootstrap-wysihtml5.js'></script>
		<script src='js/dropzone.min.js'></script>
		<script src='js/modernizr.min.js'></script>
		<script src='js/pace.min.js'></script>
		<script src='js/jquery.popupoverlay.min.js'></script>
		<script src='js/jquery.slimscroll.min.js'></script>
		<script src='js/jquery.cookie.min.js'></script>
		<script src="js/endless/endless_form.js"></script>
		<script src="js/endless/endless.js"></script>

	</body>
</html>
