/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */

// Para sobreescribir los mensajes de error de jquery-validation-plugin
jQuery.extend(jQuery.validator.messages, {
    required: "Debe completar este campo.", 
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Por favor, introduzca una fecha válida.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Debe ingresar un número válido.",
    digits: "Debe ingresar números enteros (dí­gitos).",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Debe ingresar la misma contraseña nuevamente.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Debe ingresar un valor con un largo entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Debe ingresar un valor menor o igual a {0}."),
    min: jQuery.validator.format("Debe ingresar un valor mayor o igual a {0}.")
});

// Para validar un rut Chileno con Jquery-Validate
function validaRut(campo) {
    if (campo.length == 0) {
        return false;
    }
    if (campo.length < 8) {
        return false;
    }

    // Chequea que tenga el guion antes del digito verificador
    if (campo.charAt(campo.length - 2) != '-') {
        return false;
    }

    // Chequea que no tenga puntos (esto se puede quitar si deseas que el rut contenga puntos)
    if (campo.indexOf('.') != -1) {
        return false;
    }

    campo = campo.replace('-', '')
    campo = campo.replace(/\./g, '')

    var suma = 0;
    var caracteres = "1234567890kK";
    var contador = 0;
    for (var i = 0; i < campo.length; i++) {
        u = campo.substring(i, i + 1);
        if (caracteres.indexOf(u) != -1)
            contador++;
    }
    if (contador == 0) {
        return false
    }

    var rut = campo.substring(0, campo.length - 1)
    var drut = campo.substring(campo.length - 1)
    var dvr = '0';
    var mul = 2;

    for (i = rut.length - 1; i >= 0; i--) {
        suma = suma + rut.charAt(i) * mul
        if (mul == 7)
            mul = 2
        else
            mul++
    }
    res = suma % 11
    if (res == 1)
        dvr = 'k'
    else if (res == 0)
        dvr = '0'
    else {
        dvi = 11 - res
        dvr = dvi + ""
    }
    if (dvr != drut.toLowerCase()) {
        return false;
    }
    else {
        return true;
    }
}

$.validator.addMethod("rut_required", function (value, element) {
    return this.optional(element) || validaRut(value);
}, "El rut ingresado es inválido.");

$.validator.addMethod("letras_espacios_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-ZáéíóúñÁÉÍÓÚ ]+" + "$"));
}, "En este campo sólo se permiten letras y espacios.");

$.validator.addMethod("letras_numeros_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-z0-9]+" + "$"));
}, "En este campo sólo se permiten letras minúsculas y números.");

$.validator.addMethod("letras_numeros_espacios_guion_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-Z- 0-9]+" + "$"));
}, "En este campo sólo se permiten letras mayusculas, minusculas, guiones y números.");

$.validator.addMethod("letras_espacios_comas_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-Z, ]+" + "$"));
}, "En este campo sólo se permiten letras mayusculas, minusculas y comas.");

$.validator.addMethod("dos_decimales_required", function (value, element) {
    return value.match(new RegExp("^([0-9]+)|([0-9]+\.([0-9]|[0-9][0-9]))$"));
}, "En este campo debe ingresar un número con 1 o 2 decimales.");

$.validator.addMethod("letras_espacios_puntos_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-ZáéíóúñÁÉÍÓÚ .]+" + "$"));
});

$.validator.addMethod("validacion_email", function (value, element) {
    return value.match(new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i));
}, "Correo eletrónico es inválido.");

$.validator.addMethod("numeros_letras_guion", function (value, element) {
    return value.match(new RegExp(/^[0-9-]{1,10}$/));
});

$.validator.addMethod("letras_numeros_required", function (value, element) {
    return value.match(new RegExp("^" + "[0-9]+" + "$"));
}, "En este campo sólo se permiten números.");

$.validator.addMethod('IP4Checker', function (value) {
    return value.match(new RegExp(/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/));
}, 'Dirección IP Inválida');

$.validator.addMethod('url_validation', function (value) {
    return value.match(new RegExp(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \?=.-]*)*\/?$/));
}, 'URL Inválida');

$.validator.addMethod('numero_celular_chileno', function (value) {
    return value.match(new RegExp("^[+][0-9]{3,3}-? ?[0-9]{8,8}$"));
}, 'Número inválido, Formato Correcto: +56912345678');

$(document).ready(function () {
    //#######################################################################
    /* FORMULARIO QUE INGRESA LOS PROVEEDORES */
    $("#form_ingreso_proveedores").validate({
        rules: {
            'proveedor_nombre_empresa': {required: true, letras_espacios_required: true},
            'proveedor_rut': {required: true, rut_required: true},
            'proveedor_alias_fantasia': {required: true, letras_numeros_espacios_guion_required: true},
            'proveedor_url_sitio_web': {required: false, url_validation: true},
            'proveedor_direccion': {required: false},
            'proveedor_n_mesa_ayuda': {required: false},
            'proveedor_documento_oficial': {required: false},
            'proveedor_nota': {required: false},
            'proveedor_img': {required: false}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'proveedor_nombre_empresa': {required: "Campo obligatorio."},
            'proveedor_rut': {required: "Campo obligatorio."},
            'proveedor_alias_fantasia': {required: "Campo obligatorio."},
            'proveedor_url_sitio_web': {required: "Campo obligatorio."},
            'proveedor_direccion': {required: "Campo obligatorio."},
            'proveedor_n_mesa_ayuda': {required: "Campo obligatorio."},
            'proveedor_documento_oficial': {required: "Campo obligatorio."},
            'proveedor_nota': {required: "Campo obligatorio."},
            'proveedor_img': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_provee').html('<img src="img/ajax-loader.gif" alt="" />');
            var archivos_img = document.getElementById("proveedor_documento_oficial");//Creamos un objeto con el elemento que contiene los archivos_img: el campo input file, que tiene el id = 'archivos_img'
            var archivo = archivos_img.files;
            var archivos_img = new FormData();
            for (i = 0; i < archivo.length; i++) {
                archivos_img.append('proveedor_documento_oficial' + i, archivo[i]); //Añadimos cada archivo a el arreglo con un indice direfente
            }
            //"nombre=" + $('#ContactName').val() + "&email=" + $('#ContactRecipient').val() +
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_proveedor_BD.php",
                data: $(form).serialize(),
                success: function (data) {
                    
                    $.ajax({
                        type: "POST",
                        url: "./modelo/up_contrato_proveedor.php",
                        data: archivos_img,
                        contentType: false,
                        processData: false,
                        cache: false,
                        success: function (data_img) {
                            //alert(data_img);
                        }
                    });
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_provee').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Proveedor registrado exitosamente!</strong></div>');
                        $('#form_ingreso_proveedores')[0].reset();
                        $('#form_ingreso_proveedores').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_provee').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Proveedor ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_provee').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar proveedor!</strong></div>');
                    }                    
                    //alert(data);

                }
            });
        }
    });
    //#######################################################################
    /* FORMULARIO QUE INGRESA LOS ADMINISTRATIVOS */
    $("#form_ingreso_administrativos").validate({
        rules: {
            'selec_proveedor_admninistrativos': {required: true},
            'proveedor_nombre_administrativo': {required: true, letras_espacios_puntos_required:true},
            'proveedor_cargo_administrativo': {required: true, letras_espacios_required:true},
            'proveedor_email_administrativo': {required: true,validacion_email:true},
            'proveedor_tel_movil_administrativo': {required: false, numero_celular_chileno:false},
            'proveedor_tel_fijo_administrativo': {required: false}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'selec_proveedor_admninistrativos': {required: "Campo obligatorio."},
            'proveedor_nombre_administrativo': {required: "Campo obligatorio.", letras_espacios_puntos_required:"En este campo sólo se aceptan letras, espacios y puntos."},
            'proveedor_cargo_administrativo': {required: "Campo obligatorio."},
            'proveedor_email_administrativo': {required: "Campo obligatorio."},
            'proveedor_tel_movil_administrativo': {required: "Campo obligatorio."},
            'proveedor_tel_fijo_administrativo': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_administrativo').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_administrativos_BD.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_ingreso_administrativo').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_administrativo').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Administrativo registrado exitosamente!</strong></div>');
                        $('#form_ingreso_administrativos')[0].reset();
                        $('#form_ingreso_administrativos').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_administrativo').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Administrativo ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_administrativo').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar Administrativo!</strong></div>');
                    }
                }

            });
        }
    });
    //#######################################################################
    /* FORMULARIO QUE INGRESA LOS TECNICOS */
    $("#form_ingreso_tecnicos").validate({
        rules: {
            'selec_proveedor_tecnicos': {required: true},
            'proveedor_nombre_tecnico': {required: true, letras_espacios_required:true},
            'proveedor_cargo_tecnico': {required: true, letras_espacios_required:true},
            'proveedor_email_tecnico': {required: true, validacion_email:true},
            'proveedor_tel_movil_tecnico': {required: false, numero_celular_chileno:false},
            'proveedor_tel_fijo_tecnico': {required: false, letras_numeros_required:false}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'selec_proveedor_tecnicos': {required: "Campo obligatorio."},
            'proveedor_nombre_tecnico': {required: "Campo obligatorio."},
            'proveedor_cargo_tecnico': {required: "Campo obligatorio."},
            'proveedor_email_tecnico': {required: "Campo obligatorio."},
            'proveedor_tel_movil_tecnico': {required: "Campo obligatorio."},
            'proveedor_tel_fijo_tecnico': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_tecnicos').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_tecnicos_BD.php",
                data: $(form).serialize(),
                //data: dataString,
                success: function (data) {
                    //$('#msj_respuesta_ingreso_tecnicos').fadeIn(1000).html(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_tecnicos').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Tecnico registrado exitosamente!</strong></div>');
                        $('#form_ingreso_tecnicos')[0].reset();
                        $('#form_ingreso_tecnicos').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_tecnicos').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Tecnico ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_tecnicos').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar Tecnico!</strong></div>');
                    }
                }

            });
        }
    });
    //#######################################################################
    /* FORMULARIO QUE INGRESA LOS SERVICIOS */
    $("#form_ingreso_servicio").validate({
        rules: {
            selec_proveedor_servicio: {required: true},            
            servicio_responsabilidad_proveedor: {required: true, letras_espacios_comas_required :true },
            selec_proveedor_servicio2: {required: false},            
            servicio_responsabilidad_proveedor2: {required: false, letras_espacios_comas_required :false},
            selec_proveedor_servicio3: {required: false},            
            servicio_responsabilidad_proveedor3: {required: false, letras_espacios_comas_required :false},
            selec_proveedor_servicio4: {required: false},            
            servicio_responsabilidad_proveedor4: {required: false, letras_espacios_comas_required :false},
            selec_proveedor_servicio5: {required: false},            
            servicio_responsabilidad_proveedor5: {required: false, letras_espacios_comas_required :false},
            selec_proveedor_servicio6: {required: false},            
            servicio_responsabilidad_proveedor6: {required: false, letras_espacios_comas_required :true},
            servicio_nombre: {required: true, letras_espacios_required :true},
            servicio_alias: {required: false, letras_espacios_required :false},
            servicio_cod_proyec: {required: false},
            servicio_descripcion: {required: true},
            servicio_tiempo_marcha_blanca: {required: false},
            servicio_fecha_produccion: {required: true},
            servicio_fecha_marcha_blanca: {required: false},
            servicio_nota: {required: false, maxlength: 140},
            servicio_img: {required: false}

        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error has-feedback');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            selec_proveedor_servicio: {required: "Campo obligatorio."},
            servicio_responsabilidad_proveedor: {required: "Campo obligatorio."},            
            selec_proveedor_servicio2: {required: "Campo obligatorio."},            
            servicio_responsabilidad_proveedor2: {required: "Campo obligatorio."},
            selec_proveedor_servicio3: {required: "Campo obligatorio."},            
            servicio_responsabilidad_proveedor3: {required: "Campo obligatorio."},
            selec_proveedor_servicio4: {required: "Campo obligatorio."},            
            servicio_responsabilidad_proveedor4: {required: "Campo obligatorio."},
            selec_proveedor_servicio5: {required: "Campo obligatorio."},            
            servicio_responsabilidad_proveedor5: {required: "Campo obligatorio."},
            selec_proveedor_servicio6: {required: "Campo obligatorio."},            
            servicio_responsabilidad_proveedor6: {required: "Campo obligatorio."},            
            servicio_nombre: {required: "Campo obligatorio."},
            servicio_alias: {required: "Campo obligatorio."},
            servicio_cod_proyec: {required: "Campo obligatorio."},
            servicio_descripcion: {required: "Campo obligatorio."},            
            servicio_tiempo_marcha_blanca: {required: "Campo obligatorio."},
            servicio_fecha_produccion: {required: " "},
            servicio_fecha_marcha_blanca: {required: " "},
            servicio_nota: {required: "Campo obligatorio.", maxlength: "Solo 140 Caracteres"},
            servicio_img: {required: "Campo obligatorio."}

        },
        submitHandler: function (form) {
            //var input_selec_año = $("#user_anno_ingreso").val();
            $('#msj_respuesta_ingreso_servicio').html('<img src="img/ajax-loader.gif" alt="" />');
            //alert(input_selec_año);
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_servicio_BD.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    //$('#msj_respuesta_ingreso_servicio').fadeIn(1000).html(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_servicio').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Servicio registrado exitosamente!</strong></div>');
                        $('#form_ingreso_servicio')[0].reset();
                        $('#form_ingreso_servicio').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_servicio').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Servicio ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_servicio').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar Servicio!</strong></div>');
                    }
                }

            });
        }
    });
    //#######################################################################
    /* FORMULARIO QUE INGRESA LOS ENCARGADOS */
    $("#form_ingreso_encargados").validate({
        rules: {
            'selec_servicio_encargado': {required: true},
            'encarga_nomnbre': {required: true, letras_espacios_required: true},
            'encarga_email': {required: true, validacion_email:true},
            'selec_area_encargados': {required: true},
            'selec_escalamiento':{ required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error has-feedback');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'selec_servicio_encargado': {required: "Campo obligatorio."},
            'encarga_nomnbre': {required: "Campo obligatorio."},
            'encarga_email': {required: "Campo obligatorio."},
            'selec_area_encargados': {required: "Campo obligatorio."},
            'selec_escalamiento': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_encargado').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_encargados_BD.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_ingreso_encargado').fadeIn(1000).html(data_encargados);                    
                    //alert(data_encargados);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_encargado').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Encargado registrado exitosamente!</strong></div>');
                        $('#form_ingreso_encargados')[0].reset();
                        $('#form_ingreso_encargados').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_encargado').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Encargado ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_encargado').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar Encargado!</strong></div>');
                    }
                }
            });
        }
    });
    //#######################################################################
    /* FORMULARIO QUE INGRESA LOS PATROCINADORES */
    $("#form_ingreso_patrocinador").validate({
        rules: {
            selec_servicio_patrocinador: {required: true},
            servicio_nombre_patrocinador: {required: false, letras_espacios_required: true},
            servicio_email_patrocinador: {required: false, validacion_email:true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error has-feedback');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            selec_servicio_patrocinador: {required: "Campo obligatorio."},
            servicio_nombre_patrocinador: {required: "Campo obligatorio."},
            servicio_email_patrocinador: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            //var input_selec_año = $("#user_anno_ingreso").val();
            $('#msj_respuesta_ingreso_patrocinadores').html('<img src="img/ajax-loader.gif" alt="" />');
            //alert(input_selec_año);
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_patrocinadores_BD.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_ingreso_patrocinadores').fadeIn(1000).html(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_patrocinadores').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Patrocinador registrado exitosamente!</strong></div>');
                        $('#form_ingreso_patrocinador')[0].reset();
                        $('#form_ingreso_patrocinador').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_patrocinadores').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Patrocinador ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_patrocinadores').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar Patrocinador!</strong></div>');
                    }
                }

            });
        }
    });
    //#######################################################################
    /* FORMULARIO QUE INGRESA LOS JEFES DE PROYECTO */
    $("#form_ingreso_jefesproyecto").validate({
        rules: {
            selec_servicio_jefesproyector: {required: true},
            servicio_nombre_jefe_proyecto: {required: false, letras_espacios_required: true},
            servicio_email_jefe_proyecto: {required: false, validacion_email:true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error has-feedback');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            selec_servicio_jefesproyector: {required: "Campo obligatorio."},
            servicio_nombre_jefe_proyecto: {required: "Campo obligatorio."},
            servicio_email_jefe_proyecto: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_jefe_proyecto').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_jefeProyecto_BD.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_ingreso_jefe_proyecto').fadeIn(1000).html(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_jefe_proyecto').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Jefe Proyecto registrado exitosamente!</strong></div>');
                        $('#form_ingreso_jefesproyecto')[0].reset();
                        $('#form_ingreso_jefesproyecto').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_jefe_proyecto').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Jefe Proyecto ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_jefe_proyecto').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar Jefe Proyecto!</strong></div>');
                    }
                }

            });
        }
    });
    //#######################################################################
    /* FORMULARIO QUE INGRESA LOS ARQUITECTOS */
    $("#form_ingreso_arquitecto").validate({
        rules: {
            selec_servicio_arquitecto: {required: true},
            servicio_nombre_arquitecto: {required: false, letras_espacios_required: true},
            servicio_email_arquitecto: {required: false, validacion_email:true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error has-feedback');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            selec_servicio_arquitecto: {required: "Campo obligatorio."},
            servicio_nombre_arquitecto: {required: "Campo obligatorio."},
            servicio_email_arquitecto: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_arquitecto').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_arquitecto_BD.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_ingreso_arquitecto').fadeIn(1000).html(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_arquitecto').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Arquitecto registrado exitosamente!</strong></div>');
                        $('#form_ingreso_arquitecto')[0].reset();
                        $('#form_ingreso_arquitecto').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_arquitecto').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Arquitecto ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_arquitecto').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar Arquitecto!</strong></div>');
                    }
                }

            });
        }
    });    
    //#######################################################################
    /* FORMULARIO QUE INGRESA LOS CLIENTES */
    $("#form_ingreso_clientes").validate({
        rules: {
            selec_servicio_cliente: {required: true},
            servicio_nombre_area_cliente: {required: true, letras_espacios_required: true},
            servicio_nombre_contacto_cliente: {required: true, letras_espacios_required: true},
            servicio_email_contacto_cliente: {required: true, validacion_email: true},
            servicio_motivo_cliente: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error has-feedback');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            selec_servicio_cliente: {required: "Campo obligatorio."},
            servicio_nombre_area_cliente: {required: "Campo obligatorio."},
            servicio_nombre_contacto_cliente: {required: "Campo obligatorio."},
            servicio_email_contacto_cliente: {required: "Campo obligatorio."},
            servicio_motivo_cliente: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_clientes').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_clientes_BD.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_ingreso_clientes').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_clientes').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Cliente registrado exitosamente!</strong></div>');
                        $('#form_ingreso_clientes')[0].reset();
                        $('#form_ingreso_clientes').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_clientes').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Cliente ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_clientes').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar Cliente!</strong></div>');
                    }
                }

            });
        }
    });
    //#######################################################################
    /* FORMULARIO QUE INGRESA LOS SOPORTES */
    $("#form_ingreso_soporte").validate({
        rules: {
            'selec_proveedor_soporte': {required: true},
            'selec_servicio_soporte': {required: true},
            'proveedor_fecha_inicio_soporte': {required: false},
            'proveedor_fecha_termino_soporte': {required: true},
            'proveedor_tarea_incluida': {required: true},
            'proveedor_sistema_tickets': {required: false},
            'proveedor_incluye_respuestos': {required: true},
            'proveedor_flujo_soporte': {required: true},
            'proveedor_tipo_horario_soporte': {required: true},
            'proveedor_tiempo_respuesta': {required: true},
            'proveedor_tipo_asistencia[]': {required: true},
            'proveedor_tipo_soporte[]': {required: true},
            'proveedor_horario_inicio_atencion_lunes': {required: true},
            'proveedor_horario_termino_atencion_lunes': {required: true},
            'proveedor_horario_inicio_atencion_martes': {required: true},
            'proveedor_horario_termino_atencion_martes': {required: true},
            'proveedor_horario_inicio_atencion_miercoles': {required: true},
            'proveedor_horario_termino_atencion_miercoles': {required: true},
            'proveedor_horario_inicio_atencion_jueves': {required: true},
            'proveedor_horario_termino_atencion_jueves': {required: true},
            'proveedor_horario_inicio_atencion_viernes': {required: true},
            'proveedor_horario_termino_atencion_viernes': {required: true},
            'proveedor_horario_inicio_atencion_sabado': {required: true},
            'proveedor_horario_termino_atencion_sabado': {required: true},
            'proveedor_horario_inicio_atencion_domingo': {required: true},
            'proveedor_horario_termino_atencion_domingo': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');           
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'has-error',
        messages: {
            'selec_proveedor_soporte': {required: "Debe Seleccionar Proveedor"},
            'selec_servicio_soporte': {required: "Debe Seleccionar Servicio"},
            'proveedor_fecha_inicio_soporte': {required: " "},
            'proveedor_fecha_termino_soporte': {required: " "},
            'proveedor_tarea_incluida': {required: " "},
            'proveedor_sistema_tickets': {required: " "},
            'proveedor_incluye_respuestos': {required: " "},
            'proveedor_flujo_soporte': {required: " "},
            'proveedor_tipo_horario_soporte': {required: " "},
            'proveedor_tiempo_respuesta': {required: " "},
            'proveedor_tipo_asistencia[]': {required: " "},
            'proveedor_tipo_soporte[]': {required: " "},
            'proveedor_horario_inicio_atencion_lunes': {required: "Campo obligatorio."},
            'proveedor_horario_termino_atencion_lunes': {required: "Campo obligatorio."},
            'proveedor_horario_inicio_atencion_martes': {required: "Campo obligatorio."},
            'proveedor_horario_termino_atencion_martes': {required: "Campo obligatorio."},
            'proveedor_horario_inicio_atencion_miercoles': {required: "Campo obligatorio."},
            'proveedor_horario_termino_atencion_miercoles': {required: "Campo obligatorio."},
            'proveedor_horario_inicio_atencion_jueves': {required: "Campo obligatorio."},
            'proveedor_horario_termino_atencion_jueves': {required: "Campo obligatorio."},
            'proveedor_horario_inicio_atencion_viernes': {required: "Campo obligatorio."},
            'proveedor_horario_termino_atencion_viernes': {required: "Campo obligatorio."},
            'proveedor_horario_inicio_atencion_sabado': {required: "Campo obligatorio."},
            'proveedor_horario_termino_atencion_sabado': {required: "Campo obligatorio."},
            'proveedor_horario_inicio_atencion_domingo': {required: "Campo obligatorio."},
            'proveedor_horario_termino_atencion_domingo': {required: "Campo obligatorio."}

        },
        submitHandler: function (form) {
            //var input_selec_año = $("#user_anno_ingreso").val();
            //var proveedor_nota = $("#proveedor_nota").val();
            //alert(proveedor_nota);
            //$('#div_resultado_agregar_usuario').html('<img src="../grafica/img/ajax-loader.gif" alt="" />');
            //alert(input_selec_año);
            var dataString = 'busqueda=' + 100;
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_soporte_BD.php",
                data: $(form).serialize(),
                //data: dataString,
                success: function (data) {
                    //$('#msj_respuesta_ingreso_soporte').fadeIn(1000).html(data);
                    //lert(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_soporte').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Soporte registrado exitosamente!</strong></div>');
                        $('#form_ingreso_soporte')[0].reset();
                        $('#form_ingreso_soporte').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_soporte').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Soporte ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_soporte').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar el Soporte!</strong></div>');
                    }
                }

            });
        }
    });
    
    //#######################################################################
    //##########################-UPDATES SERVICIO-###########################
    $("#form_update_NombreServicio").validate({
        rules: {
            'servicio_nombre_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'servicio_nombre_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_AliasServicio").validate({
        rules: {
            'servicio_alias_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'servicio_alias_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_descripcion").validate({
        rules: {
            'servicio_descripcion_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'servicio_descripcion_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_FechaMarchaBlanca").validate({
        rules: {
            'servicio_fecha_marcha_blanca_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'servicio_fecha_marcha_blanca_update': {required: " "}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_TiempoMarchaBlanca").validate({
        rules: {
            'servicio_tiempo_marcha_blanca_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'servicio_tiempo_marcha_blanca_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_FechaProduccion").validate({
        rules: {
            'servicio_fecha_produccion_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'servicio_fecha_produccion_update': {required: " "}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_Encargados").validate({
        rules: {
            'encarga_nombre_update1': {required: true},
            'encarga_email_update1': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'encarga_nombre_update1': {required: "Campo obligatorio."},
            'encarga_email_update1': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_Patrocinador").validate({
        rules: {
            'servicio_nombre_patrocinador_update1': {required: true},
            'servicio_email_patrocinador_update1': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'servicio_nombre_patrocinador_update1': {required: "Campo obligatorio."},
            'servicio_email_patrocinador_update1': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_JefeProyecto").validate({
        rules: {
            'servicio_nombre_jefe_proyecto_update': {required: true},
            'servicio_email_jefe_proyecto_update':{required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'servicio_nombre_jefe_proyecto_update': {required: "Campo obligatorio."},
            'servicio_email_jefe_proyecto_update':{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_Arquitecto").validate({
        rules: {
            'servicio_nombre_arquitecto_update': {required: true},
            'servicio_email_arquitecto_update':{required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'servicio_nombre_arquitecto_update': {required: "Campo obligatorio."},
            'servicio_email_arquitecto_update':{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_Cliente").validate({
        rules: {
            'servicio_nombre_area_cliente_update': {required: true},
            'servicio_nombre_contacto_cliente_update': {required: true},
            'servicio_email_contacto_cliente_update': {required: true},
            'servicio_motivo_cliente_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'servicio_nombre_area_cliente_update': {required: "Campo obligatorio."},
            'servicio_nombre_contacto_cliente_update': {required: "Campo obligatorio."},
            'servicio_email_contacto_cliente_update': {required: "Campo obligatorio."},
            'servicio_motivo_cliente_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_Proveedor").validate({
        rules: {
            'selec_proveedor_soporte_update': {required: true},
            'servicio_responsabilidad_proveedor_update':{required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'selec_proveedor_soporte_update': {required: "Campo obligatorio."},
            'servicio_responsabilidad_proveedor_update':{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    /**********************************************************************************************/
    $("#form_update_UserContra").validate({
        rules: {
            'UpdateUserContraAplica': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'UpdateUserContraAplica': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_UserContra').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_UserContra').fadeIn(1000).html(data);
                }

            });
        }
    });
    /**********************************************************************************************/
    $("#form_update_UserContraBD").validate({
        rules: {
            'UpdateUserContraBD': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'UpdateUserContraBD': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_UserContraBD').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_UserContraBD').fadeIn(1000).html(data);
                }

            });
        }
    });
    /**********************************************************************************************/
    $("#form_update_RutinasMantenimiento").validate({
        rules: {
            'UpdateRutinasMantenimiento': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'UpdateRutinasMantenimiento': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_UpdateRutinasMantenimiento').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_UpdateRutinasMantenimiento').fadeIn(1000).html(data);
                }

            });
        }
    });
    /**********************************************************************************************/
    $("#form_update_problema_recurrente").validate({
        rules: {
            'UpdateProblemaRecurrente': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'UpdateProblemaRecurrente': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_UpdateProblemaRecurrente').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_UpdateProblemaRecurrente').fadeIn(1000).html(data);
                }

            });
        }
    });
    /**********************************************************************************************/
    $("#form_update_elemento_monitoriar").validate({
        rules: {
            'UpdateElementoMonitoriar': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'UpdateElementoMonitoriar': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_UpdateElementoMonitoriar').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_UpdateElementoMonitoriar').fadeIn(1000).html(data);
                }

            });
        }
    });
    /**********************************************************************************************/
    $("#form_update_elemento_respaldo").validate({
        rules: {
            'UpdateElementoRespaldo': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'UpdateElementoRespaldo': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_UpdateElementoRespaldo').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_UpdateElementoRespaldo').fadeIn(1000).html(data);
                }

            });
        }
    });
    /**********************************************************************************************/
    $("#form_update_gestor_monitoreo").validate({
        rules: {
            'selec_gestor_monitoreo': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'selec_gestor_monitoreo': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_UpdateGestorMonitoreo').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_UpdateGestorMonitoreo').fadeIn(1000).html(data);
                }

            });
        }
    });
    /**********************************************************************************************/
    $("#form_update_gestor_respaldo").validate({
        rules: {
            'selec_gestor_respaldo': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'selec_gestor_respaldo': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_UpdateGestorRespaldo').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_UpdateGestorRespaldo').fadeIn(1000).html(data);
                }

            });
        }
    });
    /**********************************************************************************************/
    $("#form_update_detalleLicencia").validate({
        rules: {
            'UpdateDetalleLicencia': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'UpdateDetalleLicencia': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_UpdateDetalleLicencia').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_UpdateDetalleLicencia').fadeIn(1000).html(data);
                }

            });
        }
    });
    /**********************************************************************************************/
    $("#form_update_fecha_termino").validate({
        rules: {
            'Updatefecha_termino_licencia': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'Updatefecha_termino_licencia': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_UpdateFechaTermino').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_UpdateFechaTermino').fadeIn(1000).html(data);
                }

            });
        }
    });
    /**********************************************************************************************/
    $("#form_update_detalle_insumos").validate({
        rules: {
            'UpdateInsumoMarca': {required: true},
            'UpdateModeloInsumo':{required: true},
            'UpdateCaracteriticaInsumo':{required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            'UpdateInsumoMarca': {required: true},
            'UpdateModeloInsumo':{required: true},
            'UpdateCaracteriticaInsumo':{required: true}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_UpdateDetalleInsumo').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_UpdateDetalleInsumo').fadeIn(1000).html(data);
                }
            });
        }
    });
    /**********************************************************************************************/
    $("#form_update_NotaServicio").validate({
        rules: {
            'servicio_nota_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'servicio_nota_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    //##########################-UPDATES PROVEEDOR-###########################
    $("#form_update_NombreProveedor").validate({
        rules: {
            'proveedor_update_nombre_empresa': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'proveedor_update_nombre_empresa': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_NombreProveedor').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates_proveedores.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_update_NombreProveedor').fadeIn(1000).html(data);
                    if(data == "1"){
                        $('#msj_respuesta_update_NombreProveedor').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Nombre ha sido actualizado exitosamente!</strong></div>');
                        $('#form_update_NombreProveedor')[0].reset();
                        $('#form_update_NombreProveedor').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_update_NombreProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Nombre ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_update_NombreProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar el Nombre!</strong></div>');
                    }
                }

            });
        }
    });
    $("#form_update_AliasProveedor").validate({
        rules: {
            'proveedor_alias_fantasia_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'proveedor_alias_fantasia_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_AliasProveedor').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates_proveedores.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_update_AliasProveedor').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_update_AliasProveedor').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Alias ha sido actualizado exitosamente!</strong></div>');
                        $('#form_update_AliasProveedor')[0].reset();
                        $('#form_update_AliasProveedor').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_update_AliasProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Alias ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_update_AliasProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar el Alias!</strong></div>');
                    }
                }

            });
        }
    });
    $("#form_update_SitioWebProveedor").validate({
        rules: {
            'proveedor_url_sitio_web_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'proveedor_url_sitio_web_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_SitioWebProveedor').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates_proveedores.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_update_SitioWebProveedor').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_update_SitioWebProveedor').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Sitio Web ha sido actualizado exitosamente!</strong></div>');
                        $('#form_update_SitioWebProveedor')[0].reset();
                        $('#form_update_SitioWebProveedor').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_update_SitioWebProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Sitio Web ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_update_SitioWebProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar el Sitio Web!</strong></div>');
                    }
                }

            });
        }
    });
    $("#form_update_NumeroMesaAyuda").validate({
        rules: {
            'proveedor_n_mesa_ayuda_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'proveedor_n_mesa_ayuda_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_NumMesaAyudaProveedor').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates_proveedores.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_update_NumMesaAyudaProveedor').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_update_NumMesaAyudaProveedor').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> N° Mesa de Ayuda ha sido actualizado exitosamente!</strong></div>');
                        $('#form_update_NumeroMesaAyuda')[0].reset();
                        $('#form_update_NumeroMesaAyuda').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_update_NumMesaAyudaProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> N° Mesa de Ayuda ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_update_NumMesaAyudaProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar el N° Mesa de Ayuda!</strong></div>');
                    }
                }

            });
        }
    });
    $("#form_update_EmailProveedore").validate({
        rules: {
            'proveedor_email_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'proveedor_email_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_EmailProveedor').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates_proveedores.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_update_EmailProveedor').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_update_EmailProveedor').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Email ha sido actualizado exitosamente!</strong></div>');
                        $('#form_update_EmailProveedore')[0].reset();
                        $('#form_update_EmailProveedore').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_update_EmailProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Email ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_update_EmailProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar el Email!</strong></div>');
                    }
                }

            });
        }
    });
    $("#form_update_DireccionProveedor").validate({
        rules: {
            'proveedor_direccion_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'proveedor_direccion_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_DireccionProveedor').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates_proveedores.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_update_DireccionProveedor').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_update_DireccionProveedor').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Dirección ha sido actualizado exitosamente!</strong></div>');
                        $('#form_update_DireccionProveedor')[0].reset();
                        $('#form_update_DireccionProveedor').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_update_DireccionProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Dirección ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_update_DireccionProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Dirección al registrar el Email!</strong></div>');
                    }
                }

            });
        }
    });    
    /*$("#form_update_HorarioAtencion").validate({
        rules: {
            'servicio_nota_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'servicio_nota_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates_proveedores.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });*/
    $("#form_update_ContactoAdministrativo").validate({
        rules: {
            proveedor_nombre_administrativo_update: {required: true},
            proveedor_cargo_administrativo_update:{required: true},
            proveedor_email_administrativo_update:{required: true},
            proveedor_tel_movil_administrativo_update:{required: false},
            proveedor_tel_fijo_administrativo_update:{required: false}
                    
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            proveedor_nombre_administrativo_update: {required: "Campo obligatorio."},
            proveedor_cargo_administrativo_update:{required: "Campo obligatorio."},
            proveedor_email_administrativo_update:{required: "Campo obligatorio."},
            proveedor_tel_movil_administrativo_update:{required: "Campo obligatorio."},
            proveedor_tel_fijo_administrativo_update:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_ContactoAdministrativoProveedor').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates_proveedores.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_update_ContactoAdministrativoProveedor').fadeIn(1000).html(data);
                    alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_update_ContactoAdministrativoProveedor').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Administrativo ha sido actualizado exitosamente!</strong></div>');
                        $('#form_update_ContactoAdministrativo')[0].reset();
                        $('#form_update_ContactoAdministrativo').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_update_ContactoAdministrativoProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Administrativo ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_update_ContactoAdministrativoProveedor').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Administrativo al registrar el Email!</strong></div>');
                    }
                    
                }

            });
        }
    });
    $("#form_update_ContactoTecnico").validate({
        rules: {
            proveedor_nombre_tecnico_update: {required: true},
            proveedor_cargo_tecnico_update: {required: true},
            proveedor_email_tecnico_update: {required: true},
            proveedor_tel_movil_tecnico_update: {required: false},
            proveedor_tel_fijo_tecnico_update: {required: false}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            proveedor_nombre_tecnico_update: {required: "Campo obligatorio."},
            proveedor_cargo_tecnico_update: {required: "Campo obligatorio."},
            proveedor_email_tecnico_update: {required: "Campo obligatorio."},
            proveedor_tel_movil_tecnico_update: {required: "Campo obligatorio."},
            proveedor_tel_fijo_tecnico_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_ContactoTecnicoProveedor').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/updates_proveedores.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_ContactoTecnicoProveedor').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_update_NotaProveedor").validate({
        rules: {
            'proveedor_nota_update': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            'proveedor_nota_update': {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/updates_proveedores.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    //#######################################################################
    $("#form_ingreso_infraestructura").validate({
        rules: {
            infra_select_componente:{required: true},
            infra_select_servicio_componente:{required: true},
            infra_nombre_componente:{required: true},
            infra_marca_componente:{required: true},
            infra_modelo_componente:{required: true},
            infra_num_serie:{required: false},
            infra_sistema_operativo:{required: false},
            infra_aplicaciones:{required: false},
            infra_num_parte:{required: false},
            infra_espf_tecnicas:{required: false},
            infra_espf_energia:{required: false},
            infra_ubica_fisica_datacenter:{required: true},
            infra_garantia_provee:{required: false},
            infra_etiqueta_fisico:{required: false},
            infra_ubica_fisica_detalle:{required: false},
            infra_garantia_detalle:{required: false},
            infra_fecha_termino_garantia:{required: true},
            infra_usuarios_contraseñas:{required: true},
            infra_diagrama_img_red: {required: false, extension: "png|jpg"}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            infra_diagrama_img_red: {required: "Campo obligatorio.", extension: "Por favor, introduzca un valor con una extensión válida."},
            infra_fecha_termino_garantia:{required: " "}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_infraestructura').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_ingreso_infraestructura').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_infraestructura').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Componente registrado exitosamente!</strong></div>');
                        $('#form_ingreso_infraestructura')[0].reset();
                        $('#form_ingreso_infraestructura').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_infraestructura').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Componente ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_infraestructura').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar Componente!</strong></div>');
                    }
                }

            });
        }
    });
    $("#form_Insert_AgregaComponente").validate({
        rules: {
            infra_tipo_componente:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            infra_tipo_componente: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_componente').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_ingreso_componente').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_componente').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Tipo Componente registrado exitosamente!</strong></div>');
                        $('#form_Insert_AgregaComponente')[0].reset();
                        $('#form_Insert_AgregaComponente').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_componente').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Tipo Componente ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_componente').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar Tipo Componente!</strong></div>');
                    }
                }

            });
        }
    });
    $("#form_ingreso_ip_componentes").validate({
        rules: {
            infra_select_ip_componente:{required: true},
            infra_num_ip_componente:{required: true, IP4Checker: true},
            infra_plan_ip_componente:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            infra_select_componente: {required: "Campo obligatorio."},
            infra_num_ip_componente: {required: "Campo obligatorio."},
            infra_plan_ip_componente: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_IP').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_ingreso_IP').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_IP').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> IP registrada exitosamente!</strong></div>');
                        $('#form_ingreso_ip_componentes')[0].reset();
                        $('#form_ingreso_ip_componentes').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_IP').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> IP ya se encuentra registrada!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_IP').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar la IP!</strong></div>');
                    }
                }

            });
        }
    });
    //#######################-UPDATES INFRAESTRUCTURA-#######################
    $("#form_update_NombreComponente").validate({
        rules: {
            componente_nombre_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_nombre_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_ModeloComponente").validate({
        rules: {
            componente_modelo_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_modelo_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_MarcaComponente").validate({
        rules: {
            componente_marca_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_marca_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_NSerieComponente").validate({
        rules: {
            componente_NSerie_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_NSerie_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_NParteComponente").validate({
        rules: {
            componente_NParte_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_NParte_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_Modal_UbicaFisicaDataCenter").validate({
        rules: {
            componente_Ubica_Fisica_Datacenter_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_Ubica_Fisica_Datacenter_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_UbicaFisicaDetalle").validate({
        rules: {
            componente_UbicaFisicaDetalle_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_UbicaFisicaDetalle_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_FechaTerminoGarantia").validate({
        rules: {
            componente_FechaTerminoGarantia_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_FechaTerminoGarantia_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_Aplicaciones").validate({
        rules: {
            componente_Aplicaciones_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_Aplicaciones_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_EspfTecnicas").validate({
        rules: {
            componente_EpsfTecnicas_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_EpsfTecnicas_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_EspfEnergia").validate({
        rules: {
            componente_EpsfEnergia_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_EpsfEnergia_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_ServiciosAsociados").validate({
        rules: {
            componente_UbicaFisicaDetalle_update:{required: false}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_UbicaFisicaDetalle_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_IPAsociadas").validate({
        rules: {
            componente_UbicaFisicaDetalle_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_UbicaFisicaDetalle_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_AsociarOtroServicio").validate({
        rules: {
            infra_select_sAsociarOtroServicioComponente:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            infra_select_sAsociarOtroServicioComponente: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_UsuariosContraseñas").validate({
        rules: {
            componente_UsuariosContraseñas_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_UsuariosContraseñas_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_EtiquetaFisico").validate({
        rules: {
            componente_EtiquetaFisico_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_EtiquetaFisico_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_GarantiaProvee").validate({
        rules: {
            componente_GarantiaProvee_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_GarantiaProvee_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_GarantiaDetalle").validate({
        rules: {
            componente_GarantiaDetalle_update:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            componente_GarantiaDetalle_update: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    $("#form_update_GarantiaDetalle").validate({
        rules: {
            form_update_DiagramaRed:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            form_update_DiagramaRed: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "./modelo/update_infraestructura.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
                    alert(data);
                }

            });
        }
    });
    //#######################################################################
    $("#form_registro_Usuario").validate({
        rules: {
            user_nombre:{required: true},
            user_apell_paterno:{required: true},
            user_apell_materno:{required: true},
            user_tel_movil:{required: false},
            user_email:{required: true},
            user_username:{required: true},
            user_contrasena:{
                required: true,
                minlength: 5,
                maxlength:12
            },
            user_confirm_contrasena:{
                required: true,
                minlength: 5,
                maxlength:13,
                equalTo: "#user_contrasena"
            },
            select_Permiso_usuario:{required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            user_nombre:{required: "Campo obligatorio."},
            user_apell_paterno:{required: "Campo obligatorio."},
            user_apell_materno:{required: "Campo obligatorio."},
            user_tel_movil:{required: "Campo obligatorio."},
            user_email:{required: "Campo obligatorio."},
            user_username:{required: "Campo obligatorio."},
            user_contrasena:{
                required: "Campo obligatorio.",
                minlength:"Introduzca una contraseña entre 5 y 12 caracteres",
                maxlength:"Introduzca una contraseña entre 5 y 12 caracteres"
            },
            user_confirm_contrasena:{
                required: "Campo obligatorio.",
                minlength:"Introduzca una contraseña entre 5 y 12 caracteres",
                maxlength:"Introduzca una contraseña entre 5 y 13 caracteres"
            },
            select_Permiso_usuario:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_usuarios').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_usuario.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_login_usuario").validate({
        rules: {
            username:{required: true},
            password:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            username: {required: "Campo obligatorio."},
            password: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_login_usuario').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/verificar_usuario.php",
                data: $(form).serialize(),
                success: function (data) {
                    if(data == "1"){
                      //alert("Existe");  
                      //$('#msj_respuesta_login_usuario').fadeIn(1000).html('<meta http-equiv="Refresh" content="0"; url="../index.php" />');
                      location.href = "index.php";
                      
                    }else{
                      //alert("NO Existe");  
                      $('#msj_respuesta_login_usuario').fadeIn(1000).html('<div class="alert alert-danger"><strong>Error en el Usuario o Contraseña</strong></div>');
                    }                        
                    //$('#msj_respuesta_login_usuario').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_lock_screen").validate({
        rules: {
            contrasena_lock_screen:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            contrasena_lock_screen: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_lock_screen_usuario').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/verificar_lock_screen.php",
                data: $(form).serialize(),
                success: function (data) {
                    //alert(data);
                    if(data == "1"){
                      //alert("Existe");  
                      location.href = "index.php";
                      
                    }else{
                      //alert("NO Existe");  
                      $('#msj_respuesta_lock_screen_usuario').fadeIn(1000).html('<div class="alert alert-danger" style="padding: 4px !important; margin: 0 auto; width: 270px;"><strong>Error en la Contraseña</strong></div>');
                    }
                }

            });
        }
    });
    $("#form_Insert_AgregaGestorMonitoreo").validate({
        rules: {
            GestorMonitoreo_nombre:{required: true},
            GestorMonitoreo_descripcion:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            GestorMonitoreo_nombre: {required: "Campo obligatorio."},
            GestorMonitoreo_descripcion: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_GestorMonitoreo').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_gestor_monitoreo.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_ingreso_GestorMonitoreo').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_Insert_AgregaGestorRespaldo").validate({
        rules: {
            GestorRespaldo_nombre:{required: true},
            GestorRespaldo_descripcion:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            GestorRespaldo_nombre: {required: "Campo obligatorio."},
            GestorRespaldo_descripcion: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_GestorRespaldo').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_gestor_respaldo.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_ingreso_GestorRespaldo').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_ingreso_monitoreo_respaldo").validate({
        rules: {
            selec_servicio_monitoreo_respaldo:{required: true},
            usuario_contrasena_aplicacion:{required: true},
            usuario_contrasena_BD:{required: true},
            rutinas_matenimiento:{required: true},
            problema_recurrente:{required: true},
            elementos_monitoriar:{required: true},
            selec_gestor_monitoreo:{required: true},
            elemento_respaldar:{required: true},
            selec_gestor_respaldo:{required: true}
            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            selec_servicio_monitoreo_respaldo:{required: "Campo obligatorio."},
            usuario_contrasena_aplicacion:{required: "Campo obligatorio."},
            usuario_contrasena_BD:{required: "Campo obligatorio."},
            rutinas_matenimiento:{required: "Campo obligatorio."},
            problema_recurrente:{required: "Campo obligatorio."},
            elementos_monitoriar:{required: "Campo obligatorio."},
            selec_gestor_monitoreo:{required: "Campo obligatorio."},
            elemento_respaldar:{required: "Campo obligatorio."},
            selec_gestor_respaldo:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_respaldo_monitoreo').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_monitoreo_respaldo.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_respaldo_monitoreo').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_respaldo_monitoreo').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Información Monitero y Respaldo registrada exitosamente!</strong></div>');
                        $('#form_ingreso_monitoreo_respaldo')[0].reset();
                        $('#form_ingreso_monitoreo_respaldo').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_respaldo_monitoreo').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Información Monitero y Respaldo ya se encuentra registrada!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_respaldo_monitoreo').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar la Información Monitero y Respaldo!</strong></div>');
                    }
                }

            });
        }
    });
    $("#form_ingreso_licencias").validate({
        rules: {
            selec_servicio_licencias:{required: true},
            selec_proveedor_licencia:{required: true},
            detalle_licencia:{required: true},
            fecha_termino_licencia:{required: true}            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            selec_servicio_licencias:{required: "Campo obligatorio."},
            selec_proveedor_licencia:{required: "Campo obligatorio."},
            detalle_licencia:{required: "Campo obligatorio."},
            fecha_termino_licencia:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_licencias').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_licencias.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_ingreso_licencias').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_licencias').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Licencia registrada exitosamente!</strong></div>');
                        $('#form_ingreso_licencias')[0].reset();
                        $('#form_ingreso_licencias').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_licencias').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Licencia ya se encuentra registrada!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_licencias').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar la Licencia!</strong></div>');
                    }
                }

            });
        }
    });
    $("#form_ingreso_insumos").validate(    {
        rules: {
            selec_proveedor_insumos:{required: true},
            marca_insumo:{required: true},
            modelo_insumo:{required: true},
            caracteristica_insumo:{required: true},
            selec_servicio_licencias:{required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            //$('#busqueda_rut').removeClass('disabled');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //$('#busqueda_rut').attr('disabled', true);
        },
        errorClass: 'help-block',
        messages: {
            selec_proveedor_insumos:{required: "Campo obligatorio."},
            marca_insumo:{required: "Campo obligatorio."},
            modelo_insumo:{required: "Campo obligatorio."},
            caracteristica_insumo:{required: "Campo obligatorio."},
            selec_servicio_licencias:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_insumo').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/ingreso_insumos.php",
                data: $(form).serialize(),
                success: function (data) {
                    //$('#msj_respuesta_ingreso_insumo').fadeIn(1000).html(data);
                    //alert(data);
                    if(data == "1"){
                        $('#msj_respuesta_ingreso_insumo').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> Insumo registrado exitosamente!</strong></div>');
                        $('#form_ingreso_insumos')[0].reset();
                        $('#form_ingreso_insumos').trigger("reset");
                    }else if(data == "0"){
                        $('#msj_respuesta_ingreso_insumo').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Insumo ya se encuentra registrado!</strong></div>');                        
                    }else if(data == "2"){
                        $('#msj_respuesta_ingreso_insumo').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar el Insumo!</strong></div>');
                    }
                }

            });
        }
    });
    ////////////////////////////////////////////////////////////////////////////////////////////////
    $("#form_Update_usuarios_sistema_inventario").validate({
        rules: {
            Update_id:{required: true},
            Update_user_nombre:{required: true},
            Update_user_apell_paterno:{required: true},
            Update_user_apell_materno:{required: true},
            Update_user_tel_movil:{required: true},
            Update_user_email:{required: true},
            Update_username:{required: true},
            select_Permiso_usuario:{required: true},
            select_status_usuario:{required: true},
            Update_user_contrasena:{
                required: false,
                minlength: 5,
                maxlength:13},
            Update_user_confirm_contrasena:{
                required: false,
                minlength: 5,
                maxlength:13,
                equalTo: "#Update_user_contrasena"}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            Update_id:{required: "Campo obligatorio."},
            Update_user_nombre:{required: "Campo obligatorio."},
            Update_user_apell_paterno:{required: "Campo obligatorio."},
            Update_user_apell_materno:{required: "Campo obligatorio."},
            Update_user_tel_movil:{required: "Campo obligatorio."},
            Update_user_email:{required: "Campo obligatorio."},
            Update_username:{required: "Campo obligatorio."},
            select_Permiso_usuario:{required: "Campo obligatorio."},
            select_status_usuario:{required: "Campo obligatorio."},
            Update_user_contrasena:{
                required: "Campo obligatorio.",
                minlength:"Introduzca una contraseña entre 5 y 12 caracteres",
                maxlength:"Introduzca una contraseña entre 5 y 13 caracteres"},
            Update_user_confirm_contrasena:{
                required: "Campo obligatorio.",
                minlength:"Introduzca una contraseña entre 5 y 12 caracteres",
                maxlength:"Introduzca una contraseña entre 5 y 13 caracteres"}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_usuario_inventario').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/update_usuario_sistema_inventario.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_usuario_inventario').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    ////////////////////////////////////////////////////////////////////////////////////////////////
    $("#form_update_FechaInicioSoporte").validate({
        rules: {
            soporte_update_FechaInicioSoporte:{required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            soporte_update_FechaInicioSoporte:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_fecha_inicio_soporte').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/update_soporte.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_fecha_inicio_soporte').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_update_FechaTerminoSoporte").validate({
        rules: {
            soporte_update_FechaTerminoSoporte:{required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            soporte_update_FechaTerminoSoporte:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_fecha_termino_soporte').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/update_soporte.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_fecha_termino_soporte').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_update_TipoHorarioSoporte").validate({
        rules: {
            soporte_update_TipoHorarioSoporte:{required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            soporte_update_TipoHorarioSoporte:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_tipo_horario_soporte').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/update_soporte.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_tipo_horario_soporte').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_update_SistemaTickets").validate({
        rules: {
            soporte_update_SistemaTickets:{required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            soporte_update_SistemaTickets:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_Sistematickets').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/update_soporte.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_Sistematickets').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_update_TipoAsistencia").validate({
        rules: {
            soporte_update_TipoAsistencia:{required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            soporte_update_TipoAsistencia:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_TipoAsistencia').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/update_soporte.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_TipoAsistencia').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_update_TipoSoporte").validate({
        rules: {
            proveedor_tipo_soporte_reactivo:{required: false},
            proveedor_tipo_soporte_proactivo:{required: false},
            proveedor_tipo_soporte_evolutivo:{required: false}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            proveedor_tipo_soporte_reactivo:{required: " "},
            proveedor_tipo_soporte_proactivo:{required: " "},
            proveedor_tipo_soporte_evolutivo:{required: " "}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_tipo_soporte').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/update_soporte.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_tipo_soporte').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_update_TiempoRespuesta").validate({
        rules: {
            soporte_update_TiemposRespuestas:{required: false}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            soporte_update_TiemposRespuestas:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_TiempoRespuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/update_soporte.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_TiempoRespuesta').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_update_TareasIncluidas").validate({
        rules: {
            soporte_update_TareasIncluidas:{required: false}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            soporte_update_TareasIncluidas:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_TareasIcluidas').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/update_soporte.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_TareasIcluidas').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_update_FlujoSoporte").validate({
        rules: {
            soporte_update_FlujoSoporte:{required: false}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            soporte_update_FlujoSoporte:{required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_FlujoSoporte').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/update_soporte.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_FlujoSoporte').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    $("#form_update_HorarioAtencion").validate({
        rules: {            
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            
        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_HorariosAtencion').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/update_soporte.php",
                data: $(form).serialize(),
                success: function (data) {
                    $('#msj_respuesta_update_HorariosAtencion').fadeIn(1000).html(data);
                    //alert(data);
                }

            });
        }
    });
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
});
