<?php 
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
?>
<?php
require_once './controlador/Db.class.php';
$db = new Db();
//LLENADO DEL SELECT
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->        
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div class="padding-md">
                    <div class="panel panel-default table-responsive">
                        <div class="panel-heading">
                            Proveedores
                            <span class="label label-info pull-right"><?php echo $count_soportes = $db->single("SELECT COUNT(*) FROM Proveedores"); ?> Registro(s)</span>
                        </div>
                        <div class="padding-md clearfix">
                            <table class="table table-striped" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>RUT</th>
                                        <th>Razón Social</th>
                                        <th>Alias Proveedor</th>
                                        <th>N° Mesa de Ayuda</th>
                                        <th>Sitio Web</th>
                                        <th>Más Información</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    $datos_sql_select_proveedor = $db->query("SELECT * FROM Proveedores");
                                    foreach ($datos_sql_select_proveedor as $row_proveedor):
                                        $provee_id = $row_proveedor["provee_id"];
                                        echo '<tr>'
                                        . '<td>' . $row_proveedor["provee_rut"] . '</td>'
                                        . '<td>' . $row_proveedor["provee_nombre_empresa"] . '</td>'
                                        . '<td>' . $row_proveedor["provee_nombre_fantasia"] . '</td>'
                                        . '<td>' . $row_proveedor["provee_num_mesa_ayuda"] . '</td>'
                                        . '<td><a href="' . $row_proveedor["provee_sitio_web"] . '" class="btn-link" target="_blank"> ' . $row_proveedor["provee_sitio_web"] . '</a></td>'
                                        . '<td><a href="info_proveedor.php?provee_id=' . $provee_id . '" class="main-link"><i class="fa fa-info fa-lg"></i> Ver Más</a></td>'
                                        . '</tr>';
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                        </div><!-- /.padding-md -->
                    </div><!-- /panel -->
                </div><!-- /.padding-md -->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/confirmation.logout.inc.php'; ?> 
        <!-- ######################################## -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/menu-active-class.js"></script>
        
        <!-- Datatable -->
        <script src='js/jquery.dataTables.min.js'></script>	
        <script type="text/javascript">
            $(document).ready(function () {
                $('#dataTable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "asStripClasses": null
                });
                $('#chk-all').click(function () {
                    if ($(this).is(':checked')) {
                        $('#responsiveTable').find('.chk-row').each(function () {
                            $(this).prop('checked', true);
                            $(this).parent().parent().parent().addClass('selected');
                        });
                    }
                    else {
                        $('#responsiveTable').find('.chk-row').each(function () {
                            $(this).prop('checked', false);
                            $(this).parent().parent().parent().removeClass('selected');
                        });
                    }
                });
            });
        </script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

        

    </body>

    <!-- Mirrored from minetheme.com/Endless1.5.1/table.php by HTTrack Website Copier/3.x [XR&CO'2010], Mon, 05 Jan 2015 15:17:14 GMT -->
</html>
