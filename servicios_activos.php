<?php
/*
  -----------------------------------------------------------------------
  DATOS DE CONTACTO
  -----------------------------------------------------------------------
  Desarrollador: Victor Manuel Diaz Galdames.
  Nivel Profesional: Ingeniero Informático
  Número Contacto (Móvil): +5699491896
  Correo Eletrónico: victordiazgaldames@gmail.com
  Curriculum Vitae: http://victordiaz.nfconnection.cl/
  Nacionalidad: Chileno
  Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
  -----------------------------------------------------------------------
  DATOS DE JEFE A CARGO
  -----------------------------------------------------------------------
  Nombre: Nelson Horacio Guzman Mora.
  Número Contacto (Móvil): +56998289358
  Correo Eletrónico: nguzman@entel.cl
  Cargo: Jefe Operación Plataformas
 */
?>
<?php
//SE INSTANCIA LA CLASE DE LA BASE DATOS
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->        
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->

            <div id="main-container">
                <div class="padding-md">
                    <div class="panel panel-default table-responsive">
                        <div class="panel-heading">
                            Data Table
                            <span class="label label-info pull-right"><?php echo $count_soportes = $db->single("SELECT COUNT(*) FROM Servicios"); ?> Registro(s)</span>
                        </div>
                        <div class="padding-md clearfix">                                                       
                            <table class="table table-striped table-bordered" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Proveedores </th>
                                        <!--<th>Proveedor N°2 Aplicativo</th>-->
                                        <th>Responsables</th>
                                        <th>Monitoreo</th><!-- Ticket Verde -->
                                        <th>Respaldo</th><!-- Ticket Verde -->
                                        <th>Más Información</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //##########################################################################
                                    //LLENADO DEL SELECT
                                    $datos_sql_select_servicios = $db->query("SELECT * FROM Servicios");

                                    foreach ($datos_sql_select_servicios as $row_serv):
                                        //$datos_sql_select_encargados = $db->query("SELECT * FROM Encargados WHERE Servicios_serv_id = '" . $row_serv["serv_id"] . "' ORDER BY encarga_dpto, encarga_escalamiento ASC");
                                        $serv_id = $row_serv["serv_id"];
                                        $NombreServicio = $row_serv["serv_nombre"];
                                        $AliasServicio = $row_serv["serv_alias"];                                        
                                        echo '<tr>';
                                        echo '<td>'.$NombreServicio.'</td>';
                                        //Proveedor N°1 Infra 
                                        echo '<td>';
                                        $Provedores = $db->query("SELECT * FROM "
                                                . "Proveedores P, "
                                                . "ResponsProveeServicio RPS, "
                                                . "Servicios S, "
                                                . "Servicios_has_Componentes SHC, "
                                                . "Componentes C "
                                                . "WHERE RPS.Servicios_serv_id = S.serv_id AND "
                                                . "RPS.Proveedores_provee_id = P.provee_id AND "
                                                . "'$serv_id' = S.serv_id AND "
                                                . "SHC.Componentes_componente_id = C.componente_id ORDER BY S.serv_id DESC");
                                        foreach ($Provedores as $row_prove):
                                            echo $row_prove["provee_nombre_empresa"]."</br>";
                                        endforeach;
                                        echo '</td>';
                                        // ./Proveedor N°1 Infra
                                        //Respansables - Encargados
                                        echo '<td>';
                                        $Encargados = $db->query("SELECT DISTINCT encarga_nombre,encarga_dpto,encarga_escalamiento FROM Encargados E, ResponsProveeServicio RPS WHERE E.Servicios_serv_id = '$serv_id' AND RPS.Servicios_serv_id = E.Servicios_serv_id");
                                        foreach ($Encargados as $row_encargados):
                                            echo $row_encargados["encarga_nombre"].' - '.$row_encargados["encarga_dpto"].' - '.$row_encargados["encarga_escalamiento"].'</br>';    
                                        endforeach;
                                        echo '</td>';
                                        // ./ Respansables - Encargados
                                        echo '<td>';
                                        $Monitoreo = $db->query("SELECT * FROM Servicios_has_Componentes SHC, Componentes C, EquiposComponentes EC WHERE SHC.Servicios_serv_id = '$serv_id' AND SHC.Componentes_componente_id = C.componente_id AND EC.equipos_compon_id = C.EquiposComponentes_equipos_compon_id");
                                        foreach ($Monitoreo as $row_monitoreo):
                                            echo $row_monitoreo["componente_nombre"].' - '.$row_monitoreo["equipo_compon_nombre_tipo"].'</br>';    
                                        endforeach;
                                        echo '</td>';
                                        echo '<td>';
                                        $Respaldos = $db->query("SELECT DISTINCT gestor_respaldo_nombre FROM Servicios S, "
                                                . "Aplicaciones_Respaldos AR, "
                                                . "Gestores_Respaldos GR, "
                                                . "Gestores_Monitoreo GM WHERE "
                                                . "AR.Gestores_Monitoreo_gestor_monitoreo_id = GM.gestor_monitoreo_id AND "
                                                . "AR.Gestores_Respaldos_gestor_respaldo_id = GR.gestor_respaldo_id AND "
                                                . "AR.Servicios_serv_id = '$serv_id'");
                                        foreach ($Respaldos as $row_respaldo):
                                            echo $row_respaldo["gestor_respaldo_nombre"].'</br>';    
                                        endforeach;
                                        echo '</td>';
                                        echo '<td>';
                                        echo '<a href="info_servicio.php?serv_id=' . $serv_id . '" class="main-link"><i class="fa fa-info fa-lg"></i> Ver Más</a>';
                                        echo '</td>';
                                        echo '</tr>';
                                        
                                        /*foreach ($datos_sql_select_encargados as $row_encarga):
                                            $datos_sql_gestores = $db->query("SELECT * FROM Aplicaciones_Respaldos AR, Gestores_Monitoreo GM, Gestores_Respaldos GR WHERE AR.Gestores_Monitoreo_gestor_monitoreo_id = GM.gestor_monitoreo_id AND AR.Gestores_Respaldos_gestor_respaldo_id = GR.gestor_respaldo_id AND AR.Servicios_serv_id = '$serv_id'");
                                            foreach ($datos_sql_select_encargados as $row_encarga2):



                                                echo "<tr>"
                                                . "<td>" . $serv_nombre . "</td>"
                                                . "<td>" . $row_serv["serv_alias"] . "</td>"
                                                . "<td>" . $row_serv["provee_nombre_empresa"] . "</td>";





                                                if ($row_encarga['encarga_nombre'] != " ") {
                                                    echo "<td>" . $row_encarga['encarga_nombre'] . "</td>";
                                                } else {
                                                    echo "<td>Sin Registro</td>";
                                                }
                                                
                                                
                                                echo "<td>" . $row_encarga2["gestor_monitoreo_nombre"] . "</td>"
                                                . "<td>" . $row_serv["serv_tiempo_marcha_blanca"] . "</td>"
                                                . '<td>'
                                                . '<a href="info_servicio.php?serv_id=' . $serv_id . '" class="main-link"><i class="fa fa-info fa-lg"></i> Ver Más</a>'
                                                . '</td>'
                                                . "</tr>";
                                                break;
                                            endforeach;
                                        endforeach;*/
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                        </div><!-- /.padding-md -->
                    </div><!-- /panel -->
                </div><!-- /.padding-md -->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <div class="custom-popup width-100" id="logoutConfirm">
            <div class="padding-md">
                <h4 class="m-top-none"> Do you want to logout?</h4>
            </div>

            <div class="text-center">
                <a class="btn btn-success m-right-sm" href="login.php">Logout</a>
                <a class="btn btn-danger logoutConfirm_close">Cancel</a>
            </div>
        </div>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <!-- Datatable -->
        <script src='js/jquery.dataTables.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#dataTable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "asStripClasses": null
                });
                $('#chk-all').click(function () {
                    if ($(this).is(':checked')) {
                        $('#responsiveTable').find('.chk-row').each(function () {
                            $(this).prop('checked', true);
                            $(this).parent().parent().parent().addClass('selected');
                        });
                    }
                    else {
                        $('#responsiveTable').find('.chk-row').each(function () {
                            $(this).prop('checked', false);
                            $(this).parent().parent().parent().removeClass('selected');
                        });
                    }
                });
            });
        </script>
        <script src="js/menu-active-class.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>



    </body>

    <!-- Mirrored from minetheme.com/Endless1.5.1/table.php by HTTrack Website Copier/3.x [XR&CO'2010], Mon, 05 Jan 2015 15:17:14 GMT -->
</html>
