<?php 
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
?>
<?php
session_start();
$NombrePersona = $_SESSION["datos_usuario_logueado"][0];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][1];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][2];
$TelefonoMovil = $_SESSION["datos_usuario_logueado"][3];
$EmailUsuario = $_SESSION["datos_usuario_logueado"][4];
$Username = $_SESSION["datos_usuario_logueado"][5];
$Contraseña = $_SESSION["datos_usuario_logueado"][6];
$PemisosUsuario = $_SESSION["datos_usuario_logueado"][7];
if (!isset($NombrePersona)) {
    header('Location: login.php');
}

//SE INSTANCIA LA CLASE DE LA BASE DATOS
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?>
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?>
            <!-- ######################################## -->
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.php"> Home</a></li>
                        <li class="active">Ingreso de Infraestructura</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                </br>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <form class="no-margin" id="form_ingreso_infraestructura">
                            <div class="panel-heading">
                                <h4>Ingreso de Componentes</h4>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Componente</label>
                                            <select class="form-control input-sm" id="infra_select_componente" name="infra_select_componente" required="required">
                                                <option value=""> Seleccione Componente</option>
                                                <?php
                                                $datos_sql_select_provee = $db->query("SELECT equipos_compon_id,equipo_compon_nombre_tipo FROM EquiposComponentes");
                                                foreach ($datos_sql_select_provee as $row):
                                                    echo "<option value=" . $row["equipos_compon_id"] . ">" . $row["equipo_compon_nombre_tipo"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                            <div class="row">
                                                <?php if($PemisosRol == "ADMINISTRADOR"){?>
                                                <div class="col-md-6 text-left"><a href="#formModal_AgregarComponente" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-plus"></i> Agregar Componente</a></div>
                                                <div class="col-md-6 text-right"><a href="#formModal_EliminarComponente" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-minus"></i> Eliminar Componente</a></div>
                                                <?php }?>
                                            </div>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Servicio</label>
                                            <select class="form-control input-sm" id="infra_select_servicio_componente" name="infra_select_servicio_componente" required="required">
                                                <option value=""> Seleccione Servicio</option>
                                                <?php
                                                $datos_sql_select_servicios = $db->query("SELECT serv_id,serv_nombre FROM Servicios");
                                                foreach ($datos_sql_select_servicios as $row):
                                                    echo "<option value=" . $row["serv_id"] . ">" . $row["serv_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            </br>
                                            <a href="#" id="btn_agregar_otro_campo1" class="btn btn-link"><i class="fa fa-plus-circle fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <div class="row hide" id="row2">
                                    <div class="col-md-4">                                        
                                    </div>                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Servicio</label>
                                            <select class="form-control input-sm" id="infra_select_servicio_componente2" name="infra_select_servicio_componente2" required="required">
                                                <option value=""> Seleccione Servicio</option>
                                                <?php
                                                foreach ($datos_sql_select_servicios as $row):
                                                    echo "<option value=" . $row["serv_id"] . ">" . $row["serv_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            </br>
                                            <a href="#" id="btn_agregar_otro_campo2" class="btn btn-link"><i class="fa fa-plus-circle fa-2x"></i></a>
                                            <a href="#" id="btn_quitar_otro_campo2" class="btn btn-link"><i class="fa fa-minus-circle fa-2x"></i></a>                                            
                                        </div>                                          
                                    </div>
                                </div>
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <div class="row hide" id="row3">
                                    <div class="col-md-4">                                        
                                    </div>                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Servicio</label>
                                            <select class="form-control input-sm" id="infra_select_servicio_componente3" name="infra_select_servicio_componente3" required="required">
                                                <option value=""> Seleccione Servicio</option>
                                                <?php
                                                foreach ($datos_sql_select_servicios as $row):
                                                    echo "<option value=" . $row["serv_id"] . ">" . $row["serv_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            </br>
                                            <a href="#" id="btn_agregar_otro_campo3" class="btn btn-link"><i class="fa fa-plus-circle fa-2x"></i></a>
                                            <a href="#" id="btn_quitar_otro_campo3" class="btn btn-link"><i class="fa fa-minus-circle fa-2x"></i></a>                                            
                                        </div>                                          
                                    </div>
                                </div>
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <div class="row hide" id="row4">
                                    <div class="col-md-4">                                        
                                    </div>                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Servicio</label>
                                            <select class="form-control input-sm" id="infra_select_servicio_componente4" name="infra_select_servicio_componente4" required="required">
                                                <option value=""> Seleccione Servicio</option>
                                                <?php
                                                foreach ($datos_sql_select_servicios as $row):
                                                    echo "<option value=" . $row["serv_id"] . ">" . $row["serv_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            </br>
                                            <a href="#" id="btn_agregar_otro_campo4" class="btn btn-link"><i class="fa fa-plus-circle fa-2x"></i></a>
                                            <a href="#" id="btn_quitar_otro_campo4" class="btn btn-link"><i class="fa fa-minus-circle fa-2x"></i></a>                                            
                                        </div>                                          
                                    </div>
                                </div>
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <div class="row hide" id="row5">
                                    <div class="col-md-4">                                        
                                    </div>                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Servicio</label>
                                            <select class="form-control input-sm" id="infra_select_servicio_componente5" name="infra_select_servicio_componente5" required="required">
                                                <option value=""> Seleccione Servicio</option>
                                                <?php
                                                foreach ($datos_sql_select_servicios as $row):
                                                    echo "<option value=" . $row["serv_id"] . ">" . $row["serv_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            </br>
                                            <a href="#" id="btn_agregar_otro_campo5" class="btn btn-link"><i class="fa fa-plus-circle fa-2x"></i></a>
                                            <a href="#" id="btn_quitar_otro_campo5" class="btn btn-link"><i class="fa fa-minus-circle fa-2x"></i></a>                                            
                                        </div>                                          
                                    </div>
                                </div>
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <div class="row hide" id="row6">
                                    <div class="col-md-4">                                        
                                    </div>                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Servicio</label>
                                            <select class="form-control input-sm" id="infra_select_servicio_componente6" name="infra_select_servicio_componente6" required="required">
                                                <option value=""> Seleccione Servicio</option>
                                                <?php
                                                foreach ($datos_sql_select_servicios as $row):
                                                    echo "<option value=" . $row["serv_id"] . ">" . $row["serv_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            </br>
                                            <a href="#" id="btn_quitar_otro_campo6" class="btn btn-link"><i class="fa fa-minus-circle fa-2x"></i></a>                                            
                                        </div>                                          
                                    </div>
                                </div>
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Marca Componente</label>
                                            <input type="text" placeholder="" id="infra_marca_componente" name="infra_marca_componente" class="form-control input-sm" >
                                        </div><!-- /form-group -->
                                    </div>                                   
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Nombre Componente</label>
                                            <input type="text" placeholder="" id="infra_nombre_componente" name="infra_nombre_componente" class="form-control input-sm" >
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Modelo Componente</label>
                                            <input type="text" placeholder="" id="infra_modelo_componente" name="infra_modelo_componente" class="form-control input-sm" >
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">N° de Serie</label>                                            
                                            <input type="text" placeholder="" id="infra_num_serie" name="infra_num_serie" class="form-control input-sm" >
                                        </div><!-- /form-group -->
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Sistema Operativo</label>
                                            <input type="text" placeholder="" id="infra_sistema_operativo" name="infra_sistema_operativo" class="form-control input-sm" >
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Aplicaciones</label>
                                            <textarea class="form-control input-sm" id="infra_aplicaciones" name="infra_aplicaciones" rows="3" placeholder="" maxlength="141" rows="3"></textarea>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">N° de Parte</label>                                            
                                            <input type="text" placeholder="" id="infra_num_parte" name="infra_num_parte" class="form-control input-sm" >
                                        </div><!-- /form-group -->
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Especificaciones Técnicas</label>
                                            <textarea class="form-control input-sm" id="infra_espf_tecnicas" name="infra_espf_tecnicas" rows="3" placeholder="" maxlength="141" rows="3"></textarea>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Especificaciones de Energía</label>
                                            <textarea class="form-control input-sm" id="infra_espf_energia" name="infra_espf_energia" rows="3" placeholder="" maxlength="141" rows="3"></textarea>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Ubicación Física DataCenter</label>                                            
                                            <select class="form-control input-sm" id="infra_ubica_fisica_datacenter" name="infra_ubica_fisica_datacenter" required="required">
                                                <option value=""> Seleccione Ubicación</option>
                                                <option value="CDLV Sala 1.1.1">CDLV Sala 1.1.1</option>
                                                <option value="CDLV Sala 1.2.1">CDLV Sala 1.2.1</option>
                                                <option value="CDLV Sala 2.1.4">CDLV Sala 2.1.4</option>
                                                <option value="CNT Sala Will">CNT Sala Will</option>
                                                <option value="CNT Sala Telrad">CNT Sala Telrad</option>
                                                <option value="CNT Sala MPLS">CNT Sala MPLS</option>
                                                <option value="CNT Sala Internet Sur">CNT Sala Internet Sur</option>
                                                <option value="CNT Zócalo">CNT Zócalo</option>
                                            </select>
                                        </div><!-- /form-group -->
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Garantía Proveedor</label>
                                            <input type="text" placeholder="" id="infra_garantia_provee" name="infra_garantia_provee" class="form-control input-sm" >
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Etiquetado Físico</label>
                                            <input type="text" placeholder="" id="infra_etiqueta_fisico" name="infra_etiqueta_fisico" class="form-control input-sm" >
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Ubicación Física Detalle</label>                                            
                                            <textarea class="form-control input-sm" id="infra_ubica_fisica_detalle" name="infra_ubica_fisica_detalle" rows="3" placeholder="" maxlength="141" rows="3"></textarea>
                                        </div><!-- /form-group -->
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Garantía Detalle </label>
                                            <textarea class="form-control input-sm" id="infra_garantia_detalle" name="infra_garantia_detalle" rows="3" placeholder="" maxlength="141" rows="3"></textarea>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Fecha Termino Garantía</label>
                                            <div class="input-group">
                                                <input type="text" value="" id="infra_fecha_termino_garantia" name="infra_fecha_termino_garantia" class="form-control input-sm" placeholder="">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Usuarios y contraseñas</label>                                            
                                            <textarea class="form-control input-sm" id="infra_usuarios_contraseñas" name="infra_usuarios_contraseñas" rows="3" placeholder="" maxlength="141" rows="3"></textarea>
                                        </div><!-- /form-group -->
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Diagrama de Red (Formatos de Imágenes Soportados .png y .jpg)</label>
                                            <input type="file" multiple="multiple" id="infra_diagrama_img_red" name="infra_diagrama_img_red">
                                        </div><!-- /form-group -->
                                    </div>                                  
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_ingreso_infraestructura"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button type="submit" class="btn btn-info">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- /panel -->
                </div><!-- /.col-->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->
        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
        <!-- Logout confirmation -->
        <?php require_once './includes/confirmation.logout.inc.php'; ?>
        <!-- ########################################################### -->
        <form id="form_Insert_AgregaComponente" method="POST">
            <div class="modal fade" id="formModal_AgregarComponente">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <label class="control-label">Nombre Componente</label>                                            
                            <input type="text" placeholder="ej: Servidor, Switch, Router, Librería de cintas, etc" id="infra_tipo_componente" name="infra_tipo_componente" class="form-control input-sm" >
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left" id="msj_respuesta_ingreso_componente">
                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>                            
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <!-- Le javascript-->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>      
        <script src="js/jquery.validate.min.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>
        <script>
            $(document).ready(function () {
                $("textarea").keydown(function (e) {
                        if (e.keyCode === 9) { // tab was pressed
                            // get caret position/selection
                            var start = this.selectionStart;
                            end = this.selectionEnd;

                            var $this = $(this);

                            // set textarea value to: text before caret + tab + text after caret
                            $this.val($this.val().substring(0, start) + "\t" + $this.val().substring(end));

                            // put caret at right position again
                            this.selectionStart = this.selectionEnd = start + 1;

                            // prevent the focus lose
                            return false;
                        }
                    });
            });
        </script>
        <script src='js/datepicker_js/bootstrap-datepicker.js'></script>
        <script src='js/datepicker_js/locales/bootstrap-datepicker.es.js'></script>
        <script>
            $(document).ready(function () {
                $('#infra_fecha_termino_garantia').datepicker({
                    language: "es",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    todayHighlight: true
                });
            });
        </script>
        <script src='js/chosen.jquery.min.js'></script><!-- Mask-input --> 
        <script src='js/jquery.maskedinput.min.js'></script><!-- Datepicker -->
        <!--<script src='js/bootstrap-datepicker.min.js'></script>--><!-- Timepicker -->
        <script src='js/bootstrap-timepicker.min.js'></script><!-- Slider -->
        <script src='js/bootstrap-slider.min.js'></script><!-- Tag input -->
        <script src='js/jquery.tagsinput.min.js'></script><!-- WYSIHTML5 -->
        <script src='js/wysihtml5-0.3.0.min.js'></script>
        <script src='js/uncompressed/bootstrap-wysihtml5.js'></script><!-- Dropzone -->
        <script src='js/dropzone.min.js'></script><!-- Modernizr -->
        <script src='js/modernizr.min.js'></script><!-- Pace -->
        <script src='js/pace.min.js'></script><!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script><!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script><!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script><!-- Endless -->
        <script src="js/endless/endless_form.js"></script>
        <script src="js/endless/endless.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#btn_agregar_otro_campo1').click(function () {
                    $("#row2").removeClass("hide");
                });                
                $('#btn_agregar_otro_campo2').click(function () {
                    $("#row3").removeClass("hide");
                });
                $('#btn_agregar_otro_campo3').click(function () {
                    $("#row4").removeClass("hide");
                });
                $('#btn_agregar_otro_campo4').click(function () {
                    $("#row5").removeClass("hide");
                });
                $('#btn_agregar_otro_campo5').click(function () {
                    $("#row6").removeClass("hide");
                });
                $('#btn_quitar_otro_campo2').click(function () {
                    $("#row2").addClass("hide");
                });
                $('#btn_quitar_otro_campo3').click(function () {
                    $("#row3").addClass("hide");
                });
                $('#btn_quitar_otro_campo4').click(function () {
                    $("#row4").addClass("hide");
                });
                $('#btn_quitar_otro_campo4').click(function () {
                    $("#row4").addClass("hide");
                });
                $('#btn_quitar_otro_campo5').click(function () {
                    $("#row5").addClass("hide");
                });
                $('#btn_quitar_otro_campo6').click(function () {
                    $("#row6").addClass("hide");
                });
            });
        </script>
    </body>
</html>
