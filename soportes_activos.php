<?php
/*
  -----------------------------------------------------------------------
  DATOS DE CONTACTO
  -----------------------------------------------------------------------
  Desarrollador: Victor Manuel Diaz Galdames.
  Nivel Profesional: Ingeniero Informático
  Número Contacto (Móvil): +5699491896
  Correo Eletrónico: victordiazgaldames@gmail.com
  Curriculum Vitae: http://victordiaz.nfconnection.cl/
  Nacionalidad: Chileno
  Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
  -----------------------------------------------------------------------
  DATOS DE JEFE A CARGO
  -----------------------------------------------------------------------
  Nombre: Nelson Horacio Guzman Mora.
  Número Contacto (Móvil): +56998289358
  Correo Eletrónico: nguzman@entel.cl
  Cargo: Jefe Operación Plataformas
 */
?>
<?php
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->        
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div class="padding-md">
                    <div class="panel panel-default table-responsive">
                        <div class="panel-heading">
                            Todos los Soportes
                            <span class="label label-info pull-right"><?php echo $count_soportes = $db->single("SELECT COUNT(*) FROM Soportes"); ?> Registro(s)</span>
                        </div>
                        <div class="padding-md clearfix">
                            <table class="table table-striped" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>Servicio Asociado</th>
                                        <th>Proveedor Asociado</th>
                                        <th>Alias Proveedor</th>
                                        <th>Fecha Termino Soporte</th>
                                        <th>Sistema Tickets</th>
                                        <th>Tipo Asistencia</th>
                                        <th>Tipo Soporte</th>
                                        <th>Más Información</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //LLENADO DEL SELECT
                                    $datos_sql_select_soporte = $db->query("SELECT *,"
                                            . "date_format(soporte_fecha_termino,'%d-%m-%Y') as soporte_fecha_termino "
                                            . "FROM Soportes");
                                    foreach ($datos_sql_select_soporte as $row_soporte):
                                        $soporte_id = $row_soporte["soporte_id"];
                                        $Nombre_servicio_sql = $db->single("SELECT serv_nombre FROM Servicios WHERE serv_id = '" . $row_soporte["Servicios_serv_id"] . "'");
                                        $Nombre_proveedores_sql = $db->single("SELECT provee_nombre_empresa FROM Proveedores WHERE provee_id = '" . $row_soporte["Proveedores_provee_id"] . "'");
                                        $Alias_proveedores_sql = $db->single("SELECT provee_nombre_fantasia FROM Proveedores WHERE provee_id = '" . $row_soporte["Proveedores_provee_id"] . "'");
                                        echo '<tr>'
                                        . '<td>' . $Nombre_servicio_sql . '</td>'
                                        . '<td>' . $Nombre_proveedores_sql . '</td>'
                                        . '<td>' . $Alias_proveedores_sql . '</td>'
                                        . '<td class="text-center">' . $row_soporte['soporte_fecha_termino'] . '</td>';
                                        if ($row_soporte["soporte_sistema_tickets"] == "") {
                                            echo '<td>Sin Sistema Tickets</td>';
                                        } else {
                                            echo '<td><a href="' . $row_soporte["soporte_sistema_tickets"] . '" class="btn-link" target="_blank"> ' . $row_soporte["soporte_sistema_tickets"] . '</a></td>';
                                        }
                                        echo '<td>';
                                        if ($row_soporte["soporte_tipo_asistencia_presencial"] != "" AND $row_soporte["soporte_tipo_asistencia_remoto"] != "") {
                                            $TipoAsitencia = "Presencial - Remoto";
                                        } elseif ($row_soporte["soporte_tipo_asistencia_presencial"] != "") {
                                            $TipoAsitencia = "Presencial";
                                        } elseif ($row_soporte["soporte_tipo_asistencia_remoto"] != "") {
                                            $TipoAsitencia = "Remoto";
                                        }
                                        echo $TipoAsitencia;
                                        echo '</td>';
                                        echo '<td>';
                                        if ($row_soporte["soporte_tipo_reactivo"] != "" AND $row_soporte["soporte_tipo_proactivo"] != "" AND $row_soporte["soporte_tipo_evolutivo"] != "") {
                                            $TipoSoporte = "Reactivo - Proactivo - Evolutivo";
                                        } elseif ($row_soporte["soporte_tipo_reactivo"] != "" AND $row_soporte["soporte_tipo_evolutivo"] != "") {
                                            $TipoSoporte = "Reactivo - Evolutivo";
                                        } elseif ($row_soporte["soporte_tipo_proactivo"] != "" AND $row_soporte["soporte_tipo_evolutivo"] != "") {
                                            $TipoSoporte = "Proactivo - Evolutivo";
                                        } elseif ($row_soporte["soporte_tipo_reactivo"] != "" AND $row_soporte["soporte_tipo_proactivo"] != "") {
                                            $TipoSoporte = "Reactivo - Proactivo";
                                        } elseif ($row_soporte["soporte_tipo_reactivo"] != "") {
                                            $TipoSoporte = $row_soporte["soporte_tipo_reactivo"];
                                        } elseif ($row_soporte["soporte_tipo_proactivo"] != "") {
                                            $TipoSoporte = $row_soporte["soporte_tipo_proactivo"];
                                        } elseif ($row_soporte["soporte_tipo_evolutivo"] != "") {
                                            $TipoSoporte = $row_soporte["soporte_tipo_evolutivo"];
                                        }
                                        echo $TipoSoporte;
                                        echo '</td>';

                                        echo '<td class="text-center"><a href="info_soporte.php?soporte_id=' . $row_soporte["soporte_id"] . '" class="main-link"><i class="fa fa-info fa-lg"></i> Ver Más</a></td>'
                                        . '</tr>';
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                        </div><!-- /.padding-md -->
                    </div><!-- /panel -->
                </div><!-- /.padding-md -->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/confirmation.logout.inc.php'; ?> 
        <!-- ######################################## -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/menu-active-class.js"></script>

        <!-- Datatable -->
        <script src='js/jquery.dataTables.min.js'></script>	
        <script type="text/javascript">
            $(document).ready(function () {
                $('#dataTable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "asStripClasses": null
                });
                $('#chk-all').click(function () {
                    if ($(this).is(':checked')) {
                        $('#responsiveTable').find('.chk-row').each(function () {
                            $(this).prop('checked', true);
                            $(this).parent().parent().parent().addClass('selected');
                        });
                    }
                    else {
                        $('#responsiveTable').find('.chk-row').each(function () {
                            $(this).prop('checked', false);
                            $(this).parent().parent().parent().removeClass('selected');
                        });
                    }
                });
            });
        </script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>



    </body>

    <!-- Mirrored from minetheme.com/Endless1.5.1/table.php by HTTrack Website Copier/3.x [XR&CO'2010], Mon, 05 Jan 2015 15:17:14 GMT -->
</html>
