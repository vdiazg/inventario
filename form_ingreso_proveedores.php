<?php 
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?>
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?>
            <!-- ######################################## -->
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.php"> Home</a></li>
                        <li class="active">Ingreso de Proveedor</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                </br>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <form class="no-margin" id="form_ingreso_proveedores" action="modelo/ingreso_proveedor_BD.php" name="form_ingreso_proveedores" enctype="multipart/form-data" method="POST">
                            <div class="panel-heading">
                                <h4>Información del Proveedor</h4>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Nombre de la Empresa</label>
                                            <input type="text" placeholder="Razón social" id="proveedor_nombre_empresa" name="proveedor_nombre_empresa" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">RUT</label>
                                            <input type="text" placeholder="Rol Único Tributario" id="proveedor_rut" name="proveedor_rut" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Nombre de Fantasía o Alias</label>
                                            <input type="text" placeholder="Por el cuál es conocida la Empresa" id="proveedor_alias_fantasia" name="proveedor_alias_fantasia" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div><!-- /form-group -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">URL Sitio Web</label>
                                            <input type="text" placeholder="ej: http://www.entel.cl/" id="proveedor_url_sitio_web" name="proveedor_url_sitio_web" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Dirección</label>
                                            <input type="text" placeholder="Dirección física Oficina Central" id="proveedor_direccion" name="proveedor_direccion" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">N° Mesa de Ayuda</label>                                            
                                            <input type="text" placeholder="Teléfono Principal, ej: +56973993813" id="proveedor_n_mesa_ayuda" name="proveedor_n_mesa_ayuda" class="form-control input-sm" data-required="true" data-minlength="8">
                                        
                                        </div><!-- /form-group -->
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label">Documento Oficial</label>
                                                <input type="file" id="proveedor_documento_oficial" name="proveedor_documento_oficial">
                                            </div>
                                        </div>
                                    </div>
                                <!--
                                    <div class="col-md-6">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label">Logo Empresa</label>
                                                <input type="file" id="proveedor_img" name="proveedor_img">
                                            </div>
                                        </div>                                    
                                    </div>-->
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Nota</label>
                                            <textarea class="form-control input-sm" id="proveedor_nota" name="proveedor_nota" rows="3" placeholder="Solo se Permiten 140 Caracteres" maxlength="141" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">                                        
                                        <div class="col-md-8 text-left">
                                            <div id="msj_respuesta_ingreso_provee"></div>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <button type="submit" class="btn btn-info">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div><!-- /panel -->
                </div><!-- /.col-->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/confirmation.logout.inc.php'; ?> 
        <!-- ######################################## -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>      
        <!-- JQuery Validate - Validacion del Formulario -->
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>
        <script>
            $(document).ready(function () {
                $("textarea").keydown(function (e) {
                        if (e.keyCode === 9) { // tab was pressed
                            // get caret position/selection
                            var start = this.selectionStart;
                            end = this.selectionEnd;

                            var $this = $(this);

                            // set textarea value to: text before caret + tab + text after caret
                            $this.val($this.val().substring(0, start) + "\t" + $this.val().substring(end));

                            // put caret at right position again
                            this.selectionStart = this.selectionEnd = start + 1;

                            // prevent the focus lose
                            return false;
                        }
                    });
            });
        </script>
        <script src="js/menu-active-class.js"></script>

        <!-- Chosen -->
        <script src='js/chosen.jquery.min.js'></script>	

        <!-- Mask-input --> 
        <script src='js/jquery.maskedinput.min.js'></script>	

        <!-- Datepicker -->
        <script src='js/bootstrap-datepicker.min.js'></script>	

        <!-- Timepicker -->
        <script src='js/bootstrap-timepicker.min.js'></script>	

        <!-- Slider -->
        <script src='js/bootstrap-slider.min.js'></script>

        <!-- Tag input -->
        <script src='js/jquery.tagsinput.min.js'></script>	

        <!-- WYSIHTML5 -->
        <script src='js/wysihtml5-0.3.0.min.js'></script>	
        <script src='js/uncompressed/bootstrap-wysihtml5.js'></script>

        <!-- Dropzone -->
        <script src='js/dropzone.min.js'></script>	

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless_form.js"></script>
        <script src="js/endless/endless.js"></script>


    </body>
</html>
