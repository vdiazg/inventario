<?php
/*
  -----------------------------------------------------------------------
  DATOS DE CONTACTO
  -----------------------------------------------------------------------
  Desarrollador: Victor Manuel Diaz Galdames.
  Nivel Profesional: Ingeniero Informático
  Número Contacto (Móvil): +5699491896
  Correo Eletrónico: victordiazgaldames@gmail.com
  Curriculum Vitae: http://victordiaz.nfconnection.cl/
  Nacionalidad: Chileno
  Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
  -----------------------------------------------------------------------
  DATOS DE JEFE A CARGO
  -----------------------------------------------------------------------
  Nombre: Nelson Horacio Guzman Mora.
  Número Contacto (Móvil): +56998289358
  Correo Eletrónico: nguzman@entel.cl
  Cargo: Jefe Operación Plataformas
 */
?>
<?php
//##########################################################################
//SE INSTANCIA LA CLASE DE LA BASE DATOS
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.php"> Home</a></li>
                        <li class="active">Ingreso de Monitoreo y Respaldos</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                </br>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <form class="no-margin" id="form_ingreso_monitoreo_respaldo"  role="form" method="post">
                            <div class="panel-heading">
                                <h4>Monitoreo y Respaldo</h4>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Servicio</label>
                                            <select class="form-control input-sm" id="selec_servicio_monitoreo_respaldo" name="selec_servicio_monitoreo_respaldo" required="required">
                                                <option value=""> Seleccione Servicio</option>
                                                <?php
                                                $datos_sql_select_provee = $db->query("SELECT * FROM Servicios");
                                                foreach ($datos_sql_select_provee as $row):
                                                    echo "<option value=" . $row["serv_id"] . ">" . $row["serv_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="alert alert-warning">
                                            <strong>Atención!</strong> Verificar que "Gestores de Monitoreo" y "Gestores de Respaldos" tengan opciones a seleccionar antes de ingresar Información en Áreas de Texto.
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Gestores de Monitoreo</label>
                                            <select class="form-control input-sm" id="selec_gestor_monitoreo" name="selec_gestor_monitoreo" required="required">
                                                <option value=""> Seleccione Gestores de Monitoreo</option>
                                                <?php
                                                $datos_sql_select_gestor_monitoreo = $db->query("SELECT * FROM Gestores_Monitoreo");
                                                foreach ($datos_sql_select_gestor_monitoreo as $row):
                                                    echo "<option value=" . $row["gestor_monitoreo_id"] . ">" . $row["gestor_monitoreo_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                            <div class="row">
                                                <div class="col-md-6 text-left"><a href="#formModal_AgregarGestorMonitoreo" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-plus"></i> Agregar Gestor Monitoreo</a></div>
                                                <div class="col-md-6 text-right"><a href="#formModal_EliminarGestorMonitoreo" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-minus"></i> Eliminar Gestor Monitoreo</a></div>
                                            </div>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Elementos a Monitoriar</label>
                                            <textarea class="form-control" rows="3" id="elementos_monitoriar" name="elementos_monitoriar"></textarea>
                                        </div><!-- /form-group -->
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Gestores de Respaldos</label>
                                            <select class="form-control input-sm" id="selec_gestor_respaldo" name="selec_gestor_respaldo" required="required">
                                                <option value=""> Seleccione Gestores de Respaldos</option>
                                                <?php
                                                $datos_sql_select_gestor_respaldo = $db->query("SELECT * FROM Gestores_Respaldos");
                                                foreach ($datos_sql_select_gestor_respaldo as $row):
                                                    echo "<option value=" . $row["gestor_respaldo_id"] . ">" . $row["gestor_respaldo_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                            <div class="row">
                                                <div class="col-md-6 text-left"><a href="#formModal_AgregarGestorRespaldo" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-plus"></i> Agregar Gestor Respaldo</a></div>
                                                <div class="col-md-6 text-right"><a href="#formModal_EliminarGestorRespaldo" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-minus"></i> Eliminar Gestor Respaldo</a></div>
                                            </div>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Elementos a Respaldar</label>
                                            <textarea class="form-control" rows="3" id="elemento_respaldar" name="elemento_respaldar"></textarea>
                                        </div><!-- /form-group -->
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Usuarios y Contraseña de Aplicación</label>
                                            <textarea class="form-control" rows="5" id="usuario_contrasena_aplicacion" name="usuario_contrasena_aplicacion"></textarea>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Usuarios y Contraseña de BD</label>
                                            <textarea class="form-control" rows="5" id="usuario_contrasena_BD" name="usuario_contrasena_BD"></textarea>
                                        </div><!-- /form-group -->
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Rutinas de Mantenimiento</label>
                                            <textarea class="form-control" rows="3" id="rutinas_matenimiento" name="rutinas_matenimiento"></textarea>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Problemas Recurrentes</label>
                                            <textarea class="form-control" rows="3" id="problema_recurrente" name="problema_recurrente"></textarea>
                                        </div><!-- /form-group -->
                                    </div>
                                </div>                                
                                <div class="panel-footer">
                                    <div class="row">                                        
                                        <div class="col-md-8 text-left">
                                            <div id="msj_respuesta_respaldo_monitoreo"></div>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <button type="submit" class="btn btn-info">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div><!-- /panel -->
                </div><!-- /.col-->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/confirmation.logout.inc.php'; ?>
        <!-- ########################################################### -->
        <form id="form_Insert_AgregaGestorMonitoreo" method="POST">
            <div class="modal fade" id="formModal_AgregarGestorMonitoreo">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <label class="control-label">Nombre Gestor Monitoreo</label>                                            
                            <input type="text" placeholder="Ej: Groundwork, OmView, Cacti, MRTG" id="GestorMonitoreo_nombre" name="GestorMonitoreo_nombre" class="form-control input-sm" >
                            </br>
                            <label class="control-label">Descripción</label>
                            <textarea class="form-control" rows="3" id="GestorMonitoreo_descripcion" name="GestorMonitoreo_descripcion"></textarea>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left" id="msj_respuesta_ingreso_GestorMonitoreo">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>                            
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_Insert_AgregaGestorRespaldo" method="POST">
            <div class="modal fade" id="formModal_AgregarGestorRespaldo">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <label class="control-label">Nombre Gestor Respaldo</label>                                            
                            <input type="text" placeholder="Ej: Networker, Netbackup" id="GestorRespaldo_nombre" name="GestorRespaldo_nombre" class="form-control input-sm" >
                            </br>
                            <label class="control-label">Descripción</label>
                            <textarea class="form-control" rows="3" id="GestorRespaldo_descripcion" name="GestorRespaldo_descripcion"></textarea>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left" id="msj_respuesta_ingreso_GestorRespaldo">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>                            
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>      
        <!-- JQuery Validate - Validacion del Formulario -->
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>
        <script>
            $(document).ready(function () {
                $("textarea").keydown(function (e) {
                        if (e.keyCode === 9) { // tab was pressed
                            // get caret position/selection
                            var start = this.selectionStart;
                            end = this.selectionEnd;

                            var $this = $(this);

                            // set textarea value to: text before caret + tab + text after caret
                            $this.val($this.val().substring(0, start) + "\t" + $this.val().substring(end));

                            // put caret at right position again
                            this.selectionStart = this.selectionEnd = start + 1;

                            // prevent the focus lose
                            return false;
                        }
                    });
            });
        </script>
        <script src="js/menu-active-class.js"></script>
        <!-- Chosen -->
        <script src='js/chosen.jquery.min.js'></script>	

        <!-- Mask-input --> 
        <script src='js/jquery.maskedinput.min.js'></script>	

        <!-- Datepicker -->
        <script src='js/bootstrap-datepicker.min.js'></script>	

        <!-- Timepicker -->
        <script src='js/bootstrap-timepicker.min.js'></script>	

        <!-- Slider -->
        <script src='js/bootstrap-slider.min.js'></script>

        <!-- Tag input -->
        <script src='js/jquery.tagsinput.min.js'></script>	

        <!-- WYSIHTML5 -->
        <script src='js/wysihtml5-0.3.0.min.js'></script>	
        <script src='js/uncompressed/bootstrap-wysihtml5.js'></script>
        <!-- Dropzone -->
        <script src='js/dropzone.min.js'></script>	

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless_form.js"></script>
        <script src="js/endless/endless.js"></script>


    </body>
</html>
