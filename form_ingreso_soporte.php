<?php 
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?>
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?>
            <!-- ######################################## -->
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.php"> Home</a></li>
                        <li class="active">Ingreso de Soporte</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                </br>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <form class="no-margin" id="form_ingreso_soporte">
                            <div class="panel-heading">
                                <h4>Ingreso de Soporte</h4>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Proveedor</label>
                                            <?php
                                            //##########################################################################
                                            //SE INSTANCIA LA CLASE DE LA BASE DATOS
                                            require_once './controlador/Db.class.php';
                                            $db = new Db();
                                            //LLENADO DEL SELECT
                                            $datos_sql_select_provee = $db->query("SELECT * FROM Proveedores");
                                            ?>
                                            <select class="form-control input-sm" id="selec_proveedor_soporte" name="selec_proveedor_soporte" required="required">
                                                <option value=""> Seleccione Proveedor</option>
                                                <?php
                                                foreach ($datos_sql_select_provee as $row):
                                                    echo "<option value=" . $row["provee_id"] . ">" . $row["provee_nombre_fantasia"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Servicio</label>
                                            <?php
                                            //##########################################################################
                                            //SE INSTANCIA LA CLASE DE LA BASE DATOS
                                            require_once './controlador/Db.class.php';
                                            $db = new Db();
                                            //LLENADO DEL SELECT
                                            $datos_sql_select_servicios = $db->query("SELECT * FROM Servicios");
                                            ?>
                                            <select class="form-control input-sm" id="selec_servicio_soporte" name="selec_servicio_soporte" required="required">
                                                <option value=""> Seleccione Servicio</option>
                                                <?php
                                                foreach ($datos_sql_select_servicios as $row):
                                                    echo "<option value=" . $row["serv_id"] . ">" . $row["serv_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">                                   
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Fecha Inicio Soporte</label>
                                            <div class="input-group">
                                                <input type="text" value="" id="proveedor_fecha_inicio_soporte" name="proveedor_fecha_inicio_soporte" class="form-control input-sm" placeholder="">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>  
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Fecha Termino Soporte</label>
                                            <div class="input-group">
                                                <input type="text" value="" id="proveedor_fecha_termino_soporte" name="proveedor_fecha_termino_soporte" class="form-control input-sm" placeholder="">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>  
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Tareas Incluidas</label>
                                            <textarea class="form-control input-sm" id="proveedor_tarea_incluida" name="proveedor_tarea_incluida" rows="3" placeholder="Solo se Permiten 140 Caracteres" maxlength="141" rows="3"></textarea>
                                        </div><!-- /form-group -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label">Sistema de Tickets</label>
                                            <input type="text" placeholder="URL" class="form-control input-sm" id="proveedor_sistema_tickets" name="proveedor_sistema_tickets">
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Incluye Respuestos</label>
                                            <div class="col-lg-10">
                                                <label class="label-radio inline">
                                                    <input type="radio" id="proveedor_incluye_respuestos" name="proveedor_incluye_respuestos" value="SI">
                                                    <span class="custom-radio"></span>
                                                    SI
                                                </label>
                                                <label class="label-radio inline">
                                                    <input type="radio" id="proveedor_incluye_respuestos" name="proveedor_incluye_respuestos" value="NO">
                                                    <span class="custom-radio"></span>
                                                    NO
                                                </label>
                                            </div><!-- /.col -->
                                        </div><!-- /form-group -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label">Flujo de Soporte</label>
                                            <textarea class="form-control input-sm" rows="3" id="proveedor_flujo_soporte" name="proveedor_flujo_soporte" placeholder="Descripción de Escalamiento"></textarea>
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Tipo del Horario Soporte</label>
                                            <div class="col-lg-10">
                                                <label class="label-radio inline">
                                                    <input type="radio" id="proveedor_tipo_horario_soporte_5x8" name="proveedor_tipo_horario_soporte" value="5x8">
                                                    <span class="custom-radio"></span>
                                                    5x8
                                                </label>
                                                <label class="label-radio inline">
                                                    <input type="radio" id="proveedor_tipo_horario_soporte_7x24" name="proveedor_tipo_horario_soporte" value="7x24">
                                                    <span class="custom-radio"></span>
                                                    7x24
                                                </label>
                                            </div><!-- /.col -->
                                        </div><!-- /form-group -->
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Tiempos de Respuesta</label>
                                            <textarea class="form-control input-sm" rows="3" id="proveedor_tiempo_respuesta" name="proveedor_tiempo_respuesta" placeholder="Tiempos y Criticidad"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Tipo de Asistencia</label>
                                            <div class="col-lg-10">
                                                <label class="label-radio inline">
                                                    <input type="checkbox" id="proveedor_tipo_asistencia" name="proveedor_tipo_asistencia[]" value="Presencial">
                                                    <span class="custom-radio"></span>
                                                    Presencial
                                                </label>
                                                <label class="label-radio inline">
                                                    <input type="checkbox" id="proveedor_tipo_asistencia" name="proveedor_tipo_asistencia[]" value="Remoto">
                                                    <span class="custom-radio"></span>
                                                    Remoto
                                                </label>
                                            </div><!-- /.col -->
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">                                        
                                        <div class="form-group">
                                            <label for="proveedor_tipo_soporte_error" name="proveedor_tipo_soporte_error" id="proveedor_tipo_soporte_error" class="control-label">Tipo de Soporte</label>
                                            <div class="col-lg-10">
                                                <label class="label-radio inline">
                                                    <input type="checkbox" id="proveedor_tipo_soporte" name="proveedor_tipo_soporte[]" value="Reactivo">
                                                    <span class="custom-radio"></span>
                                                    Reactivo
                                                </label>
                                                <label class="label-radio inline">
                                                    <input type="checkbox" id="proveedor_tipo_soporte" name="proveedor_tipo_soporte[]" value="Proactivo">
                                                    <span class="custom-radio"></span>
                                                    Proactivo
                                                </label>
                                                <label class="label-radio inline">
                                                    <input type="checkbox" id="proveedor_tipo_soporte" name="proveedor_tipo_soporte[]" value="Evolutivo">
                                                    <span class="custom-radio"></span>
                                                    Evolutivo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <h5>Horarios de Atención</h5>
                                    <div class="col-md-2">
                                        <h4 class="text-center">Lunes</h4>
                                        <label class="control-label">Hora de Inicio</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_inicio_atencion_lunes" name="proveedor_horario_inicio_atencion_lunes"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                        </br>
                                        <label class="control-label">Hora de Termino</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_termino_atencion_lunes" name="proveedor_horario_termino_atencion_lunes"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <h4 class="text-center">Martes</h4>
                                        <label class="control-label">Hora de Inicio</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_inicio_atencion_martes" name="proveedor_horario_inicio_atencion_martes"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                        </br>
                                        <label class="control-label">Hora de Termino</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_termino_atencion_martes" name="proveedor_horario_termino_atencion_martes"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <h4 class="text-center">Miercoles</h4>
                                        <label class="control-label">Hora de Inicio</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_inicio_atencion_miercoles" name="proveedor_horario_inicio_atencion_miercoles"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                        </br>
                                        <label class="control-label">Hora de Termino</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_termino_atencion_miercoles" name="proveedor_horario_termino_atencion_miercoles"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <h4 class="text-center">Jueves</h4>
                                        <label class="control-label">Hora de Inicio</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_inicio_atencion_jueves" name="proveedor_horario_inicio_atencion_jueves"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                        </br>
                                        <label class="control-label">Hora de Termino</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_termino_atencion_jueves" name="proveedor_horario_termino_atencion_jueves"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <h4 class="text-center">Viernes</h4>
                                        <label class="control-label">Hora de Inicio</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_inicio_atencion_viernes" name="proveedor_horario_inicio_atencion_viernes"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                        </br>
                                        <label class="control-label">Hora de Termino</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_termino_atencion_viernes" name="proveedor_horario_termino_atencion_viernes"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2" id="proveedor_horario_atencion_sabado">
                                        <h4 class="text-center">Sabado</h4>
                                        <label class="control-label">Hora de Inicio</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_inicio_atencion_sabado" name="proveedor_horario_inicio_atencion_sabado"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                        </br>
                                        <label class="control-label">Hora de Termino</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_termino_atencion_sabado" name="proveedor_horario_termino_atencion_sabado"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2" id="proveedor_horario_atencion_domingo">
                                        <h4 class="text-center">Domingo</h4>
                                        <label class="control-label">Hora de Inicio</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_inicio_atencion_domingo" name="proveedor_horario_inicio_atencion_domingo"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                        </br>
                                        <label class="control-label">Hora de Termino</label>
                                        <div class="input-group bootstrap-timepicker">
                                            <input class="timepicker form-control input-sm" type="text" id="proveedor_horario_termino_atencion_domingo" name="proveedor_horario_termino_atencion_domingo"/>
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </br>
                            <div class="panel-footer">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_ingreso_soporte"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button type="submit" class="btn btn-info">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- /panel -->
                </div><!-- /.col-->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php //require_once './includes/confirmation.logout.inc.php'; ?> 
        <!-- ######################################## -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <script src="js/jquery-1.10.2.js"></script><!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>
        <script>
            $(document).ready(function () {
                $("textarea").keydown(function (e) {
                        if (e.keyCode === 9) { // tab was pressed
                            // get caret position/selection
                            var start = this.selectionStart;
                            end = this.selectionEnd;

                            var $this = $(this);

                            // set textarea value to: text before caret + tab + text after caret
                            $this.val($this.val().substring(0, start) + "\t" + $this.val().substring(end));

                            // put caret at right position again
                            this.selectionStart = this.selectionEnd = start + 1;

                            // prevent the focus lose
                            return false;
                        }
                    });
            });
        </script>
        <script src='js/datepicker_js/bootstrap-datepicker.js'></script>
        <script src='js/datepicker_js/locales/bootstrap-datepicker.es.js'></script>
        <script>
            $(document).ready(function () {
                $('#proveedor_fecha_inicio_soporte').datepicker({
                    language: "es",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    todayHighlight: true
                });
                $('#proveedor_fecha_termino_soporte').datepicker({
                    language: "es",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    todayHighlight: true
                });
                $('#proveedor_tipo_horario_soporte_5x8').click(function() {
                    $("#proveedor_horario_atencion_sabado").hide();
                    $("#proveedor_horario_atencion_domingo").hide();
                });
                $('#proveedor_tipo_horario_soporte_7x24').click(function() {
                    $("#proveedor_horario_atencion_sabado").show();
                    $("#proveedor_horario_atencion_domingo").show();
                });
            });
                
        </script>
        <script src='js/chosen.jquery.min.js'></script><!-- Mask-input --> 
        <script src='js/jquery.maskedinput.min.js'></script><!-- Datepicker -->
        <!--<script src='js/bootstrap-datepicker.min.js'></script>--><!-- Timepicker -->
        <script src='js/bootstrap-timepicker.min.js'></script><!-- Slider -->
        <script src='js/bootstrap-slider.min.js'></script><!-- Tag input -->
        <script src='js/jquery.tagsinput.min.js'></script><!-- WYSIHTML5 -->
        <script src='js/wysihtml5-0.3.0.min.js'></script>
        <script src='js/uncompressed/bootstrap-wysihtml5.js'></script><!-- Dropzone -->
        <script src='js/dropzone.min.js'></script><!-- Modernizr -->
        <script src='js/modernizr.min.js'></script><!-- Pace -->
        <script src='js/pace.min.js'></script><!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script><!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script><!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script><!-- Endless -->
        <script src="js/endless/endless_form.js"></script>
        <script src="js/endless/endless.js"></script>
    </body>
</html>
