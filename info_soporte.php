<?php
/*
  -----------------------------------------------------------------------
  DATOS DE CONTACTO
  -----------------------------------------------------------------------
  Desarrollador: Victor Manuel Diaz Galdames.
  Nivel Profesional: Ingeniero Informático
  Número Contacto (Móvil): +5699491896
  Correo Eletrónico: victordiazgaldames@gmail.com
  Curriculum Vitae: http://victordiaz.nfconnection.cl/
  Nacionalidad: Chileno
  Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
  -----------------------------------------------------------------------
  DATOS DE JEFE A CARGO
  -----------------------------------------------------------------------
  Nombre: Nelson Horacio Guzman Mora.
  Número Contacto (Móvil): +56998289358
  Correo Eletrónico: nguzman@entel.cl
  Cargo: Jefe Operación Plataformas
 */
?>
<?php
$soporte_id = $_GET['soporte_id'];
session_start();
$_SESSION["soporte_id"] = $soporte_id;
require_once './controlador/Db.class.php';
$db = new Db();
//LLENADO DEL SELECT
$datos_sql_select_soporte = $db->query("SELECT *,date_format(soporte_fecha_inicio,'%d-%m-%Y') as soporte_fecha_inicio,date_format(soporte_fecha_termino,'%d-%m-%Y') as soporte_fecha_termino  FROM Soportes WHERE soporte_id = '" . $soporte_id . "'");
if ($datos_sql_select_soporte == false) {
    header('Location: ./error404.php');
}
foreach ($datos_sql_select_soporte as $row_soporte):

endforeach;
?>
<!DOCTYPE html>
<html lang="en">    
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>

        <div id="wrapper" class="bg-white preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div class="padding-md">
                    <div class="clearfix">
                        <div class="row text-center">
                            <div class="col-md-12"><h4><strong>Información Detallada de Soporte</strong></h4></div>
                        </div>
                        <hr>
                        <div class="row text-center">
                            <div class="col-md-6" style="border-right: 1px solid #D2D2D2;"><h4 class="m-bottom-xs m-top-xs"><strong>Proveedor Asociado: </strong><?php echo $db->single("SELECT provee_nombre_empresa FROM Servicios S, Proveedores P, Soportes SOPO WHERE '" . $row_soporte["Servicios_serv_id"] . "' = S.serv_id AND P.provee_id = SOPO.Proveedores_provee_id"); ?></h4></div>
                            <div class="col-md-6"><h4 class="m-bottom-xs m-top-xs"><strong>Servicio Asociado: </strong><?php echo $db->single("SELECT serv_nombre FROM Servicios S, Proveedores P, Soportes SOPO WHERE '" . $row_soporte["Servicios_serv_id"] . "' = S.serv_id AND P.provee_id = SOPO.Proveedores_provee_id"); ?></h4></div>
                        </div>
                        <hr>
                        <div class="clearfix">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>Información Detallada</h5>
                                    <span class="text-muted"><strong>Fecha Inicio del Soporte: </strong> <?php echo $row_soporte["soporte_fecha_inicio"]; ?> - <a href="#formModal_UpdateFechaInicioSoporte" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                    <span class="text-muted"><strong>Fecha Termino del Soporte: </strong><?php echo $row_soporte["soporte_fecha_termino"]; ?> - <a href="#formModal_UpdateFechaTerminoSoporte" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                    <span class="text-muted"><strong>Tipo de Horario:</strong> <?php echo $row_soporte["soporte_tipo_horario"]; ?> - <a href="#formModal_UpdateTipoHorarioSoporte" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                    <span class="text-muted"><strong>Sistema Tickets: </strong> <?php
                                        if ($row_soporte["soporte_sistema_tickets"] == "") {
                                            echo 'Sin Sistema Tickets';
                                        } else {
                                            echo $row_soporte["soporte_sistema_tickets"];
                                        }
                                        ?> - <a href="#formModal_UpdateSistemaTickets" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                    <span class="text-muted"><strong>Tipo de Asistencia:</strong> <?php
                                        if ($row_soporte["soporte_tipo_asistencia_presencial"] != "" AND $row_soporte["soporte_tipo_asistencia_remoto"] != "") {
                                            $TipoAsitencia = "Presencial - Remoto";
                                        } elseif ($row_soporte["soporte_tipo_asistencia_presencial"] != "") {
                                            $TipoAsitencia = "Presencial";
                                        } elseif ($row_soporte["soporte_tipo_asistencia_remoto"] != "") {
                                            $TipoAsitencia = "Remoto";
                                        }
                                        echo $TipoAsitencia;
                                        ?> - <a href="#formModal_UpdateTipoAsistencia" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                    <span class="text-muted"><strong>Tipo Soporte:</strong> <?php
                                        if ($row_soporte["soporte_tipo_reactivo"] != "" AND $row_soporte["soporte_tipo_proactivo"] != "" AND $row_soporte["soporte_tipo_evolutivo"] != "") {
                                            $TipoSoporte = "Reactivo - Proactivo - Evolutivo";
                                        } elseif ($row_soporte["soporte_tipo_reactivo"] != "" AND $row_soporte["soporte_tipo_evolutivo"] != "") {
                                            $TipoSoporte = "Reactivo - Evolutivo";
                                        } elseif ($row_soporte["soporte_tipo_proactivo"] != "" AND $row_soporte["soporte_tipo_evolutivo"] != "") {
                                            $TipoSoporte = "Proactivo - Evolutivo";
                                        } elseif ($row_soporte["soporte_tipo_reactivo"] != "" AND $row_soporte["soporte_tipo_proactivo"] != "") {
                                            $TipoSoporte = "Reactivo - Proactivo";
                                        } elseif ($row_soporte["soporte_tipo_reactivo"] != "") {
                                            $TipoSoporte = $row_soporte["soporte_tipo_reactivo"];
                                        } elseif ($row_soporte["soporte_tipo_proactivo"] != "") {
                                            $TipoSoporte = $row_soporte["soporte_tipo_proactivo"];
                                        } elseif ($row_soporte["soporte_tipo_evolutivo"] != "") {
                                            $TipoSoporte = $row_soporte["soporte_tipo_evolutivo"];
                                        }
                                        echo $TipoSoporte;
                                        ?> - <a href="#formModal_UpdateTipoSoporte" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br> </div>
                                <div class="col-md-6">  <h5>Horario Atención - <a href="#formModal_UpdateHorariosAtencion" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5> 
                                    <?php
                                    echo '<span class="text-muted"><strong>Lunes: </strong> ' . $row_soporte["soporte_hora_inicio_atencion_lunes"] . ' - ' . $row_soporte["soporte_hora_termino_atencion_lunes"] . ' Hrs.</span></br>'
                                    . '<span class="text-muted"><strong>Martes: </strong> ' . $row_soporte["soporte_hora_inicio_atencion_martes"] . ' - ' . $row_soporte["soporte_hora_termino_atencion_martes"] . ' Hrs.</span></br>'
                                    . '<span class="text-muted"><strong>Miércoles: </strong> ' . $row_soporte["soporte_hora_inicio_atencion_miercoles"] . ' - ' . $row_soporte["soporte_hora_termino_atencion_miercoles"] . ' Hrs.</span></br>'
                                    . '<span class="text-muted"><strong>Jueves: </strong> ' . $row_soporte["soporte_hora_inicio_atencion_jueves"] . ' - ' . $row_soporte["soporte_hora_termino_atencion_jueves"] . ' Hrs.</span></br>'
                                    . '<span class="text-muted"><strong>Viernes: </strong> ' . $row_soporte["soporte_hora_inicio_atencion_viernes"] . ' - ' . $row_soporte["soporte_hora_termino_atencion_viernes"] . ' Hrs.</span></br>';
                                    if ($row_soporte["soporte_tipo_horario"] == "7x24") {
                                        echo '<span class="text-muted"><strong>Sábado: </strong> ' . $row_soporte["soporte_hora_inicio_atencion_sabado"] . ' - ' . $row_soporte["soporte_hora_termino_atencion_sabado"] . ' Hrs.</span></br>'
                                        . '<span class="text-muted"><strong>Domingo: </strong> ' . $row_soporte["soporte_hora_inicio_atencion_domingo"] . ' - ' . $row_soporte["soporte_hora_termino_atencion_domingo"] . ' Hrs.</span>';
                                    }
                                    ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="text-muted"><strong><h5>Tiempos de Respuesta - <a href="#formModal_UpdateTiemposRespuestas" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5></strong>
                                        <textarea class="form-control" placeholder="" rows="5" id="" name="" disabled><?php echo $row_soporte["soporte_tiempo_respuesta"]; ?></textarea>
                                    </span>
                                    </br>
                                    <span class="text-muted"><strong><h5>Tareas Incluidas - <a href="#formModal_UpdateTareasIncluidas" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5></strong> 
                                        <textarea class="form-control" placeholder="" rows="5" id="" name="" disabled><?php echo $row_soporte["soporte_tareas_incluidas"]; ?></textarea>
                                    </span>
                                    </br>
                                    <span class="text-muted"><strong><h5>Flujo de Soporte - <a href="#formModal_UpdateFlujoSoporte" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5></strong>
                                        <textarea class="form-control" placeholder="" rows="5" id="" name="" disabled><?php echo $row_soporte["soporte_flujo"]; ?></textarea>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!--
                        <div class="" style="margin-bottom: 20px;">
                            </br>
                            <a class="btn btn-success hidden-print" id="invoicePrint"><i class="fa fa-print"></i> Imprimir Ficha</a>
                        </div>-->
                    </div><!-- /.padding20 -->
                </div><!-- /main-container -->
            </div><!-- /wrapper -->

            <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
            <!-- ########################################################### -->
            <form id="form_update_FechaInicioSoporte" method="POST">
                <div class="modal fade" id="formModal_UpdateFechaInicioSoporte">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="control-label">Fecha Inicio Soporte</label>
                                    <input type="text" value="<?php echo $row_soporte["soporte_fecha_inicio"]; ?>" placeholder="" id="soporte_update_FechaInicioSoporte" name="soporte_update_FechaInicioSoporte" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_update_fecha_inicio_soporte"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                        <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                    </div>
                                </div>                            
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_FechaTerminoSoporte" method="POST">
                <div class="modal fade" id="formModal_UpdateFechaTerminoSoporte">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Fecha Termino Soporte</label>
                                            <input type="text" value="<?php echo $row_soporte["soporte_fecha_termino"]; ?>" placeholder="" id="soporte_update_FechaTerminoSoporte" name="soporte_update_FechaTerminoSoporte" class="form-control input-sm">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_update_fecha_termino_soporte"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                        <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                    </div>
                                </div>  
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_TipoHorarioSoporte" method="POST">
                <div class="modal fade" id="formModal_UpdateTipoHorarioSoporte">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="control-label">Tipo de Horario Registrado</label>
                                                    <input type="text" disabled="" value="<?php echo $row_soporte["soporte_tipo_horario"]; ?>" placeholder="" id="soporte_update_TipoHorarioSoporte" name="soporte_update_TipoHorarioSoporte" class="form-control input-sm">
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="control-label">Modificar Tipo de Horario</label></br>
                                                    <?php if ($row_soporte["soporte_tipo_horario"] == "7x24") { ?>
                                                        <label class="label-radio inline">
                                                            <input type="radio" id="proveedor_tipo_horario_soporte_5x8" name="soporte_update_TipoHorarioSoporte" value="5x8">
                                                            <span class="custom-radio"></span>
                                                            5x8
                                                        </label>
                                                    <?php } else { ?>
                                                        <label class="label-radio inline">
                                                            <input type="radio" id="proveedor_tipo_horario_soporte_7x24" name="soporte_update_TipoHorarioSoporte" value="7x24">
                                                            <span class="custom-radio"></span>
                                                            7x24
                                                        </label>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_update_tipo_horario_soporte"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                        <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_SistemaTickets" method="POST">
                <div class="modal fade" id="formModal_UpdateSistemaTickets">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Sistema Tickets</label>
                                            <input type="text" placeholder="ej: http://www.entel.cl/" value="<?php
                                            if ($row_soporte["soporte_sistema_tickets"] == "") {
                                                echo 'Sin Sistema Tickets';
                                            } else {
                                                echo $row_soporte["soporte_sistema_tickets"];
                                            }
                                            ?>" id="soporte_update_SistemaTickets" name="soporte_update_SistemaTickets" class="form-control input-sm">
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_update_Sistematickets"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                        <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_TipoAsistencia" method="POST">
                <div class="modal fade" id="formModal_UpdateTipoAsistencia">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Tipo Asistencia Registrado</label>
                                                    <input type="text" value="<?php
                                                    if ($row_soporte["soporte_tipo_asistencia_presencial"] != "" AND $row_soporte["soporte_tipo_asistencia_remoto"] != "") {
                                                        $TipoAsitencia = "Presencial - Remoto";
                                                    } elseif ($row_soporte["soporte_tipo_asistencia_presencial"] != "") {
                                                        $TipoAsitencia = "Presencial";
                                                    } elseif ($row_soporte["soporte_tipo_asistencia_remoto"] != "") {
                                                        $TipoAsitencia = "Remoto";
                                                    }
                                                    echo $TipoAsitencia;
                                                    ?>" disabled="" placeholder="" id="soporte_update_TipoAsistencia_BD" name="soporte_update_TipoAsistencia_BD" class="form-control input-sm">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Tipo Asistencia a Modificar</label></br>
                                                <label class="label-radio inline">
                                                    <input type="checkbox" id="soporte_update_TipoAsistencia_Presencial" name="soporte_update_TipoAsistencia_Presencial" value="Presencial">
                                                    <span class="custom-radio"></span>
                                                    Presencial
                                                </label>
                                                <label class="label-radio inline">
                                                    <input type="checkbox" id="soporte_update_TipoAsistencia_Remoto" name="soporte_update_TipoAsistencia_Remoto" value="Remoto">
                                                    <span class="custom-radio"></span>
                                                    Remoto
                                                </label>                                          
                                            </div>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_update_TipoAsistencia"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                        <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_TipoSoporte" method="POST">
                <div class="modal fade" id="formModal_UpdateTipoSoporte">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">                                    
                                        <div class="row">                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Tipo Soporte Registrado</label>                                            
                                                    <input type="text" value="<?php
                                                    if ($row_soporte["soporte_tipo_reactivo"] != "" AND $row_soporte["soporte_tipo_proactivo"] != "" AND $row_soporte["soporte_tipo_evolutivo"] != "") {
                                                        $TipoSoporte = "Reactivo - Proactivo - Evolutivo";
                                                    } elseif ($row_soporte["soporte_tipo_reactivo"] != "" AND $row_soporte["soporte_tipo_evolutivo"] != "") {
                                                        $TipoSoporte = "Reactivo - Evolutivo";
                                                    } elseif ($row_soporte["soporte_tipo_proactivo"] != "" AND $row_soporte["soporte_tipo_evolutivo"] != "") {
                                                        $TipoSoporte = "Proactivo - Evolutivo";
                                                    } elseif ($row_soporte["soporte_tipo_reactivo"] != "" AND $row_soporte["soporte_tipo_proactivo"] != "") {
                                                        $TipoSoporte = "Reactivo - Proactivo";
                                                    } elseif ($row_soporte["soporte_tipo_reactivo"] != "") {
                                                        $TipoSoporte = $row_soporte["soporte_tipo_reactivo"];
                                                    } elseif ($row_soporte["soporte_tipo_proactivo"] != "") {
                                                        $TipoSoporte = $row_soporte["soporte_tipo_proactivo"];
                                                    } elseif ($row_soporte["soporte_tipo_evolutivo"] != "") {
                                                        $TipoSoporte = $row_soporte["soporte_tipo_evolutivo"];
                                                    }
                                                    echo $TipoSoporte;
                                                    ?>" disabled="" placeholder="" id="" name="" class="form-control input-sm">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Tipo Soporte a Modificar</label></br>
                                                <label class="label-radio inline">
                                                    <input type="checkbox" id="proveedor_tipo_soporte_reactivo" name="proveedor_tipo_soporte_reactivo" value="Reactivo">
                                                    <span class="custom-radio"></span>
                                                    Reactivo
                                                </label>
                                                <label class="label-radio inline">
                                                    <input type="checkbox" id="proveedor_tipo_soporte_proactivo" name="proveedor_tipo_soporte_proactivo" value="Proactivo">
                                                    <span class="custom-radio"></span>
                                                    Proactivo
                                                </label>
                                                <label class="label-radio inline">
                                                    <input type="checkbox" id="proveedor_tipo_soporte_evolutivo" name="proveedor_tipo_soporte_evolutivo" value="Evolutivo">
                                                    <span class="custom-radio"></span>
                                                    Evolutivo
                                                </label>
                                            </div>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_update_tipo_soporte"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                        <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_TiempoRespuesta" method="POST">
                <div class="modal fade" id="formModal_UpdateTiemposRespuestas">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Tiempos de Respuesta</label>
                                            <textarea class="form-control" placeholder="" rows="5" id="soporte_update_TiemposRespuestas" name="soporte_update_TiemposRespuestas" ><?php echo $row_soporte["soporte_tiempo_respuesta"]; ?></textarea>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_update_TiempoRespuesta"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                        <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_TareasIncluidas" method="POST">
                <div class="modal fade" id="formModal_UpdateTareasIncluidas">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Tareas Incluidas</label>
                                            <textarea class="form-control" placeholder="" rows="5" id="soporte_update_TareasIncluidas" name="soporte_update_TareasIncluidas"><?php echo $row_soporte["soporte_tareas_incluidas"]; ?></textarea>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_update_TareasIcluidas"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                        <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_FlujoSoporte" method="POST">
                <div class="modal fade" id="formModal_UpdateFlujoSoporte">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Flujo de Soporte</label>
                                            <textarea class="form-control" placeholder="" rows="3" id="soporte_update_FlujoSoporte" name="soporte_update_FlujoSoporte" ><?php echo $row_soporte["soporte_flujo"]; ?></textarea>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                            <div class="modal-footer">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_update_FlujoSoporte"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                        <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <form id="form_update_HorarioAtencion" method="POST">
                <div class="modal fade" id="formModal_UpdateHorariosAtencion">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Modificar Descripción a Actualizar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <?php if ($row_soporte["soporte_tipo_horario"] == "5x8") { ?>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h4 class="text-center">Lunes</h4>
                                                        <label class="control-label">Hora de Inicio</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo$row_soporte["soporte_hora_inicio_atencion_lunes"]; ?>" value="" type="text" id="proveedor_horario_inicio_atencion_lunes_update" name="proveedor_horario_inicio_atencion_lunes_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        </br>
                                                        <label class="control-label">Hora de Termino</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_termino_atencion_lunes"]; ?>" type="text" id="proveedor_horario_termino_atencion_lunes_update" name="proveedor_horario_termino_atencion_lunes_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h4 class="text-center">Martes</h4>
                                                        <label class="control-label">Hora de Inicio</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_inicio_atencion_martes"]; ?>" type="text" id="proveedor_horario_inicio_atencion_martes_update" name="proveedor_horario_inicio_atencion_martes_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        </br>
                                                        <label class="control-label">Hora de Termino</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_termino_atencion_martes"]; ?>" type="text" id="proveedor_horario_termino_atencion_martes_update" name="proveedor_horario_termino_atencion_martes_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h4 class="text-center">Miercoles</h4>
                                                        <label class="control-label">Hora de Inicio</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_inicio_atencion_miercoles"]; ?>" type="text" id="proveedor_horario_inicio_atencion_miercoles_update" name="proveedor_horario_inicio_atencion_miercoles_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        </br>
                                                        <label class="control-label">Hora de Termino</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_termino_atencion_miercoles"]; ?>" type="text" id="proveedor_horario_termino_atencion_miercoles_update" name="proveedor_horario_termino_atencion_miercoles_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h4 class="text-center">Jueves</h4>
                                                        <label class="control-label">Hora de Inicio</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_inicio_atencion_jueves"]; ?>" type="text" id="proveedor_horario_inicio_atencion_jueves_update" name="proveedor_horario_inicio_atencion_jueves_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        </br>
                                                        <label class="control-label">Hora de Termino</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_termino_atencion_jueves"]; ?>" type="text" id="proveedor_horario_termino_atencion_jueves_update" name="proveedor_horario_termino_atencion_jueves_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h4 class="text-center">Viernes</h4>
                                                        <label class="control-label">Hora de Inicio</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_inicio_atencion_viernes"]; ?>" type="text" id="proveedor_horario_inicio_atencion_viernes_update" name="proveedor_horario_inicio_atencion_viernes_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        </br>
                                                        <label class="control-label">Hora de Termino</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_termino_atencion_viernes"]; ?>" type="text" id="proveedor_horario_termino_atencion_viernes_update" name="proveedor_horario_termino_atencion_viernes_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h4 class="text-center">Lunes</h4>
                                                        <label class="control-label">Hora de Inicio</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo$row_soporte["soporte_hora_inicio_atencion_lunes"]; ?>" value="" type="text" id="proveedor_horario_inicio_atencion_lunes_update" name="proveedor_horario_inicio_atencion_lunes_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        </br>
                                                        <label class="control-label">Hora de Termino</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_termino_atencion_lunes"]; ?>" type="text" id="proveedor_horario_termino_atencion_lunes_update" name="proveedor_horario_termino_atencion_lunes_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h4 class="text-center">Martes</h4>
                                                        <label class="control-label">Hora de Inicio</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_inicio_atencion_martes"]; ?>" type="text" id="proveedor_horario_inicio_atencion_martes_update" name="proveedor_horario_inicio_atencion_martes_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        </br>
                                                        <label class="control-label">Hora de Termino</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_termino_atencion_martes"]; ?>" type="text" id="proveedor_horario_termino_atencion_martes_update" name="proveedor_horario_termino_atencion_martes_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h4 class="text-center">Miercoles</h4>
                                                        <label class="control-label">Hora de Inicio</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_inicio_atencion_miercoles"]; ?>" type="text" id="proveedor_horario_inicio_atencion_miercoles_update" name="proveedor_horario_inicio_atencion_miercoles_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        </br>
                                                        <label class="control-label">Hora de Termino</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_termino_atencion_miercoles"]; ?>" type="text" id="proveedor_horario_termino_atencion_miercoles_update" name="proveedor_horario_termino_atencion_miercoles_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h4 class="text-center">Jueves</h4>
                                                        <label class="control-label">Hora de Inicio</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_inicio_atencion_jueves"]; ?>" type="text" id="proveedor_horario_inicio_atencion_jueves_update" name="proveedor_horario_inicio_atencion_jueves_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        </br>
                                                        <label class="control-label">Hora de Termino</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_termino_atencion_jueves"]; ?>" type="text" id="proveedor_horario_termino_atencion_jueves_update" name="proveedor_horario_termino_atencion_jueves_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h4 class="text-center">Viernes</h4>
                                                        <label class="control-label">Hora de Inicio</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_inicio_atencion_viernes"]; ?>" type="text" id="proveedor_horario_inicio_atencion_viernes_update" name="proveedor_horario_inicio_atencion_viernes_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        </br>
                                                        <label class="control-label">Hora de Termino</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_termino_atencion_viernes"]; ?>" type="text" id="proveedor_horario_termino_atencion_viernes_update" name="proveedor_horario_termino_atencion_viernes_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h4 class="text-center">Sabado</h4>
                                                        <label class="control-label">Hora de Inicio</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_inicio_atencion_sabado"]; ?>" type="text" id="proveedor_horario_inicio_atencion_sabado_update" name="proveedor_horario_inicio_atencion_sabado_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        </br>
                                                        <label class="control-label">Hora de Termino</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_termino_atencion_sabado"]; ?>" type="text" id="proveedor_horario_termino_atencion_sabado_update" name="proveedor_horario_termino_atencion_sabado_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row"> 
                                                    <div class="col-md-4">
                                                        <h4 class="text-center">Domingo</h4>
                                                        <label class="control-label">Hora de Inicio</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_inicio_atencion_domingo"]; ?>" type="text" id="proveedor_horario_inicio_atencion_domingo_update" name="proveedor_horario_inicio_atencion_domingo_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                        </br>
                                                        <label class="control-label">Hora de Termino</label>
                                                        <div class="input-group bootstrap-timepicker">
                                                            <input class="timepicker form-control input-sm" value="<?php echo $row_soporte["soporte_hora_termino_atencion_domingo"]; ?>" type="text" id="proveedor_horario_termino_atencion_domingo_update" name="proveedor_horario_termino_atencion_domingo_update"/>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                            <div class="modal-footer">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_update_HorariosAtencion"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                        <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </form>
            <!-- ########################################################### -->
            <!-- Logout confirmation -->
            <?php require_once './includes/confirmation.logout.inc.php'; ?> 
            <!-- ######################################## -->

            <!-- Le javascript
            ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="js/jquery-1.10.2.js"></script>
            <script src="bootstrap/js/bootstrap.min.js"></script>
            <script src="js/jquery.validate.min.js"></script>
            <script src="js/custom_jquery_validate.js"></script>
            <script>
                $(document).ready(function () {
                    $("textarea").keydown(function (e) {
                            if (e.keyCode === 9) { // tab was pressed
                                // get caret position/selection
                                var start = this.selectionStart;
                                end = this.selectionEnd;

                                var $this = $(this);

                                // set textarea value to: text before caret + tab + text after caret
                                $this.val($this.val().substring(0, start) + "\t" + $this.val().substring(end));

                                // put caret at right position again
                                this.selectionStart = this.selectionEnd = start + 1;

                                // prevent the focus lose
                                return false;
                            }
                        });
                });
            </script>
            <script src='js/datepicker_js/bootstrap-datepicker.js'></script>
            <script src='js/datepicker_js/locales/bootstrap-datepicker.es.js'></script>
            <script>
                $(document).ready(function() {
                    $('#soporte_update_FechaInicioSoporte').datepicker({
                        language: "es",
                        format: "dd-mm-yyyy",
                        autoclose: true,
                        todayHighlight: true
                    });
                    $('#soporte_update_FechaTerminoSoporte').datepicker({
                        language: "es",
                        format: "dd-mm-yyyy",
                        autoclose: true,
                        todayHighlight: true
                    });
                });
            </script>
            <script src="js/menu-active-class.js"></script>
            <script src='js/chosen.jquery.min.js'></script><!-- Mask-input --> 
            <script src='js/jquery.maskedinput.min.js'></script><!-- Datepicker -->
            <!--<script src='js/bootstrap-datepicker.min.js'></script>--><!-- Timepicker -->
            <script src='js/bootstrap-timepicker.min.js'></script><!-- Slider -->
            <script src='js/bootstrap-slider.min.js'></script><!-- Tag input -->
            <script src='js/jquery.tagsinput.min.js'></script><!-- WYSIHTML5 -->
            <script src='js/wysihtml5-0.3.0.min.js'></script>
            <script src='js/uncompressed/bootstrap-wysihtml5.js'></script><!-- Dropzone -->
            <script src='js/dropzone.min.js'></script><!-- Modernizr -->
            <script src='js/modernizr.min.js'></script><!-- Pace -->
            <script src='js/pace.min.js'></script><!-- Popup Overlay -->
            <script src='js/jquery.popupoverlay.min.js'></script><!-- Slimscroll -->
            <script src='js/jquery.slimscroll.min.js'></script><!-- Cookie -->
            <script src='js/jquery.cookie.min.js'></script><!-- Endless -->
            <script src="js/endless/endless_form.js"></script>
            <script src="js/endless/endless.js"></script>
            <script>
                $(function() {
                    $('#invoicePrint').click(function() {
                        window.print();
                    });
                });
            </script>
    </body>
</html>
