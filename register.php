<?php 
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
?>
<?php 
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body>
        <div class="login-wrapper">
            <div class="text-center">
                <h2 class="fadeInUp animation-delay8" style="font-weight:bold">
                    <div class="text-center">
                        <img src="img/logos_proveedores/nf_logo.png" style="width: 10%; margin-bottom: 15px;">
                    </div>
                    <span style="color:#0154a0; text-shadow:0 1px #ffffff">Inventario Red Móvil</span>
                </h2>
            </div>
            <div class="col-md-12  animation-delay1">	
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-plus-circle fa-lg"></i> Sign up
                    </div>
                    <div class="panel-body">
                        <form class="form-login" id="form_registro_Usuario" method="POST">
                            <div class="row">
                                <div class="col-md-3"><div class="form-group"><label class="control-label" for="user_nombre">Nombre o Nombres</label><input id="user_nombre" name="user_nombre" type="text" placeholder="Nombre o Nombres" class="form-control input-sm bounceIn " ></div></div>
                                <div class="col-md-3"><div class="form-group"><label class="control-label">Apellido Paterno</label><input id="user_apell_paterno" name="user_apell_paterno" type="text" placeholder="Apellido Paterno" class="form-control input-sm bounceIn " ></div></div>
                                <div class="col-md-3"><div class="form-group"><label class="control-label">Apellido Materno</label><input id="user_apell_materno" name="user_apell_materno" type="text" placeholder="Apellido Materno" class="form-control input-sm bounceIn " ></div></div>
                                <div class="col-md-3"><div class="form-group"><label class="control-label">Teléfono Móvil</label><input id="user_tel_movil" name="user_tel_movil" type="text" placeholder="Teléfono Móvil" class="form-control input-sm bounceIn " ></div></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><div class="form-group"><label class="control-label">Correo Electrónico</label><input id="user_email" name="user_email" type="email" placeholder="" class="form-control input-sm bounceIn " ></div></div>
                                <div class="col-md-3"><div class="form-group"><label class="control-label">Nombre de Usuario</label><input id="user_username" name="user_username" type="text" placeholder="" class="form-control input-sm bounceIn " ></div></div>
                                <div class="col-md-3"><div class="form-group"><label class="control-label">Contraseña</label><input  id="user_contrasena" name="user_contrasena" type="password" placeholder="" class="form-control input-sm bounceIn " ></div></div>
                                <div class="col-md-3"><div class="form-group"><label class="control-label">Confirmar Contraseña</label><input id="user_confirm_contrasena" name="user_confirm_contrasena" type="password" placeholder="" class="form-control input-sm bounceIn " ></div></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Seleccionar Permiso al Usuario </label>
                                        <select class="form-control input-sm" id="select_Permiso_usuario" name="select_Permiso_usuario">
                                            <option value="">Seleccione Permiso</option>
                                            <?php
                                            $datos_sql_select_permisos = $db->query("SELECT permiso_id,permiso_rol FROM Permisos");
                                            foreach ($datos_sql_select_permisos as $row):
                                                echo "<option value=" . $row["permiso_id"] . ">" . $row["permiso_rol"] . "</option>";
                                            endforeach
                                            ?>
                                        </select>
                                    </div>                                
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                <div class="row">                                        
                                    <div class="col-md-8 text-left">
                                        <div id="msj_respuesta_ingreso_usuarios"></div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button type="submit" class="btn btn-success btn-sm "><i class="fa fa-plus-circle"></i> Registrar</button></div>
                                </div>
                            </div>
                    </div>
                    </div>
                    </div>
                    </form>
                    </div>
                </div><!-- /panel -->
            </div><!-- /login-widget -->
        </div><!-- /login-wrapper -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!--Bootstrap-->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!--Cookie-->
        <script src='js/jquery.cookie.min.js'></script>

        <!--Endless-->
        <script src="js/endless/endless.js"></script>
    </body>
</html>
