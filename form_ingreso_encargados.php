<?php 
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.php"> Home</a></li>
                        <li class="active">Ingreso de Encargados</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                </br>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <form class="no-margin" id="form_ingreso_encargados" role="form" method="post">
                            <div class="panel-heading">
                                <h4>Información de los Encargados </h4>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Servicio</label>
                                            <?php
                                            //##########################################################################
                                            //SE INSTANCIA LA CLASE DE LA BASE DATOS
                                            require_once './controlador/Db.class.php';
                                            $db = new Db();
                                            //LLENADO DEL SELECT
                                            $datos_sql_select_provee = $db->query("SELECT * FROM Servicios");
                                            ?>
                                            <select class="form-control input-sm" id="selec_servicio_encargado" name="selec_servicio_encargado" required="required">
                                                <option value=""> Seleccione Servicio</option>
                                                <?php
                                                foreach ($datos_sql_select_provee as $row):
                                                    echo "<option value=" . $row["serv_id"] . ">" . $row["serv_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Nombre del Encargado</label>
                                            <input type="text" id="encarga_nomnbre" name="encarga_nomnbre" placeholder="" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Email del Encargado</label>
                                            <input type="text" id="encarga_email" name="encarga_email" placeholder="ej. encargado@entel.cl" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div><!-- /form-group -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Área</label>
                                            <select class="form-control input-sm" id="selec_area_encargados" name="selec_area_encargados" required="required">
                                                <option value=""> Seleccione Área</option>
                                                <option value="OvasPlat"> OvasPlat</option>
                                                <option value="Ingeniería"> Ingeniería</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Nivel Escalamiento</label>
                                            <select class="form-control input-sm" id="selec_escalamiento" name="selec_escalamiento" required="required">
                                                <option value=""> Seleccione</option>
                                                <option value="Nivel N°1"> Nivel °1</option>
                                                <option value="Nivel N°2"> Nivel °2</option>
                                                <option value="Nivel N°3"> Nivel °3</option>
                                                <option value="Nivel N°4"> Nivel °4</option>
                                                <option value="Nivel N°5"> Nivel °5</option>
                                                <option value="Nivel N°6"> Nivel °6</option>
                                                <option value="Nivel N°7"> Nivel °7</option>
                                                <option value="Nivel N°8"> Nivel °8</option>
                                                <option value="Nivel N°9"> Nivel °9</option>
                                                <option value="Nivel N°10"> Nivel °10</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>                                
                               <div class="panel-footer">
                                    <div class="row">                                        
                                        <div class="col-md-8 text-left">
                                            <div id="msj_respuesta_ingreso_encargado"></div>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <button type="submit" class="btn btn-info">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div><!-- /panel -->
                </div><!-- /.col-->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/confirmation.logout.inc.php'; ?> 
        <!-- ######################################## -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>      
        <!-- JQuery Validate - Validacion del Formulario -->
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>
        <script>
            $(document).ready(function () {
                $("textarea").keydown(function (e) {
                        if (e.keyCode === 9) { // tab was pressed
                            // get caret position/selection
                            var start = this.selectionStart;
                            end = this.selectionEnd;

                            var $this = $(this);

                            // set textarea value to: text before caret + tab + text after caret
                            $this.val($this.val().substring(0, start) + "\t" + $this.val().substring(end));

                            // put caret at right position again
                            this.selectionStart = this.selectionEnd = start + 1;

                            // prevent the focus lose
                            return false;
                        }
                    });
            });
        </script>
        <script src="js/menu-active-class.js"></script>
        <!-- Chosen -->
        <script src="js/chosen.jquery.min.js"></script>	

        <!-- Mask-input --> 
        <script src="js/jquery.maskedinput.min.js"></script>	

        <!-- Datepicker -->
        <script src="js/bootstrap-datepicker.min.js"></script>	

        <!-- Timepicker -->
        <script src="js/bootstrap-timepicker.min.js"></script>	

        <!-- Slider -->
        <script src="js/bootstrap-slider.min.js"></script>

        <!-- Tag input -->
        <script src="js/jquery.tagsinput.min.js"></script>	

        <!-- WYSIHTML5 -->
        <script src="js/wysihtml5-0.3.0.min.js"></script>	
        <script src="js/uncompressed/bootstrap-wysihtml5.js"></script>

        <!-- Dropzone -->
        <script src="js/dropzone.min.js"></script>	

        <!-- Modernizr -->
        <script src="js/modernizr.min.js"></script>

        <!-- Pace -->
        <script src="js/pace.min.js"></script>

        <!-- Popup Overlay -->
        <script src="js/jquery.popupoverlay.min.js"></script>

        <!-- Slimscroll -->
        <script src="js/jquery.slimscroll.min.js"></script>

        <!-- Cookie -->
        <script src="js/jquery.cookie.min.js"></script>

        <!-- Endless -->
        <script src="js/endless/endless_form.js"></script>
        <script src="js/endless/endless.js"></script>


    </body>
</html>
