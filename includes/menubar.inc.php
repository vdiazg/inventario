<?php
require_once './controlador/Db.class.php';
$db = new Db();
session_start();
$NombrePersona = $_SESSION["datos_usuario_logueado"][0];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][1];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][2];
$TelefonoMovil = $_SESSION["datos_usuario_logueado"][3];
$EmailUsuario = $_SESSION["datos_usuario_logueado"][4];
$Username = $_SESSION["datos_usuario_logueado"][5];
$Contraseña = $_SESSION["datos_usuario_logueado"][6];
$PemisosUsuario = $_SESSION["datos_usuario_logueado"][7];
$PemisosRol = $_SESSION["datos_usuario_logueado"][8];
if (!isset($NombrePersona)) {
    header('Location: login.php');
}

?>
<aside class="fixed skin-1">
    <div class="sidebar-inner scrollable-sidebar">
        <div class="size-toggle">
            <a class="clearfix" href="blank.php" >
                <img class="img-responsive" style="-webkit-animation: fadeInRotate .9s ease;" src="img/logos_proveedores/nf_logo.png" alt="User Avatar">
            </a>
            <a class="btn btn-sm" id="sizeToggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="btn btn-sm pull-right logoutConfirm_open"  href="#logoutConfirm">
                <i class="fa fa-power-off"></i>
            </a>
        </div><!-- /size-toggle -->	
        <div class="user-block clearfix">
            <img src="<?php echo $db->single("SELECT usuario_img_url FROM Usuarios WHERE usuario_nombre = '$NombrePersona' AND usuario_username = '$Username'"); ?>" alt="User Avatar">
            <div class="detail">
                <strong><?php echo $NombrePersona."</br>".$ApellidoPaterno." ".$ApellidoMaterno; ?></strong>
                <ul class="list-inline text-center">
                    <li><a href="profile.php">Perfil</a></li>
                </ul>
            </div>
        </div>
        <div class="main-menu" >
            <ul class="" id="">
                <li class="">
                    <a href="index.php">
                        <span class="menu-icon">
                            <i class="fa fa-desktop fa-lg"></i> 
                        </span>
                        <span class="text">
                            Tablero Instrumentos
                        </span>
                        <span class="menu-hover"></span>
                    </a>
                </li>
                <li class="openable">
                    <a href="form_ingreso_proveedores.php">
                        <span class="menu-icon"><i class="fa fa-file-text fa-lg"></i></span>
                        <span class="text">Proveedores</span>
                        <span class="menu-hover"></span>
                    </a>
                    <ul class="submenu">
                        <?php if($PemisosRol == "ADMINISTRADOR" OR $PemisosRol == "ESCRITURA"){?>
                        <li><a href="form_ingreso_proveedores.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso de Proveedores</span></a></li>
                        <li><a href="form_ingreso_administrativos.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso Administrativos</span></a></li>
                        <li><a href="form_ingreso_tecnicos.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso Técnicos</span></a></li>
                        <?php } ?>
                        <?php if($PemisosRol == "ADMINISTRADOR" OR $PemisosRol == "LECTURA"){?>
                        <li><a href="proveedores.php"><span class="submenu-label"><i class="fa fa-eye fa-lg"></i> Ver Proveedores</span></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li class="openable">
                    <a href="#">
                        <span class="menu-icon"><i class="fa fa-file-text fa-lg"></i></span>
                        <span class="text">Servicios</span>
                        <span class="menu-hover"></span>
                    </a>
                    <ul class="submenu">
                        <?php if($PemisosRol == "ADMINISTRADOR" OR $PemisosRol == "ESCRITURA"){?>
                        <li><a href="form_ingreso_servicio.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso Servicio</span></a></li>
                        <li><a href="form_ingreso_encargados.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso Encargados</span></a></li>                        
                        <li><a href="form_ingreso_patrocinador.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso Patrocinadores</span></a></li>
                        <li><a href="form_ingreso_jefeProyecto.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso Jefes Proyectos</span></a></li>
                        <li><a href="form_ingreso_arquitecto.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso Arquitectos</span></a></li>                        
                        <li><a href="form_ingreso_clientes.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso Clientes</span></a></li>
                        <li><a href="form_ingreso_monitoreo_respaldo.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso de Monitoreo y Respaldos</span></a></li>
                        <li><a href="form_ingreso_licencias.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso de Licencias</span></a></li>
                        <li><a href="form_ingreso_insumos.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso Insumos</span></a></li>
                        <?php } ?>
                        <?php if($PemisosRol == "ADMINISTRADOR" OR $PemisosRol == "LECTURA"){?>
                        <li><a href="servicios_activos.php"><span class="submenu-label"><i class="fa fa-eye fa-lg"></i> Ver Servicios</span></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li class="openable">
                    <a href="#">
                        <span class="menu-icon"><i class="fa fa-file-text fa-lg"></i></span>
                        <span class="text">Infraestructura</span>
                        <span class="menu-hover"></span>
                    </a>
                    <ul class="submenu">
                        <?php if($PemisosRol == "ADMINISTRADOR" OR $PemisosRol == "ESCRITURA"){?>
                        <li><a href="form_ingreso_infraestructura.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso de Componente</span></a></li>
                        <li><a href="form_ingreso_ipcomponentes.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso de IP's</span></a></li>
                        <?php } ?>
                        <?php if($PemisosRol == "ADMINISTRADOR" OR $PemisosRol == "LECTURA"){?>
                        <li><a href="infraestructura.php"><span class="submenu-label"><i class="fa fa-eye fa-lg"></i> Ver Componentes</span></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li class="openable open">
                    <a href="form_ingreso_soporte.php">
                        <span class="menu-icon">
                            <i class="fa fa-support fa-lg"></i> 
                        </span>
                        <span class="text">
                            Soportes
                        </span>
                        <span class="menu-hover"></span>
                    </a>
                    <ul class="submenu">
                        <?php if($PemisosRol == "ADMINISTRADOR" OR $PemisosRol == "ESCRITURA"){?>
                        <li><a href="form_ingreso_soporte.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingreso de Soporte</span></a></li>
                        <?php } ?>
                        <?php if($PemisosRol == "ADMINISTRADOR" OR $PemisosRol == "LECTURA"){?>
                        <li><a href="soportes_activos.php"><span class="submenu-label"><i class="fa fa-eye fa-lg"></i> Ver Soportes</span></a></li>
                        <?php } ?>
                    </ul>
                </li>
                
                
                <!--<li class="openable open">
                    <a href="blank.php">
                        <span class="menu-icon">
                            <i class="fa fa-file-text fa-lg"></i> 
                        </span>
                        <span class="text">
                            Page
                        </span>
                        <span class="menu-hover"></span>
                    </a>
                    <ul class="submenu">
                        <li><a href="login.php"><span class="submenu-label">Sign in</span></a></li>
                        <li><a href="register.php"><span class="submenu-label">Sign up</span></a></li>
                        <li><a href="lock_screen.php"><span class="submenu-label">Lock Screen</span></a></li>
                        <li><a href="profile.php"><span class="submenu-label">Profile</span></a></li>
                        <li><a href="blog.php"><span class="submenu-label">Blog</span></a></li>
                        <li><a href="single_post.php"><span class="submenu-label">Single Post</span></a></li>
                        <li><a href="landing.php"><span class="submenu-label">Landing</span></a></li>
                        <li><a href="search_result.php"><span class="submenu-label">Search Result</span></a></li>
                        <li><a href="chat.php"><span class="submenu-label">Chat Room</span></a></li>
                        <li><a href="movie.php"><span class="submenu-label">Movie Gallery</span></a></li>
                        <li><a href="pricing.php"><span class="submenu-label">Pricing</span></a></li>
                        <li><a href="invoice.php"><span class="submenu-label">Invoice</span></a></li>
                        <li><a href="faq.php"><span class="submenu-label">FAQ</span></a></li>
                        <li><a href="contact.php"><span class="submenu-label">Contact</span></a></li>
                        <li><a href="error404.php"><span class="submenu-label">Error404</span></a></li>
                        <li><a href="error500.php"><span class="submenu-label">Error500</span></a></li>
                        <li><a href="blank.php"><span class="submenu-label">Blank</span></a></li>
                    </ul>
                </li>
                <li class="openable">
                    <a href="blank.php">
                        <span class="menu-icon">
                            <i class="fa fa-tag fa-lg"></i> 
                        </span>
                        <span class="text">
                            Component
                        </span>
                        <span class="badge badge-success bounceIn animation-delay5">9</span>
                        <span class="menu-hover"></span>
                    </a>
                    <ul class="submenu">
                        <li><a href="ui_element.php"><span class="submenu-label">UI Features</span></a></li>
                        <li><a href="button.php"><span class="submenu-label">Button & Icons</span></a></li>
                        <li><a href="tab.php"><span class="submenu-label">Tab</span></a></li>
                        <li><a href="nestable_list.php"><span class="submenu-label">Nestable List</span></a></li>
                        <li><a href="calendar.php"><span class="submenu-label">Calendar</span></a></li>
                        <li><a href="table.php"><span class="submenu-label">Table</span></a></li>
                        <li><a href="widget.php"><span class="submenu-label">Widget</span></a></li>
                        <li><a href="form_element.php"><span class="submenu-label">Form Element</span></a></li>
                        <li><a href="form_wizard.php"><span class="submenu-label">Form Wizard</span></a></li>
                    </ul>
                </li>

                <li>
                    <a href="timeline.php">
                        <span class="menu-icon">
                            <i class="fa fa-clock-o fa-lg"></i> 
                        </span>
                        <span class="text">
                            Timeline
                        </span>
                        <span class="menu-hover"></span>
                    </a>
                </li>
                <li>
                    <a href="gallery.php">
                        <span class="menu-icon">
                            <i class="fa fa-picture-o fa-lg"></i> 
                        </span>
                        <span class="text">
                            Gallery
                        </span>
                        <span class="menu-hover"></span>
                    </a>
                </li>
                <li>
                    <a href="inbox.php">
                        <span class="menu-icon">
                            <i class="fa fa-envelope fa-lg"></i> 
                        </span>
                        <span class="text">
                            Inbox
                        </span>
                        <span class="badge badge-danger bounceIn animation-delay6">4</span>
                        <span class="menu-hover"></span>
                    </a>
                </li>
                <li>
                    <a href="email_selection.php">
                        <span class="menu-icon">
                            <i class="fa fa-tasks fa-lg"></i> 
                        </span>
                        <span class="text">
                            Email Template
                        </span>
                        <small class="badge badge-warning bounceIn animation-delay7">New</small>
                        <span class="menu-hover"></span>
                    </a>
                </li>
                <li class="openable">
                    <a href="blank.php">
                        <span class="menu-icon">
                            <i class="fa fa-magic fa-lg"></i> 
                        </span>
                        <span class="text">
                            Multi-Level menu
                        </span>
                        <span class="menu-hover"></span>
                    </a>
                    <ul class="submenu">
                        <li class="openable">
                            <a href="blank.php">
                                <span class="submenu-label">menu 2.1</span>
                                <span class="badge badge-danger bounceIn animation-delay1 pull-right">3</span>
                            </a>
                            <ul class="submenu third-level">
                                <li><a href="blank.php"><span class="submenu-label">menu 3.1</span></a></li>
                                <li><a href="blank.php"><span class="submenu-label">menu 3.2</span></a></li>
                                <li class="openable">
                                    <a href="blank.php">
                                        <span class="submenu-label">menu 3.3</span>
                                        <span class="badge badge-danger bounceIn animation-delay1 pull-right">2</span>
                                    </a>
                                    <ul class="submenu fourth-level">
                                        <li><a href="blank.php"><span class="submenu-label">menu 4.1</span></a></li>
                                        <li><a href="blank.php"><span class="submenu-label">menu 4.2</span></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="openable">
                            <a href="blank.php">
                                <span class="submenu-label">menu 2.2</span>
                                <span class="badge badge-success bounceIn animation-delay2 pull-right">3</span>
                            </a>
                            <ul class="submenu third-level">
                                <li class="openable">
                                    <a href="blank.php">
                                        <span class="submenu-label">menu 3.1</span>
                                        <span class="badge badge-success bounceIn animation-delay1 pull-right">2</span>
                                    </a>
                                    <ul class="submenu fourth-level">
                                        <li><a href="blank.php"><span class="submenu-label">menu 4.1</span></a></li>
                                        <li><a href="blank.php"><span class="submenu-label">menu 4.2</span></a></li>
                                    </ul>
                                </li>
                                <li><a href="blank.php"><span class="submenu-label">menu 3.2</span></a></li>
                                <li><a href="blank.php"><span class="submenu-label">menu 3.3</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>-->
            </ul>
        </div><!-- /main-menu -->
    </div><!-- /sidebar-inner -->
</aside>
