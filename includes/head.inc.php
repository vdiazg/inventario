<meta charset="utf-8">
<title>Inventario Red Móvil</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Inventario Entel, PHP5,MYSQL">
<meta name="author" content="Víctor Manuel Díaz Galdames">
<link rel="shortcut icon" href="img/logos_proveedores/nf_logo.png">

<!-- Bootstrap core CSS -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="fonts/css/font-awesome.min.css" rel="stylesheet">

<!-- Pace -->
<!--<link href="css/pace.css" rel="stylesheet">-->

<!-- Chosen -->
<link href="css/chosen/chosen.min.css" rel="stylesheet"/>
        
<link href="css/jquery.dataTables_themeroller.css" rel="stylesheet">

<!-- Datepicker -->
<!--<link href="css/datepicker.css" rel="stylesheet"/>-->
<link href="css/datepicker_css/datepicker3.css" rel="stylesheet"/>

<!-- Timepicker -->
<link href="css/bootstrap-timepicker.css" rel="stylesheet"/>
        
<!-- Color box -->
<link href="css/colorbox/colorbox.css" rel="stylesheet">

<!-- Morris -->
<link href="css/morris.css" rel="stylesheet"/>	

<!-- Tag input -->
<link href="css/jquery.tagsinput.css" rel="stylesheet"/>

<!-- WYSIHTML5 -->
<link href="css/bootstrap-wysihtml5.css" rel="stylesheet"/>

<!-- Dropzone -->
<link href='css/dropzone/dropzone.css' rel="stylesheet"/>

<!-- Endless -->
<link href="css/endless.css" rel="stylesheet">
<link href="css/endless-skin.css" rel="stylesheet">
