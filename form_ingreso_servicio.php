<?php 
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.php"> Home</a></li>
                        <li class="active">Ingreso de Servicio</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                </br>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <form class="no-margin" id="form_ingreso_servicio" method="POST" enctype="multipart/form-data">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-4"><h4>Detalles del Servicio</h4></div>
                                    <div class="col-md-8"><div class="alert alert-warning">
                                        <strong>Atención!</strong> Una vez registrado el Servicio, se debe Ingresar el o los encargados en "<a href="form_ingreso_encargados.php"><span class="submenu-label"><i class="fa fa-plus fa-lg"></i> Ingresar Encargados</span></a>". De esta manera se podrá visualizar en "<a><span class="submenu-label"><i class="fa fa-eye fa-lg"></i> Ver Servicios</span></a>".
                            </div></div>
                                </div>
                                
                            </div>
                            <div class="panel-body">
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <div class="row" id="">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Proveedor</label>
                                            <?php
                                            //##########################################################################
                                            //SE INSTANCIA LA CLASE DE LA BASE DATOS
                                            require_once './controlador/Db.class.php';
                                            $db = new Db();
                                            //LLENADO DEL SELECT
                                            $datos_sql_select_provee = $db->query("SELECT * FROM Proveedores");
                                            ?>
                                            <select class="form-control input-sm" id="selec_proveedor_servicio" name="selec_proveedor_servicio" required="required">
                                                <option value=""> Seleccione Proveedor</option>
                                                <?php
                                                foreach ($datos_sql_select_provee as $row):
                                                    echo "<option value=" . $row["provee_id"] . ">" . $row["provee_nombre_fantasia"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div>
                                    </div>                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Responsabilidad Proveedor</label>
                                            <input type="text" id="servicio_responsabilidad_proveedor" name="servicio_responsabilidad_proveedor" placeholder="Hardware, Software, Aplicaciones, Etc" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            </br>
                                            <a href="#" id="btn_agregar_otro_campo1" class="btn btn-link"><i class="fa fa-plus-circle fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <div class="row hide" id="row2">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Proveedor</label>
                                            <select class="form-control input-sm " id="selec_proveedor_servicio2" name="selec_proveedor_servicio2" required="required">
                                                <option value=""> Seleccione Proveedor</option>
                                                <?php
                                                foreach ($datos_sql_select_provee as $row):
                                                    echo "<option value=" . $row["provee_id"] . ">" . $row["provee_nombre_fantasia"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div>
                                    </div>                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Responsabilidad Proveedor</label>
                                            <input type="text" id="servicio_responsabilidad_proveedor2" name="servicio_responsabilidad_proveedor2" placeholder="Hardware, Software, Aplicaciones, Etc" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            </br>
                                            <a href="#" id="btn_agregar_otro_campo2" class="btn btn-link"><i class="fa fa-plus-circle fa-2x"></i></a>
                                            <a href="#" id="btn_quitar_otro_campo2" class="btn btn-link"><i class="fa fa-minus-circle fa-2x"></i></a>                                            
                                        </div>

                                    </div>
                                </div>
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <div class="row hide" id="row3">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Proveedor</label>
                                            <select class="form-control input-sm" id="selec_proveedor_servicio3" name="selec_proveedor_servicio3" required="required">
                                                <option value=""> Seleccione Proveedor</option>
                                                <?php
                                                foreach ($datos_sql_select_provee as $row):
                                                    echo "<option value=" . $row["provee_id"] . ">" . $row["provee_nombre_fantasia"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div>
                                    </div>                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Responsabilidad Proveedor</label>
                                            <input type="text" id="servicio_responsabilidad_proveedor3" name="servicio_responsabilidad_proveedor3" placeholder="Hardware, Software, Aplicaciones, Etc" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            </br>
                                            <a href="#" id="btn_agregar_otro_campo3" class="btn btn-link"><i class="fa fa-plus-circle fa-2x"></i></a>
                                            <a href="#" id="btn_quitar_otro_campo3" class="btn btn-link"><i class="fa fa-minus-circle fa-2x"></i></a>                                            
                                        </div>                                          
                                    </div>
                                </div>
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <div class="row hide" id="row4">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Proveedor</label>
                                            <select class="form-control input-sm" id="selec_proveedor_servicio4" name="selec_proveedor_servicio4" required="required">
                                                <option value=""> Seleccione Proveedor</option>
                                                <?php
                                                foreach ($datos_sql_select_provee as $row):
                                                    echo "<option value=" . $row["provee_id"] . ">" . $row["provee_nombre_fantasia"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div>
                                    </div>                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Responsabilidad Proveedor</label>
                                            <input type="text" id="servicio_responsabilidad_proveedor4" name="servicio_responsabilidad_proveedor4" placeholder="Hardware, Software, Aplicaciones, Etc" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            </br>
                                            <a href="#" id="btn_agregar_otro_campo4" class="btn btn-link"><i class="fa fa-plus-circle fa-2x"></i></a>
                                            <a href="#" id="btn_quitar_otro_campo4" class="btn btn-link"><i class="fa fa-minus-circle fa-2x"></i></a>                                            
                                        </div>

                                    </div>
                                </div>
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <div class="row hide" id="row5">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Proveedor</label>
                                            <select class="form-control input-sm" id="selec_proveedor_servicio5" name="selec_proveedor_servicio5" required="required">
                                                <option value=""> Seleccione Proveedor</option>
                                                <?php
                                                foreach ($datos_sql_select_provee as $row):
                                                    echo "<option value=" . $row["provee_id"] . ">" . $row["provee_nombre_fantasia"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div>
                                    </div>                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Responsabilidad Proveedor</label>
                                            <input type="text" id="servicio_responsabilidad_proveedor5" name="servicio_responsabilidad_proveedor5" placeholder="Hardware, Software, Aplicaciones, Etc" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            </br>
                                            <a href="#" id="btn_agregar_otro_campo5" class="btn btn-link"><i class="fa fa-plus-circle fa-2x"></i></a>
                                            <a href="#" id="btn_quitar_otro_campo5" class="btn btn-link"><i class="fa fa-minus-circle fa-2x"></i></a>                                            
                                        </div>

                                    </div>
                                </div>
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <div class="row hide" id="row6">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione Proveedor</label>
                                            <select class="form-control input-sm" id="selec_proveedor_servicio6" name="selec_proveedor_servicio6" required="required">
                                                <option value=""> Seleccione Proveedor</option>
                                                <?php
                                                foreach ($datos_sql_select_provee as $row):
                                                    echo "<option value=" . $row["provee_id"] . ">" . $row["provee_nombre_fantasia"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                        </div>
                                    </div>                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Responsabilidad Proveedor</label>
                                            <input type="text" id="servicio_responsabilidad_proveedor6" name="servicio_responsabilidad_proveedor6" placeholder="Hardware, Software, Aplicaciones, Etc" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            </br>
                                            <a href="#" id="btn_quitar_otro_campo6" class="btn btn-link"><i class="fa fa-minus-circle fa-2x"></i></a>                                            
                                        </div>

                                    </div>
                                </div>
                                <!-- ///////////////////////////////////////////////////////////////////////////// -->
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label" for="servicio_nombre">Nombre del Servicio</label>
                                            <input type="text" id="servicio_nombre" name="servicio_nombre" placeholder="Denominación Oficial del Proyecto" class="form-control input-sm" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Alias del Servicio</label>
                                            <input type="text" id="servicio_alias" name="servicio_alias" placeholder="Otro Nombre del Proyecto" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Código de Proyecto</label>
                                            <input type="text" id="servicio_cod_proyec" name="servicio_cod_proyec" placeholder="Número Usado por Ingeniería" class="form-control input-sm" data-required="true" data-minlength="8">
                                        </div><!-- /form-group -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Descripción del Servicio</label>
                                            <!--<input type="text" placeholder="" class="form-control input-sm" data-required="true" data-minlength="8" rows="3">-->
                                            <textarea class="form-control input-sm" id="servicio_descripcion" name="servicio_descripcion" rows="3" data-required="true" placeholder="Función y Alcances del Servicio"></textarea>
                                        </div><!-- /form-group -->
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Tiempo Marcha Blanca</label>
                                            <input id="servicio_tiempo_marcha_blanca" name="servicio_tiempo_marcha_blanca" class="form-control input-sm" type="text" placeholder="ej: 2 Semanas, 3 Semanas ó más."/>
                                        </div><!-- /form-group -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Fecha Producción</label>
                                            <div class="input-group">
                                                <input type="text" id="servicio_fecha_produccion" name="servicio_fecha_produccion" value="" class="datepicker form-control input-sm" placeholder="Fecha Oficial de Inicio en Operaciones">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>  
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Fecha Marcha Blanca</label>
                                            <div class="input-group">
                                                <input type="text" id="servicio_fecha_marcha_blanca" name="servicio_fecha_marcha_blanca" value="" class="datepicker form-control input-sm" placeholder="Fecha de Inicio de Pruebas">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div><!-- /form-group -->
                                    </div><!--
                                    <div class="col-md-4">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label">Logo Empresa</label>
                                                <input type="file" id="servicio_img" name="servicio_img">
                                            </div>
                                        </div>                                    
                                    </div>-->
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Nota</label>
                                            <textarea class="form-control input-sm" id="servicio_nota" name="servicio_nota" rows="3" placeholder="Solo se Permiten 140 Caracteres" maxlength="141" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">                                        
                                        <div class="col-md-8 text-left">
                                            <div id="msj_respuesta_ingreso_servicio"></div>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <button type="submit" class="btn btn-info">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div><!-- /panel -->
                </div><!-- /.col-->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
        <!-- Logout confirmation -->
        <?php require_once './includes/confirmation.logout.inc.php'; ?> 
        <!-- ######################################## -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>      
        <!-- JQuery Validate - Validacion del Formulario -->
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>
        <script src='js/datepicker_js/bootstrap-datepicker.js'></script>
        <script src='js/datepicker_js/locales/bootstrap-datepicker.es.js'></script>
        <script>
            $(document).ready(function () {
                $('#servicio_fecha_produccion').datepicker({
                    language: "es",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    todayHighlight: true
                });
                $('#servicio_fecha_marcha_blanca').datepicker({
                    language: "es",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    todayHighlight: true
                });
                $("textarea").keydown(function (e) {
                        if (e.keyCode === 9) { // tab was pressed
                            // get caret position/selection
                            var start = this.selectionStart;
                            end = this.selectionEnd;

                            var $this = $(this);

                            // set textarea value to: text before caret + tab + text after caret
                            $this.val($this.val().substring(0, start) + "\t" + $this.val().substring(end));

                            // put caret at right position again
                            this.selectionStart = this.selectionEnd = start + 1;

                            // prevent the focus lose
                            return false;
                        }
                    });
            });
        </script>
        <script src="js/menu-active-class.js"></script>

        <!-- Chosen -->
        <script src='js/chosen.jquery.min.js'></script>	

        <!-- Mask-input --> 
        <script src='js/jquery.maskedinput.min.js'></script>	

        <!-- Datepicker -->
        <!--<script src='js/bootstrap-datepicker.min.js'></script>-->	

        <!-- Timepicker -->
        <script src='js/bootstrap-timepicker.min.js'></script>	

        <!-- Slider -->
        <script src='js/bootstrap-slider.min.js'></script>

        <!-- Tag input -->
        <script src='js/jquery.tagsinput.min.js'></script>	

        <!-- WYSIHTML5 -->
        <script src='js/wysihtml5-0.3.0.min.js'></script>	
        <script src='js/uncompressed/bootstrap-wysihtml5.js'></script>

        <!-- Dropzone -->
        <script src='js/dropzone.min.js'></script>	

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless_form.js"></script>
        <script src="js/endless/endless.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#btn_agregar_otro_campo1').click(function () {
                    $("#row2").removeClass("hide");
                });
                $('#btn_agregar_otro_campo2').click(function () {
                    $("#row3").removeClass("hide");
                });
                $('#btn_agregar_otro_campo3').click(function () {
                    $("#row4").removeClass("hide");
                });
                $('#btn_agregar_otro_campo4').click(function () {
                    $("#row5").removeClass("hide");
                });
                $('#btn_agregar_otro_campo5').click(function () {
                    $("#row6").removeClass("hide");
                });
                $('#btn_quitar_otro_campo2').click(function () {
                    $("#row2").addClass("hide");
                });
                $('#btn_quitar_otro_campo3').click(function () {
                    $("#row3").addClass("hide");
                });
                $('#btn_quitar_otro_campo4').click(function () {
                    $("#row4").addClass("hide");
                });
                $('#btn_quitar_otro_campo4').click(function () {
                    $("#row4").addClass("hide");
                });
                $('#btn_quitar_otro_campo5').click(function () {
                    $("#row5").addClass("hide");
                });
                $('#btn_quitar_otro_campo6').click(function () {
                    $("#row6").addClass("hide");
                });
            });
        </script>

    </body>
</html>
