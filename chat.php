<?php
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
session_start();
$NombrePersona = $_SESSION["datos_usuario_logueado"][0];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][1];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][2];
$TelefonoMovil = $_SESSION["datos_usuario_logueado"][3];
$EmailUsuario = $_SESSION["datos_usuario_logueado"][4];
$Username = $_SESSION["datos_usuario_logueado"][5];
$Contraseña = $_SESSION["datos_usuario_logueado"][6];
$PemisosUsuario = $_SESSION["datos_usuario_logueado"][7];
if (!isset($NombrePersona)) {
    header('Location: login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body style="overflow:hidden;">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div class="chat-wrapper">
                    <div class="chat-sidebar border-right bg-white">
                        <div class="border-bottom padding-sm text-center" style="height: 40px;"><h5><strong>Miembros</strong></h5></div>
                        <ul class="friend-list">
                            <div class="border-bottom padding-sm" style="height: 40px;"><strong>Contactos</strong></div>
                            <?php
                            $todos_Usuarios = $db->query("SELECT * FROM Usuarios WHERE  not (usuario_username='$Username');");
                            foreach ($todos_Usuarios as $row_usuarios):
                                
                                $id_usuario = $row_usuarios["usuario_id"];
                                $nombre_usuario = $row_usuarios["usuario_nombre"];
                                
                                echo '<li class="">
                                        <a class="a_id_usuario" style="padding: 14px;" name="" id="' . $id_usuario . '"';
                                    echo "onclick=\"cargarModalModificar('$id_usuario','$nombre_usuario');\">";
                                echo '<img src="img/Foto_Perfil_Víctor_Díaz.png" alt="" class="img-circle pull-left" style="width: 35px; height: 35px; margin-top: -9px;">
                                        <div class="friend-name pull-right"><strong>' . $row_usuarios["usuario_nombre"] . '</strong></div></br>
                                        </a>
                                      </li>';
                            endforeach;
                            ?>                            
                        </ul>
                        <div class="border-bottom padding-sm" style="height: 45px;"><strong>Conversaciones</strong></div>
                        <ul class="friend-list">
                            <li class="active bounceInDown">
                                <a href="#" class="clearfix" id="">
                                    <img src="img/user.jpg" alt="" class="img-circle">
                                    <div class="friend-name">	
                                        <strong>John Doe</strong>
                                    </div>
                                    <div class="last-message text-muted">Hello, Are you there?</div>
                                    <small class="time text-muted">Just now</small>
                                    <small class="chat-alert badge badge-danger">1</small>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="clearfix">
                                    <img src="img/user2.jpg" alt="" class="img-circle">
                                    <div class="friend-name">	
                                        <strong>Jane Doe</strong>
                                    </div>
                                    <div class="last-message text-muted">Lorem ipsum dolor sit amet.</div>
                                    <small class="time text-muted">5 mins ago</small>
                                    <small class="chat-alert text-muted"><i class="fa fa-check"></i></small>
                                </a>
                            </li>
                            <li class="active">
                                <a href="#" class="clearfix">
                                    <img src="img/user3.jpg" alt="" class="img-circle">
                                    <div class="friend-name">	
                                        <strong>Sarah</strong>
                                    </div>
                                    <div class="last-message text-muted">Lorem ipsum dolor sit amet.</div>
                                    <small class="time text-muted">12 mins ago</small>
                                    <small class="chat-alert badge badge-danger">2</small>
                                </a>
                            </li>
                            <li class="active">
                                <a href="#" class="clearfix">
                                    <img src="img/user4.jpg" alt="" class="img-circle">
                                    <div class="friend-name">	
                                        <strong>Angelina</strong>
                                    </div>
                                    <div class="last-message text-muted">Lorem ipsum dolor sit amet.</div>
                                    <small class="time text-muted">1 hr ago</small>
                                    <small class="chat-alert badge badge-danger">1</small>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="clearfix">
                                    <img src="img/user5.jpg" alt="" class="img-circle">
                                    <div class="friend-name">	
                                        <strong>Kate</strong>
                                    </div>
                                    <div class="last-message text-muted">Lorem ipsum dolor sit amet.</div>
                                    <small class="time text-muted">Yesterday</small>
                                    <small class="chat-alert text-muted"><i class="fa fa-reply"></i></small>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="chat-inner scrollable-div" id="div_scroll_chat">
                        <div class="chat-header bg-white">
                            <button type="button" class="navbar-toggle" id="friendListToggle">
                                <i class="fa fa-comment fa-lg"></i>
                                <span class="notification-label bounceIn animation-delay4">7</span>
                            </button>
                            <div class="pull-right">
                                <a class="btn btn-xs btn-default">Add Friend</a>
                                <a class="btn btn-xs btn-default">Create Group</a>
                            </div>
                        </div>
                        <form method="POST" id='form_chat_mensajes' name="form_chat_mensajes">
                            <div class="chat-message" id="div_chat_mensaje">
                                <div style="height: 30px;" ><strong id="strong_usuario"></strong></div>
                                <ul class="chat" id="ul_chat_mensaje"></ul>
                            </div>

                            <div class="chat-box bg-white">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control border no-shadow no-rounded" id="input_mensaje" name="input_mensaje" placeholder="Type your message here">
                                        <span class="input-group-btn" style="vertical-align: -webkit-baseline-middle !important;">
                                            <a class="btn btn-success no-rounded" id="btn_send_chat" onclick="RegistrarMensaje()" type="button">Send</a>
                                        </span>
                                    </div><!-- /input-group -->	
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /chat-wrapper -->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/confirmation.logout.inc.php'; ?> 
        <!-- ######################################## -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>
        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {});
            
            var input_nombre_usuario;                
            var input_mensaje;
            
            function cargarModalModificar(id_usuario, nombre_usuario) {
                input_nombre_usuario = nombre_usuario;              
                $("#strong_usuario").html("Usuario: " +id_usuario +nombre_usuario);
                CargaMensajesAnteriores();                
            }
            function RegistrarMensaje() {
                //setInterval(CargaMensajesAnteriores, 500);
                $.ajaxSetup({cache: false});
                //input_nombre_usuario = $("#input_nombre_usuario").val();                
                input_mensaje = $("#input_mensaje").val();
                var dataString = 'input_mensaje=' + input_mensaje + '&input_nombre_usuario=' + input_nombre_usuario;
                $.ajax({
                    type: "POST",
                    url: "./modelo/ingreso_mensaje_chat.php",
                    cache: false,
                    data: dataString,
                    success: function (data) {
                        if (data == "1") {
                            $("#input_mensaje").val("");
                            CargaMensajesAnteriores();
                        } else {
             
                        }
                    }
                });
            }
            function CargaMensajesAnteriores() {
                $('#ul_chat_mensaje').html('<img src="img/ajax-loader.gif" alt="" />');
                $.ajax({
                    type: "POST",
                    url: "modelo/ver_mensajes_chat.php",
                    data: 'input_nombre_usuario='+input_nombre_usuario,
                    success: function (data) {
                        $("#ul_chat_mensaje").html(data);
                        setTimeout(function () {
                                var objDiv = document.getElementById("div_scroll_chat");
                                objDiv.scrollTop = objDiv.scrollHeight;
                            }, 500);
                    }
                });
            }

            //setTimeout(CargaMensajesAnteriores, 500);            
            //});
            /*$("#form_chat_mensajes").validate({
             rules: {
             input_mensaje: {required: true}
             
             },
             highlight: function (element) {
             $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
             //$('#busqueda_rut').removeClass('disabled');
             },
             success: function (element) {
             $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
             //$('#busqueda_rut').attr('disabled', true);
             },
             errorClass: 'help-block',
             messages: {
             input_mensaje: {required: "Campo obligatorio."}
             },
             submitHandler: function (form) {
             //$('#msj_respuesta_lock_screen_usuario').html('<img src="img/ajax-loader.gif" alt="" />');
             
             setInterval(CargaMensajesAnteriores, 500);
             $.ajax({
             type: "POST",
             url: "./modelo/ingreso_mensaje_chat.php",
             cache: false,
             data: $(form).serialize()+dataString,
             success: function (data) {
             //var objDiv = document.getElementById("div_scroll_chat");
             //objDiv.scrollTop = objDiv.scrollHeight;
             alert (data);
             /*
             if (data == "1") {
             $("#input_mensaje").val("");
             //alert("Existe");  
             //location.href = "index.php";
             //var div_scroll_chat = document.getElementById("div_scroll_chat");
             /*setTimeout(function () {
             var objDiv = document.getElementById("div_scroll_chat");
             objDiv.scrollTop = objDiv.scrollHeight;
             }, 500);
             } else {
             //alert("NO Existe");
             //$('#msj_respuesta_lock_screen_usuario').fadeIn(1000).html('<div class="alert alert-danger" style="padding: 4px !important; margin: 0 auto; width: 270px;"><strong>Error en la Contraseña</strong></div>');
             
             }
             }
             
             });
             }
             });
             }*/
        </script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>
        <script type="text/javascript">
            //-----------------------------------------------------------



            //-----------------------------------------------------------
            $(function () {
                $('#friendListToggle').click(function () {
                    $('.chat-wrapper').toggleClass('sidebar-display');
                });

                $('.scrollable-div').slimScroll({
                    height: '100%',
                    size: '3px'
                });

                document.ontouchmove = function (e) {
                    if (disableScroll) {
                        e.preventDefault();
                    }
                };
            });
        </script>
    </body>
</html>
