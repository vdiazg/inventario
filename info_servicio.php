<?php
/*
  -----------------------------------------------------------------------
  DATOS DE CONTACTO
  -----------------------------------------------------------------------
  Desarrollador: Victor Manuel Diaz Galdames.
  Nivel Profesional: Ingeniero Informático
  Número Contacto (Móvil): +5699491896
  Correo Eletrónico: victordiazgaldames@gmail.com
  Curriculum Vitae: http://victordiaz.nfconnection.cl/
  Nacionalidad: Chileno
  Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
  -----------------------------------------------------------------------
  DATOS DE JEFE A CARGO
  -----------------------------------------------------------------------
  Nombre: Nelson Horacio Guzman Mora.
  Número Contacto (Móvil): +56998289358
  Correo Eletrónico: nguzman@entel.cl
  Cargo: Jefe Operación Plataformas
 */
?>
<!DOCTYPE html>
<?php
$serv_id = $_GET['serv_id']; //SE INSTANCIA LA CLASE DE LA BASE DATOS

require_once './controlador/Db.class.php';
$db = new Db();
//LLENADO DEL SELECT
$datos_sql_select_servicios = $db->query("SELECT *,"
        . "date_format(serv_fecha_marcha_blanca,'%d-%m-%Y') as serv_fecha_marcha_blanca,"
        . "date_format(serv_fecha_produccion,'%d-%m-%Y') as serv_fecha_produccion "
        . "FROM Servicios WHERE serv_id = '" . $serv_id . "'");
if ($datos_sql_select_servicios == false) {
    header('Location: ./error404.php');
}
foreach ($datos_sql_select_servicios as $row_serv):

endforeach;
session_start();
$_SESSION["IDserv"] = $row_serv["serv_id"];
?>
<html lang="en">    
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="bg-white preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div class="padding-md">
                    <div class="clearfix">
                        <div class="row text-center">
                            <div class="col-md-12"><h4><strong>Información Detallada del Servicio</strong></h4></div>
                        </div>
                        <hr>
                        <div class="row text-center">
                            <div class="col-md-6" style="border-right: 1px solid #D2D2D2;"><h4 class="m-bottom-xs m-top-xs"><strong>Proveedor Asociado: </strong></br><?php 
                            
                            
                            
                            //echo $db->single("SELECT provee_nombre_empresa FROM Servicios S, Proveedores P, ResponsProveeServicio RPS WHERE " . $row_serv["serv_id"] . " = RPS.Servicios_serv_id AND RPS.Proveedores_provee_id = P.provee_id"); 
                            $Provedores = $db->query("SELECT * FROM "
                                                . "Proveedores P, "
                                                . "ResponsProveeServicio RPS, "
                                                . "Servicios S, "
                                                . "Servicios_has_Componentes SHC, "
                                                . "Componentes C "
                                                . "WHERE RPS.Servicios_serv_id = S.serv_id AND "
                                                . "RPS.Proveedores_provee_id = P.provee_id AND "
                                                . "'$serv_id' = S.serv_id AND "
                                                . "SHC.Componentes_componente_id = C.componente_id ORDER BY S.serv_id DESC");
                                        foreach ($Provedores as $row_prove):
                                            echo $row_prove["provee_nombre_empresa"]."</br>";
                                        endforeach;
                            
                            
                            
                            ?></h4></div>
                            <div class="col-md-6"><h4 class="m-bottom-xs m-top-xs"><strong>Servicio Asociado: </strong><?php echo $row_serv["serv_nombre"]; ?></h4></div>
                        </div>
                        <!--<div class="pull-left">
                            <span class="">
                                <img class="img-demo img-responsive" style="width: 150px; height: 150px;" src="img/logos_servicios/logo_entel_200x200.jpg" >
                            </span>
                            <div class="pull-left m-left-sm">
                                <h3 class="m-bottom-xs m-top-xs"><strong>Servicio: </strong><?php echo $row_serv["serv_nombre"]; ?> <a href="#formModal_UpdateNombreServicio" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h3>
                                <span class="text-muted"><strong>Nombre de Fantasía o Alias:</strong> <?php echo $row_serv["serv_alias"]; ?> - <a href="#formModal_UpdateAliasServicio" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                            </div>
                        </div>
                        <div class="pull-right">
                            <h5><strong>Código Proyecto: </strong># <?php echo $row_serv["serv_cod_proyec"]; ?></h5>
                            <span><strong>Ingreso de Servicio a Inventario: </strong>9 de Enero del 2015 - No BD</span>
                        </div>-->
                    </div>
                    <hr>
                    <div class="clearfix">
                        <div class="row">
                            <div class="col-md-6 pull-left"> 
                                <h5>Información del Servicio</h5>                                    
                                <span class="text-muted"><strong>Descripción: </strong><p align="justify"> <?php echo $row_serv["serv_descripcion"]; ?> - <a href="#formModal_UpdateDescripcion" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></p></span></br>
                                <span class="text-muted"><strong>Fecha Marcha Blanca: </strong> <?php echo $row_serv["serv_fecha_marcha_blanca"]; ?> - <a href="#formModal_UpdateFechaMarchaBlanca" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                <span class="text-muted"><strong>Tiempo Marcha Blanca: </strong> <?php echo $row_serv["serv_tiempo_marcha_blanca"]; ?> - <a href="#formModal_UpdateTiempoMarchaBlanca" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                <span class="text-muted"><strong>Fecha Producción: </strong> <?php echo $row_serv["serv_fecha_produccion"]; ?> - <a href="#formModal_UpdateFechaProduccion" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                            </div>
                            <div class="col-md-6 pull-left"> 
                                <div class="col-md-12">
                                    <h5>Contactos Encargados  - <a href="#formModal_UpdateEncargados" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                    <?php
                                    $datos_sql_select_encargados = $db->query("SELECT * FROM Encargados WHERE Servicios_serv_id = '" . $row_serv["serv_id"] . "' ORDER BY encarga_dpto, encarga_escalamiento ASC");
                                    foreach ($datos_sql_select_encargados as $row_encarga):
                                        echo '<span class="text-muted"><strong>Nombre del Encargado ' . $row_encarga['encarga_escalamiento'] . ' de ' . $row_encarga['encarga_dpto'] . ': </strong> ' . $row_encarga['encarga_nombre'] . '</span></br>'
                                        . '<span class="text-muted"><strong>Email del Encargado ' . $row_encarga['encarga_escalamiento'] . ' de ' . $row_encarga['encarga_dpto'] . ': </strong> ' . $row_encarga['encarga_email'] . '</span></br></br>';
                                    endforeach;
                                    ?>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Contacto Patrocinador  - <a href="#formModal_UpdatePatrocinador" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                <?php
                                $datos_sql_select_patrocina = $db->query("SELECT * FROM Patrocinadores WHERE Servicios_serv_id = '" . $row_serv["serv_id"] . "'");
                                foreach ($datos_sql_select_patrocina as $row_patrocina):
                                    echo '<span class="text-muted"><strong>Nombre: </strong> ' . $row_patrocina["patroci_nombre"] . '</span></br>'
                                    . '<span class="text-muted"><strong>Cargo: </strong> ' . $row_patrocina["patroci_email"] . '</span></br></br>';
                                endforeach;
                                ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Contacto Jefe Proyecto  - <a href="#formModal_UpdateJefeProyecto" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5> 
                                <?php
                                $datos_sql_select_jefeproyec = $db->query("SELECT * FROM JefesProyecto WHERE Servicios_serv_id = '" . $row_serv["serv_id"] . "'");
                                foreach ($datos_sql_select_jefeproyec as $row_jefeproyec):
                                    echo '<span class="text-muted"><strong>Nombre: </strong> ' . $row_jefeproyec["jefesproyec_nombre"] . '</span></br>'
                                    . '<span class="text-muted"><strong>Cargo: </strong> ' . $row_jefeproyec["jefesproyec_email"] . '</span></br></br>';
                                endforeach;
                                ?>
                            </div>
                            <div class="col-md-4">
                                <h5>Contacto Arquitecto  - <a href="#formModal_UpdateArquitecto" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5> 
                                <?php
                                $datos_sql_select_arquitecto = $db->query("SELECT * FROM Arquitectos WHERE Servicios_serv_id = '" . $row_serv["serv_id"] . "'");
                                foreach ($datos_sql_select_arquitecto as $row_arquitecto):
                                    echo '<span class="text-muted"><strong>Nombre: </strong> ' . $row_arquitecto["arquitec_nombre"] . '</span></br>'
                                    . '<span class="text-muted"><strong>Cargo: </strong> ' . $row_arquitecto["arquitec_email"] . '</span></br></br>';
                                endforeach;
                                ?>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Contactos Clientes  - <a href="#formModal_UpdateClientes" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                <?php
                                $datos_sql_select_clientes = $db->query("SELECT * FROM Clientes c, Servicios_has_Clientes s WHERE c.Client_id = s.Clientes_client_id AND s.Servicios_serv_id = '" . $row_serv["serv_id"] . "'");
                                foreach ($datos_sql_select_clientes as $row_clientes):
                                    echo '<span class="text-muted"><strong>Nombre Área Cliente: </strong> ' . $row_clientes["client_area"] . '</span></br>'
                                    . '<span class="text-muted"><strong>Nombre Contacto Cliente: </strong> ' . $row_clientes["cliente_nombre_contacto"] . '</span></br>'
                                    . '<span class="text-muted"><strong>Email Contacto Cliente: </strong> ' . $row_clientes["client_email"] . '</span></br>'
                                    . '<span class="text-muted"><strong>Motivo Cliente: </strong> ' . $row_clientes["client_motivo"] . '</span></br></br>'
                                    . '<hr>';
                                endforeach;
                                ?>                                    
                            </div>
                            <div class="col-md-4">
                            <h4 class="text-center">Proveedor <!-- <a href="#formModal_UpdateProveedor" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a>--></h4>
                                <?php
                                //$Provedores = $db->query("SELECT * FROM Proveedores P, ResponsProveeServicio RPS WHERE RPS.Servicios_serv_id = " . $row_serv['serv_id'] . " AND RPS.Proveedores_provee_id = P.provee_id");
                                $Provedores = $db->query("SELECT * FROM "
                                                . "Proveedores P, "
                                                . "ResponsProveeServicio RPS, "
                                                . "Servicios S, "
                                                . "Servicios_has_Componentes SHC, "
                                                . "Componentes C "
                                                . "WHERE RPS.Servicios_serv_id = S.serv_id AND "
                                                . "RPS.Proveedores_provee_id = P.provee_id AND "
                                                . "".$row_serv['serv_id']." = S.serv_id AND "
                                                . "SHC.Componentes_componente_id = C.componente_id ORDER BY RPS.Servicios_serv_id DESC");
                                foreach ($Provedores as $row_prove):
                                    $Responsabilidad1 = $db->single("SELECT respons_provee_servicio1 FROM ResponsProveeServicio RPS WHERE RPS.Servicios_serv_id = ".$row_serv['serv_id']." AND RPS.Proveedores_provee_id = (SELECT provee_id FROM Proveedores WHERE provee_nombre_fantasia = '" . $row_prove['provee_nombre_fantasia'] . "')");
                                    $Responsabilidad2 = $db->single("SELECT respons_provee_servicio2 FROM ResponsProveeServicio RPS WHERE RPS.Servicios_serv_id = ".$row_serv['serv_id']." AND RPS.Proveedores_provee_id = (SELECT provee_id FROM Proveedores WHERE provee_nombre_fantasia = '" . $row_prove['provee_nombre_fantasia'] . "')");
                                    $Responsabilidad3 = $db->single("SELECT respons_provee_servicio3 FROM ResponsProveeServicio RPS WHERE RPS.Servicios_serv_id = ".$row_serv['serv_id']." AND RPS.Proveedores_provee_id = (SELECT provee_id FROM Proveedores WHERE provee_nombre_fantasia = '" . $row_prove['provee_nombre_fantasia'] . "')");
                                    $Responsabilidad4 = $db->single("SELECT respons_provee_servicio4 FROM ResponsProveeServicio RPS WHERE RPS.Servicios_serv_id = ".$row_serv['serv_id']." AND RPS.Proveedores_provee_id = (SELECT provee_id FROM Proveedores WHERE provee_nombre_fantasia = '" . $row_prove['provee_nombre_fantasia'] . "')");
                                    $Responsabilidad5 = $db->single("SELECT respons_provee_servicio5 FROM ResponsProveeServicio RPS WHERE RPS.Servicios_serv_id = ".$row_serv['serv_id']." AND RPS.Proveedores_provee_id = (SELECT provee_id FROM Proveedores WHERE provee_nombre_fantasia = '" . $row_prove['provee_nombre_fantasia'] . "')");
                                    $Responsabilidad6 = $db->single("SELECT respons_provee_servicio6 FROM ResponsProveeServicio RPS WHERE RPS.Servicios_serv_id = ".$row_serv['serv_id']." AND RPS.Proveedores_provee_id = (SELECT provee_id FROM Proveedores WHERE provee_nombre_fantasia = '" . $row_prove['provee_nombre_fantasia'] . "')");

                                    if ($Responsabilidad1 != "") {
                                        $MSJ_Responsabilidad1 = '<span class="text-muted"><strong>Responsabilidad N°1 Proveedor: </strong> ' . $Responsabilidad1 . '</span></br>';
                                    } else {
                                        //$MSJ_Responsabilidad1 = '<span class="text-muted"><strong>Responsabilidad N°1 Proveedor: </strong> -------------------- </span></br>';
                                        $MSJ_Responsabilidad1 = '';
                                    }
                                    if ($Responsabilidad2 != "") {
                                        $MSJ_Responsabilidad2 = '<span class="text-muted"><strong>Responsabilidad N°2 Proveedor: </strong> ' . $Responsabilidad2 . '</span></br>';
                                    } else {
                                        //$MSJ_Responsabilidad2 = '<span class="text-muted"><strong>Responsabilidad N°2 Proveedor: </strong> -------------------- </span></br>';
                                        $MSJ_Responsabilidad2 = '';
                                    }
                                    if ($Responsabilidad3 != "") {
                                        $MSJ_Responsabilidad3 = '<span class="text-muted"><strong>Responsabilidad N°3 Proveedor: </strong> ' . $Responsabilidad3 . '</span></br>';
                                    } else {
                                        //$MSJ_Responsabilidad3 = '<span class="text-muted"><strong>Responsabilidad N°3 Proveedor: </strong> -------------------- </span></br>';
                                        $MSJ_Responsabilidad3 = '';
                                    }
                                    if ($Responsabilidad4 != "") {
                                        $MSJ_Responsabilidad4 = '<span class="text-muted"><strong>Responsabilidad N°4 Proveedor: </strong> ' . $Responsabilidad4 . '</span></br>';
                                    } else {
                                        //$MSJ_Responsabilidad4 = '<span class="text-muted"><strong>Responsabilidad N°4 Proveedor: </strong> -------------------- </span></br>';
                                        $MSJ_Responsabilidad4 = '';
                                    }
                                    if ($Responsabilidad5 != "") {
                                        $MSJ_Responsabilidad5 = '<span class="text-muted"><strong>Responsabilidad N°5 Proveedor: </strong> ' . $Responsabilidad5 . '</span></br>';
                                    } else {
                                        //$MSJ_Responsabilidad5 = '<span class="text-muted"><strong>Responsabilidad N°5 Proveedor: </strong> -------------------- </span></br>';
                                        $MSJ_Responsabilidad5 = '';
                                    }
                                    if ($Responsabilidad6 != "") {
                                        $MSJ_Responsabilidad6 = '<span class="text-muted"><strong>Responsabilidad N°6 Proveedor: </strong> ' . $Responsabilidad6 . '</span></br>';
                                    } else {
                                        //$MSJ_Responsabilidad6 = '<span class="text-muted"><strong>Responsabilidad N°6 Proveedor: </strong> -------------------- </span></br>';
                                        $MSJ_Responsabilidad6 = '';
                                    }
                                    echo '</br>';
                                    echo '<span class="text-muted"><strong>Proveedor: </strong> ' . $row_prove['provee_nombre_fantasia'] . '</span></br>';
                                    echo $MSJ_Responsabilidad1;
                                    echo $MSJ_Responsabilidad2;
                                    echo $MSJ_Responsabilidad3;
                                    echo $MSJ_Responsabilidad4;
                                    echo $MSJ_Responsabilidad5;
                                    echo $MSJ_Responsabilidad6;
                                endforeach;
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Usuarios y Contraseñas Aplicación  - <a href="#formModal_UpdateUserContra" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                <?php
                                $datos_sql_select_usuarios_contraseña_aplica = $db->query("SELECT * FROM `Aplicaciones_Respaldos` WHERE `Servicios_serv_id` = '" . $row_serv['serv_id'] . "'");
                                foreach ($datos_sql_select_usuarios_contraseña_aplica as $row_aplica_respal):
                                    echo '<textarea class="form-control" placeholder="" rows="8" id="" name="" disabled>' . $row_aplica_respal["aplicacion_respaldo_users_pass_aplicaciones"] . '</textarea>';
                                endforeach;
                                ?>
                            </div>
                            <div class="col-md-6">
                                <h5>Usuarios y Contraseñas BD  - <a href="#formModal_UpdateUserContraBD" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit" ></i></a></h5>
                                <?php
                                //$datos_sql_select_usuarios_contraseña_bd = $db->query("SELECT * FROM `Aplicaciones_Respaldos` WHERE `Servicios_serv_id` = '" . $row_serv['serv_id'] . "'");
                                foreach ($datos_sql_select_usuarios_contraseña_aplica as $row_aplica_respal):
                                    echo '<textarea class="form-control" placeholder="" rows="8" id="" name="" disabled>' . $row_aplica_respal["aplicacion_respaldo_users_pass_BD"] . '</textarea>';
                                endforeach;
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Rutinas de Mantenimiento  - <a href="#formModal_UpdateRutinasMantenimiento" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                <?php
                                //$datos_sql_select_rutinas_mantenimiento = $db->query("SELECT * FROM `Aplicaciones_Respaldos` WHERE `Servicios_serv_id` = '" . $row_serv['serv_id'] . "'");
                                foreach ($datos_sql_select_usuarios_contraseña_aplica as $row_aplica_respal):
                                    echo '<textarea class="form-control" placeholder="" rows="3" id="" name="" disabled>' . $row_aplica_respal["aplicacion_respaldo_rutina_mantenimiento"] . '</textarea>';
                                endforeach;
                                ?>
                            </div>
                            <div class="col-md-6">
                                <h5>Problema Recurrente  - <a href="#formModal_UpdateProblemaRecurrente" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit" ></i></a></h5>
                                <?php
                                //$datos_sql_select_problema_recurrente = $db->query("SELECT aplicacion_respaldo_problema_recurrente FROM `Aplicaciones_Respaldos` WHERE `Servicios_serv_id` = '" . $row_serv['serv_id'] . "'");
                                foreach ($datos_sql_select_usuarios_contraseña_aplica as $row_aplica_respal):
                                    echo '<textarea class="form-control" placeholder="" rows="3" id="" name="" disabled>' . $row_aplica_respal["aplicacion_respaldo_problema_recurrente"] . '</textarea>';
                                endforeach;
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Elemento Monitoriar  - <a href="#formModal_UpdateElementoMonitoriar" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                <?php
                                //$datos_sql_select_elemento_monitoriar = $db->query("SELECT aplicacion_respaldo_elemento_monitoriar FROM `Aplicaciones_Respaldos` WHERE `Servicios_serv_id` = '" . $row_serv['serv_id'] . "'");
                                foreach ($datos_sql_select_usuarios_contraseña_aplica as $row_aplica_respal):
                                    echo '<textarea class="form-control" placeholder="" rows="3" id="" name="" disabled>' . $row_aplica_respal["aplicacion_respaldo_elemento_monitoriar"] . '</textarea>';
                                endforeach;
                                ?>
                            </div>
                            <div class="col-md-6">
                                <h5>Elemento Respaldo  - <a href="#formModal_UpdateElementoRespaldo" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit" ></i></a></h5>
                                <?php
                                //$datos_sql_select_elemento_respaldo = $db->query("SELECT aplicacion_respaldo_elemento FROM `Aplicaciones_Respaldos` WHERE `Servicios_serv_id` = '" . $row_serv['serv_id'] . "'");
                                foreach ($datos_sql_select_usuarios_contraseña_aplica as $row_aplica_respal):
                                    echo '<textarea class="form-control" placeholder="" rows="3" id="" name="" disabled>' . $row_aplica_respal["aplicacion_respaldo_elemento"] . '</textarea>';
                                endforeach;
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Gestor de Monitoreo  - <a href="#formModal_UpdateGestorMonitoreo" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                <?php
                                $datos_sql_select_gestor_monitoreo = $db->query("SELECT * FROM Aplicaciones_Respaldos AP, Gestores_Monitoreo GM WHERE AP.Servicios_serv_id = '" . $row_serv['serv_id'] . "' AND AP.Gestores_Monitoreo_gestor_monitoreo_id = GM.gestor_monitoreo_id");
                                foreach ($datos_sql_select_gestor_monitoreo as $row_aplica_respal):
                                    echo '<textarea class="form-control" placeholder="" rows="3" id="" name="" disabled>' . $row_aplica_respal["gestor_monitoreo_nombre"] . '</textarea>';
                                endforeach;
                                ?>
                            </div>
                            <div class="col-md-6">
                                <h5>Gestor de Respaldo  - <a href="#formModal_UpdateGestorRespaldo" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit" ></i></a></h5>
                                <?php
                                $datos_sql_select_gestor_respaldo = $db->query("SELECT * FROM Aplicaciones_Respaldos AP, Gestores_Respaldos GR WHERE AP.Servicios_serv_id = '" . $row_serv['serv_id'] . "' AND AP.Gestores_Respaldos_gestor_respaldo_id = GR.gestor_respaldo_id");
                                foreach ($datos_sql_select_gestor_respaldo as $row_aplica_respal):
                                    echo '<textarea class="form-control" placeholder="" rows="3" id="" name="" disabled>' . $row_aplica_respal["gestor_respaldo_nombre"] . '</textarea>';
                                endforeach;
                                ?>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Detalle Licencia  - <a href="#formModal_UpdateDetalleLicencia" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                <?php
                                $datos_sql_select_detalle_licencia = $db->query("SELECT * FROM Aplicaciones_Licencias AL, Proveedores P WHERE AL.Servicios_serv_id = '" . $row_serv['serv_id'] . "' AND AL.Proveedores_provee_id = P.provee_id");
                                foreach ($datos_sql_select_detalle_licencia as $row_aplica_respal):
                                    echo '<textarea class="form-control" placeholder="" rows="3" id="" name="" disabled>' . $row_aplica_respal["aplicacion_licencia_detalle"] . '</textarea>';
                                endforeach;
                                ?>
                            </div>
                            <div class="col-md-6">
                                <h5>Fecha Termino Licencia  - <a href="#formModal_UpdateFechaTermino" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                <?php
                                $datos_sql_select_detalle_insumos_fecha = $db->query("SELECT *,date_format(aplicacion_licencia_fecha_termino,'%d-%m-%Y') as aplicacion_licencia_fecha_termino FROM Aplicaciones_Licencias AL, Proveedores P WHERE AL.Servicios_serv_id = '" . $row_serv['serv_id'] . "' AND AL.Proveedores_provee_id = P.provee_id");
                                foreach ($datos_sql_select_detalle_insumos_fecha as $row_aplica_respal):
                                    echo '<span class="text-muted"><strong>Marca: </strong> ' . $row_aplica_respal['aplicacion_licencia_fecha_termino'] . '</span></br>';
                                endforeach;
                                ?>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Detalle Insumos  - <a href="#formModal_UpdateDetalleInsumos" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                <?php
                                $datos_sql_select_detalle_insumos = $db->query("SELECT * FROM Aplicaciones_Insumos AI, Proveedores P WHERE AI.Servicios_serv_id = '" . $row_serv['serv_id'] . "' AND AI.Proveedores_provee_id = P.provee_id");
                                foreach ($datos_sql_select_detalle_insumos as $row_aplica_respal):
                                    echo '<hr>';
                                    echo '<span class="text-muted"><strong>Marca: </strong> ' . $row_aplica_respal['aplicacion_marca'] . '</span></br>'
                                    . '<span class="text-muted"><strong>Modelo: </strong> ' . $row_aplica_respal['aplicacion_modelo'] . '</span></br>'
                                    . '<span class="text-muted"><strong>Caracteristica: </strong> ' . $row_aplica_respal['aplicacion_caracteristica'] . '</span></br>';
                                endforeach;
                                ?>
                            </div>
                        </div>
                        <hr>
                        <div class="padding-sm bg-grey">
                            <h5><a href="#formModal_UpdateNotaServicio" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a> - Nota: </h5>
                            <textarea class="form-control input-sm" id="" name="" rows="3" disabled="" placeholder="" maxlength="141" rows="3"><?php echo $row_serv["serv_nota"]; ?></textarea>
                        </div>
                    </div>
                </div><!-- /.padding20 -->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/confirmation.logout.inc.php'; ?> 
        <!-- ######################################## -->
        <!-- MODAL'S -->
        <!-- ########################################################### -->
        <form id="form_update_NombreServicio" method="POST">
            <div class="modal fade" id="formModal_UpdateNombreServicio">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="servicio_nombre">Nombre del Servicio</label>
                                        <input type="text" id="servicio_nombre_update" name="servicio_nombre_update" value="<?php echo $row_serv["serv_nombre"]; ?>" placeholder="Denominación Oficial del Proyecto" class="form-control input-sm" required="required">
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_AliasServicio" method="POST">
            <div class="modal fade" id="formModal_UpdateAliasServicio">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Alias del Servicio</label>
                                        <input type="text" id="servicio_alias_update" name="servicio_alias_update" value="<?php echo $row_serv["serv_alias"]; ?>" placeholder="Otro Nombre del Proyecto" class="form-control input-sm">
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_descripcion" method="POST">
            <div class="modal fade" id="formModal_UpdateDescripcion">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">

                                        <textarea class="form-control input-sm" id="servicio_descripcion_update" name="servicio_descripcion_update" rows="4" data-required="true" ><?php echo $row_serv["serv_descripcion"]; ?></textarea>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_FechaMarchaBlanca" method="POST">
            <div class="modal fade" id="formModal_UpdateFechaMarchaBlanca">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" id="servicio_fecha_marcha_blanca_update" name="servicio_fecha_marcha_blanca_update" value="<?php echo $row_serv["serv_fecha_marcha_blanca"]; ?>" class="datepicker form-control input-sm" placeholder="Fecha de Inicio de Pruebas">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_TiempoMarchaBlanca" method="POST">
            <div class="modal fade" id="formModal_UpdateTiempoMarchaBlanca">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input id="servicio_tiempo_marcha_blanca_update" name="servicio_tiempo_marcha_blanca_update" value="<?php echo $row_serv["serv_tiempo_marcha_blanca"]; ?>" class="form-control input-sm" type="text" placeholder="ej: 2 Semanas, 3 Semanas ó más."/>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_FechaProduccion" method="POST">
            <div class="modal fade" id="formModal_UpdateFechaProduccion">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" id="servicio_fecha_produccion_update" name="servicio_fecha_produccion_update" value="<?php echo $row_serv["serv_fecha_produccion"]; ?>" class="datepicker form-control input-sm" placeholder="Fecha Oficial de Inicio en Operaciones">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_Encargados" method="POST">
            <div class="modal fade" id="formModal_UpdateEncargados">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php
                                        $_SESSION["encarga_id"] = array();
                                        foreach ($datos_sql_select_encargados as $row_encarga):
                                            $resultado = count($datos_sql_select_encargados);
                                            while ($num <= $resultado) {
                                                $suma = $suma + $num;
                                                $num = $num + 1;
                                                echo '<label class="control-label">Nombre del Encargado del ' . $row_encarga['encarga_escalamiento'] . ' de ' . $row_encarga['encarga_dpto'] . '</label>'
                                                . '<input type="text" id="encarga_nombre_update' . $num . '" name="encarga_nombre_update' . $num . '" placeholder="" class="form-control input-sm" value="' . $row_encarga['encarga_nombre'] . '">'
                                                . '<label class="control-label">Email del Encargado del ' . $row_encarga['encarga_escalamiento'] . ' de ' . $row_encarga['encarga_dpto'] . '</label>'
                                                . '<input type="text" id="encarga_email_update' . $num . '" name="encarga_email_update' . $num . '" placeholder="ej. encargado@entel.cl" class="form-control input-sm" value="' . $row_encarga['encarga_email'] . '"></br>';

                                                break;
                                            }
                                            array_push($_SESSION["encarga_id"], $row_encarga['encarga_id']);

                                        endforeach;
                                        //print_r($_SESSION["encarga_id"]);
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_Patrocinador" method="POST">
            <div class="modal fade" id="formModal_UpdatePatrocinador">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php
                                        $_SESSION["patroci_id"] = array();
                                        foreach ($datos_sql_select_patrocina as $row_patrocina):
                                            $resultado_patrocina = count($datos_sql_select_patrocina);
                                            while ($num_patrocina <= $resultado_patrocina) {
                                                $suma_patrocina = $suma_patrocina + $num_patrocina;
                                                //echo "$num_patrocina<br>";
                                                $num_patrocina = $num_patrocina + 1;
                                                echo '<label class="control-label">Nombre del Patrocinador</label>'
                                                . '<input type="text" id="servicio_nombre_patrocinador_update' . $num_patrocina . '" name="servicio_nombre_patrocinador_update' . $num_patrocina . '" value="' . $row_patrocina["patroci_nombre"] . '" placeholder="Nombre del Responsable" class="form-control input-sm"></br>'
                                                . '<label class="control-label">Email del Patrocinador</label>'
                                                . '<input type="text" id="servicio_email_patrocinador_update' . $num_patrocina . '" name="servicio_email_patrocinador_update' . $num_patrocina . '" placeholder="ej. patrocinador@entel.cl" value="' . $row_patrocina["patroci_email"] . '" class="form-control input-sm"></br>';
                                                break;
                                            }
                                            array_push($_SESSION["patroci_id"], $row_patrocina["patroci_id"]);
                                        endforeach;
                                        //print_r($_SESSION["patroci_id"]);
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_JefeProyecto" method="POST">
            <div class="modal fade" id="formModal_UpdateJefeProyecto">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php
                                        $_SESSION["jefesproyec_id"] = array();
                                        foreach ($datos_sql_select_jefeproyec as $row_jefeproyec):
                                            $resultado_jefeproyec = count($datos_sql_select_jefeproyec);
                                            while ($num_jefeproyec <= $resultado_jefeproyec) {
                                                $suma_jefeproyec = $suma_jefeproyec + $num_jefeproyec;
                                                //echo "$num_jefeproyec<br>";
                                                $num_jefeproyec = $num_jefeproyec + 1;
                                                echo '<label class="control-label">Nombre Jefe de Proyecto</label>'
                                                . '<input type="text" id="servicio_nombre_jefe_proyecto_update' . $num_jefeproyec . '" name="servicio_nombre_jefe_proyecto_update' . $num_jefeproyec . '" value="' . $row_jefeproyec["jefesproyec_nombre"] . '" placeholder="Nombre del Responsable" class="form-control input-sm"></br>'
                                                . '<label class="control-label">Email del Jefe Proyecto</label>'
                                                . '<input type="text" id="servicio_email_jefe_proyecto_update' . $num_jefeproyec . '" name="servicio_email_jefe_proyecto_update' . $num_jefeproyec . '" value="' . $row_jefeproyec["jefesproyec_email"] . '" placeholder="ej. jefeproyecto@entel.cl" class="form-control input-sm">';
                                                break;
                                            }
                                            array_push($_SESSION["jefesproyec_id"], $row_jefeproyec["jefesproyec_id"]);
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_Arquitecto" method="POST">
            <div class="modal fade" id="formModal_UpdateArquitecto">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php
                                        $_SESSION["arquitec_id"] = array();
                                        foreach ($datos_sql_select_arquitecto as $row_arquitecto):
                                            $resultado_arquitecto = count($datos_sql_select_arquitecto);
                                            while ($num_arquitecto <= $resultado_arquitecto) {
                                                $suma_arquitecto = $suma_arquitecto + $num_arquitecto;
                                                //echo "$num_arquitecto<br>";
                                                $num_arquitecto = $num_arquitecto + 1;
                                                echo '<label class="control-label">Nombre del Arquitecto</label>'
                                                . '<input type="text" id="servicio_nombre_arquitecto_update' . $num_arquitecto . '" name="servicio_nombre_arquitecto_update' . $num_arquitecto . '" value="' . $row_arquitecto["arquitec_nombre"] . '" placeholder="Nombre del Responsable" class="form-control input-sm" data-required="true" data-minlength="8"></br>'
                                                . '<label class="control-label">Email del Arquitecto</label>'
                                                . '<input type="text" id="servicio_email_arquitecto_update' . $num_arquitecto . '" name="servicio_email_arquitecto_update' . $num_arquitecto . '" value="' . $row_arquitecto["arquitec_email"] . '" placeholder="ej. arquitecto@entel.cl" class="form-control input-sm">';
                                                break;
                                            }
                                            array_push($_SESSION["arquitec_id"], $row_arquitecto["arquitec_id"]);
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_Cliente" method="POST">
            <div class="modal fade" id="formModal_UpdateClientes">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php
                                        $_SESSION["client_id"] = array();
                                        foreach ($datos_sql_select_clientes as $row_clientes):
                                            $resultado_cliente = count($datos_sql_select_cliente);
                                            while ($num_cliente <= $resultado_cliente) {
                                                $suma_cliente = $suma_cliente + $num_cliente;
                                                //echo "$num_cliente<br>";
                                                $num_cliente = $num_cliente + 1;
                                                echo '<label class="control-label">Nombre Área Cliente</label>'
                                                . '<input type="text" id="servicio_nombre_area_cliente_update' . $num_cliente . '" name="servicio_nombre_area_cliente_update' . $num_cliente . '" placeholder="" class="form-control input-sm" value="' . $row_clientes["client_area"] . '">'
                                                . '<label class="control-label">Nombre Contacto Cliente</label>'
                                                . '<input type="text" id="servicio_nombre_contacto_cliente_update' . $num_cliente . '" name="servicio_nombre_contacto_cliente_update' . $num_cliente . '" placeholder="" class="form-control input-sm" value="' . $row_clientes["cliente_nombre_contacto"] . '">'
                                                . '<label class="control-label">Email Contacto Cliente</label>'
                                                . '<input type="text" id="servicio_email_contacto_cliente_update' . $num_cliente . '" name="servicio_email_contacto_cliente_update' . $num_cliente . '" placeholder="ej. cliente@entel.cl" class="form-control input-sm" value="' . $row_clientes["client_email"] . '">'
                                                . '<label class="control-label">Motivo Cliente</label>'
                                                . '<input type="text" id="servicio_motivo_cliente_update' . $num_cliente . '" name="servicio_motivo_cliente_update' . $num_cliente . '" placeholder="Necesidad Cubierta por el Servicio" class="form-control input-sm" value="' . $row_clientes["client_motivo"] . '">';
                                                break;
                                            }
                                            array_push($_SESSION["client_id"], $row_clientes["client_id"]);
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_Proveedor" method="POST">
            <div class="modal fade" id="formModal_UpdateProveedor">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Fecha Marcha Blanca a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Seleccione Proveedor</label>

                    <!--<select class="form-control input-sm" id="selec_proveedor_soporte_update" name="selec_proveedor_soporte_update" required="required">-->
                                        <?php
                                        $_SESSION["respons_provee_servicio_id"] = array();
                                        $_SESSION["Servicios_serv_id"] = array();
                                        $_SESSION["Proveedores_provee_id"] = array();
                                        echo '<span class="text-muted"><strong>Proveedor: </strong> ' . $row_proveedor['provee_nombre_empresa'] . '</span></br>';
                                        ?>
                        <!--<option value="<?php //echo $row_proveedor['provee_nombre_empresa'];      ?>"> <?php //echo "Seleccionada: ".$row_proveedor['provee_nombre_empresa'];      ?></option>-->
                                        <?php
                                        //$datos_sql_select_proveedor = $db->query("SELECT * FROM Proveedores");
                                        foreach ($datos_sql_select_responProveeServ as $responProveeServ):
                                            /* echo "<option value=" . $row["provee_id"] . ">" . $row["provee_nombre_empresa"] . "</option>";
                                              echo "</select>"; */
                                            $resultado_proveedor = count($datos_sql_select_responProveeServ);
                                            while ($num_proveedor <= $resultado_proveedor) {
                                                $suma_proveedor = $suma_proveedor + $num_proveedor;
                                                //echo "$num_proveedor<br>";
                                                $num_proveedor = $num_proveedor + 1;
                                                echo '<label class="control-label">Responsabilidad Proveedor</label>'
                                                . '<input type="text" id="servicio_responsabilidad_proveedor_update' . $num_proveedor . '" value="' . $responProveeServ['respons_provee_servicio1'] . '" name="servicio_responsabilidad_proveedor_update' . $num_proveedor . '" placeholder="Hardware, Software, Aplicaciones, Etc" class="form-control input-sm">';
                                                break;
                                            }
                                            array_push($_SESSION["respons_provee_servicio_id"], $responProveeServ["respons_provee_servicio_id"]);
                                            array_push($_SESSION["Servicios_serv_id"], $responProveeServ["Servicios_serv_id"]);
                                            array_push($_SESSION["Proveedores_provee_id"], $responProveeServ["Proveedores_provee_id"]);
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left" id="msj_respuesta_update_responsabilidad_proveedor">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_UserContra" method="POST">
            <div class="modal fade" id="formModal_UpdateUserContra">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Modificar Usuarios y Contraseñas Aplicación</label>
                                        <?php
                                        foreach ($datos_sql_select_usuarios_contraseña_aplica as $row_aplica_respal):
                                            echo '<input type="text" id="UserContraID" name="UserContraID" value="' . $row_aplica_respal["aplicacion_respaldo_id"] . '" class="hidden">';
                                            echo '<textarea class="form-control" placeholder="" rows="8" id="UpdateUserContraAplica" name="UpdateUserContraAplica">' . $row_aplica_respal["aplicacion_respaldo_users_pass_aplicaciones"] . '</textarea>';
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left " id="msj_respuesta_update_UserContra">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_UserContraBD" method="POST">
            <div class="modal fade" id="formModal_UpdateUserContraBD">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Modificar Usuarios y Contraseñas BD</label>
                                        <?php
                                        foreach ($datos_sql_select_usuarios_contraseña_aplica as $row_aplica_respal):
                                            echo '<input type="text" id="UserContraID" name="UserContraID" value="' . $row_aplica_respal["aplicacion_respaldo_id"] . '" class="hidden">';
                                            echo '<textarea class="form-control" placeholder="" rows="8" id="UpdateUserContraBD" name="UpdateUserContraBD" >' . $row_aplica_respal["aplicacion_respaldo_users_pass_BD"] . '</textarea>';
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left " id="msj_respuesta_update_UserContraBD">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_RutinasMantenimiento" method="POST">
            <div class="modal fade" id="formModal_UpdateRutinasMantenimiento">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Modificar Rutinas de Mantenimiento</label>
                                        <?php
                                        foreach ($datos_sql_select_usuarios_contraseña_aplica as $row_aplica_respal):
                                            echo '<input type="text" id="UserContraID" name="UserContraID" value="' . $row_aplica_respal["aplicacion_respaldo_id"] . '" class="hidden">';
                                            echo '<textarea class="form-control" placeholder="" rows="3" id="UpdateRutinasMantenimiento" name="UpdateRutinasMantenimiento">' . $row_aplica_respal["aplicacion_respaldo_rutina_mantenimiento"] . '</textarea>';
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left " id="msj_respuesta_UpdateRutinasMantenimiento">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_problema_recurrente" method="POST">
            <div class="modal fade" id="formModal_UpdateProblemaRecurrente">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Modificar Problema Recurrente</label>
                                        <?php
                                        foreach ($datos_sql_select_usuarios_contraseña_aplica as $row_aplica_respal):
                                            echo '<input type="text" id="UserContraID" name="UserContraID" value="' . $row_aplica_respal["aplicacion_respaldo_id"] . '" class="hidden">';
                                            echo '<textarea class="form-control" placeholder="" rows="3" id="UpdateProblemaRecurrente" name="UpdateProblemaRecurrente">' . $row_aplica_respal["aplicacion_respaldo_problema_recurrente"] . '</textarea>';
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left " id="msj_respuesta_UpdateProblemaRecurrente">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_elemento_monitoriar" method="POST">
            <div class="modal fade" id="formModal_UpdateElementoMonitoriar">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Modificar Elemento Monitoriar</label>
                                        <?php
                                        foreach ($datos_sql_select_usuarios_contraseña_aplica as $row_aplica_respal):
                                            echo '<input type="text" id="UserContraID" name="UserContraID" value="' . $row_aplica_respal["aplicacion_respaldo_id"] . '" class="hidden">';
                                            echo '<textarea class="form-control" placeholder="" rows="3" id="UpdateElementoMonitoriar" name="UpdateElementoMonitoriar">' . $row_aplica_respal["aplicacion_respaldo_elemento_monitoriar"] . '</textarea>';
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left " id="msj_respuesta_UpdateElementoMonitoriar">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_elemento_respaldo" method="POST">
            <div class="modal fade" id="formModal_UpdateElementoRespaldo">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Modificar Elemento Respaldo</label>
                                        <?php
                                        foreach ($datos_sql_select_usuarios_contraseña_aplica as $row_aplica_respal):
                                            echo '<input type="text" id="UserContraID" name="UserContraID" value="' . $row_aplica_respal["aplicacion_respaldo_id"] . '" class="hidden">';
                                            echo '<textarea class="form-control" placeholder="" rows="3" id="UpdateElementoRespaldo" name="UpdateElementoRespaldo">' . $row_aplica_respal["aplicacion_respaldo_elemento"] . '</textarea>';
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left " id="msj_respuesta_UpdateElementoRespaldo">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_gestor_monitoreo" method="POST">
            <div class="modal fade" id="formModal_UpdateGestorMonitoreo">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php
                                        foreach ($datos_sql_select_gestor_monitoreo as $row_aplica_respal):
                                            echo '<input type="text" id="UserContraID" name="UserContraID" value="' . $row_aplica_respal["aplicacion_respaldo_id"] . '" class="hidden">';
                                            echo '<label class="control-label">Gestor monitoreo seleccionado: ' . $row_aplica_respal["gestor_monitoreo_nombre"] . '</label>';
                                        endforeach;
                                        ?>
                                        <select class="form-control input-sm" id="selec_gestor_monitoreo" name="selec_gestor_monitoreo" required="required">
                                            <option value=""> Seleccione Gestor de Monitoreo a modificar</option>
                                            <?php
                                            foreach ($db->query("SELECT * FROM Gestores_Monitoreo") as $row):
                                                echo "<option value=" . $row["gestor_monitoreo_id"] . ">" . $row["gestor_monitoreo_nombre"] . "</option>";
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left " id="msj_respuesta_UpdateGestorMonitoreo">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_gestor_respaldo" method="POST">
            <div class="modal fade" id="formModal_UpdateGestorRespaldo">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php
                                        foreach ($datos_sql_select_gestor_respaldo as $row_aplica_respal):
                                            echo '<input type="text" id="UserContraID" name="UserContraID" value="' . $row_aplica_respal["aplicacion_respaldo_id"] . '" class="hidden">';
                                            echo '<label class="control-label">Gestor respaldo seleccionado: ' . $row_aplica_respal["gestor_respaldo_nombre"] . '</label>';
                                        endforeach;
                                        ?>
                                        <select class="form-control input-sm" id="selec_gestor_respaldo" name="selec_gestor_respaldo" required="required">
                                            <option value=""> Seleccione Gestor de Monitoreo a modificar</option>
                                            <?php
                                            foreach ($db->query("SELECT * FROM Gestores_Respaldos") as $row):
                                                echo "<option value=" . $row["gestor_respaldo_id"] . ">" . $row["gestor_respaldo_nombre"] . "</option>";
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left " id="msj_respuesta_UpdateGestorRespaldo">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_detalleLicencia" method="POST">
            <div class="modal fade" id="formModal_UpdateDetalleLicencia">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php
                                        foreach ($datos_sql_select_detalle_licencia as $row_aplica_respal):
                                            echo '<input type="text" id="UserContraID" name="UserContraID" value="' . $row_aplica_respal["aplicacion_licencia_id"] . '" class="hidden">';
                                            echo '<textarea class="form-control" placeholder="" rows="3" id="UpdateDetalleLicencia" name="UpdateDetalleLicencia" >' . $row_aplica_respal["aplicacion_licencia_detalle"] . '</textarea>';
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left " id="msj_respuesta_UpdateDetalleLicencia">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_fecha_termino" method="POST">
            <div class="modal fade" id="formModal_UpdateFechaTermino">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php
                                        foreach ($datos_sql_select_detalle_insumos_fecha as $row_aplica_respal):
                                            echo '<input type="text" id="UserContraID" name="UserContraID" value="' . $row_aplica_respal["aplicacion_licencia_id"] . '" class="hidden">';
                                            echo '<input type="text" id="Updatefecha_termino_licencia" value="' . $row_aplica_respal["aplicacion_licencia_fecha_termino"] . '" name="Updatefecha_termino_licencia" placeholder="" class="datepicker form-control input-sm">';
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left " id="msj_respuesta_UpdateFechaTermino">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_detalle_insumos" method="POST">
            <div class="modal fade" id="formModal_UpdateDetalleInsumos">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php
                                        foreach ($datos_sql_select_detalle_insumos as $row_aplica_respal):
                                            echo '<input type="text" id="InsumoID" name="InsumoID" value="' . $row_aplica_respal["aplicacion_insumo_id"] . '" class="hidden">';
                                            echo '<label class="control-label">Marca</label></br>'
                                            . '<input type="text" id="UpdateInsumoMarca" value="' . $row_aplica_respal["aplicacion_marca"] . '" name="UpdateInsumoMarca" placeholder="" class="datepicker form-control input-sm">'
                                            . '<label class="control-label">Modelo</label></br>'
                                            . '<input type="text" id="UpdateModeloInsumo" value="' . $row_aplica_respal["aplicacion_modelo"] . '" name="UpdateModeloInsumo" placeholder="" class="datepicker form-control input-sm">'
                                            . '<label class="control-label">Caracteristica</label></br>'
                                            . '<input type="text" id="UpdateCaracteriticaInsumo" value="' . $row_aplica_respal["aplicacion_caracteristica"] . '" name="UpdateCaracteriticaInsumo" placeholder="" class="datepicker form-control input-sm">';
                                        endforeach;
                                        ?>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left " id="msj_respuesta_UpdateDetalleInsumo">                                    
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_NotaServicio" method="POST">
            <div class="modal fade" id="formModal_UpdateNotaServicio">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Nota</label>
                                        <?php
                                        $_SESSION["serv_id"] = array();
                                        array_push($_SESSION["serv_id"], $row_serv["serv_id"]);
                                        ?>
                                        <textarea class="form-control input-sm" id="servicio_nota_update" name="servicio_nota_update" rows="3" placeholder="Solo se Permiten 140 Caracteres" maxlength="141" rows="3"><?php echo $row_serv["serv_nota"]; ?></textarea>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

            });
        </script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>
        <script src='js/datepicker_js/bootstrap-datepicker.js'></script>
        <script src='js/datepicker_js/locales/bootstrap-datepicker.es.js'></script>
        <script>
            $(document).ready(function() {
                $('#servicio_fecha_produccion_update').datepicker({
                    language: "es",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    todayHighlight: true
                });
                $('#servicio_fecha_marcha_blanca_update').datepicker({
                    language: "es",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    todayHighlight: true
                });
                $('#Updatefecha_termino_licencia').datepicker({
                    language: "es",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    todayHighlight: true
                });
                $("textarea").keydown(function(e) {
                    if (e.keyCode === 9) { // tab was pressed
                        // get caret position/selection
                        var start = this.selectionStart;
                        end = this.selectionEnd;

                        var $this = $(this);

                        // set textarea value to: text before caret + tab + text after caret
                        $this.val($this.val().substring(0, start) + "\t" + $this.val().substring(end));

                        // put caret at right position again
                        this.selectionStart = this.selectionEnd = start + 1;

                        // prevent the focus lose
                        return false;
                    }
                });
            });
        </script>
        <script src="js/menu-active-class.js"></script>
        <script src='js/modernizr.min.js'></script>
        <script src='js/pace.min.js'></script>
        <script src='js/jquery.popupoverlay.min.js'></script>
        <script src='js/jquery.slimscroll.min.js'></script>
        <script src='js/jquery.cookie.min.js'></script>
        <script src="js/endless/endless.js"></script>

        <script>

            $(function() {
                $('#invoicePrint').click(function() {
                    window.print();
                });
            });
        </script>



        <!-- ########################################################### -->

    </body>
</html>
<?php
?>
