<?php
/*
  -----------------------------------------------------------------------
  DATOS DE CONTACTO
  -----------------------------------------------------------------------
  Desarrollador: Victor Manuel Diaz Galdames.
  Nivel Profesional: Ingeniero Informático
  Número Contacto (Móvil): +5699491896
  Correo Eletrónico: victordiazgaldames@gmail.com
  Curriculum Vitae: http://victordiaz.nfconnection.cl/
  Nacionalidad: Chileno
  Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
  -----------------------------------------------------------------------
  DATOS DE JEFE A CARGO
  -----------------------------------------------------------------------
  Nombre: Nelson Horacio Guzman Mora.
  Número Contacto (Móvil): +56998289358
  Correo Eletrónico: nguzman@entel.cl
  Cargo: Jefe Operación Plataformas
 */
?>
<?php
$provee_id = $_GET['provee_id'];
session_start();
$_SESSION["provee_id"] = $provee_id;
require_once './controlador/Db.class.php';
$db = new Db();
//LLENADO DEL SELECT
$datos_sql_select_proveedor = $db->query("SELECT * FROM Proveedores WHERE provee_id = '" . $provee_id . "'");
if ($datos_sql_select_proveedor == false) {
    header('Location: ./error404.php');
}
foreach ($datos_sql_select_proveedor as $row_proveedor):

endforeach;
?>
<!DOCTYPE html>
<html lang="en">    
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>

        <div id="wrapper" class="bg-white preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->

            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div class="padding-md">
                    <div class="clearfix">
                        <div class="pull-left">
                            <span class="">
                                <a href="#formModal_UpdateImagenProveedor" role="button" data-toggle="modal" class="btn-link"><img class="img-demo img-responsive" style="width: 150px; height: 150px;" src="<?php
                                    if ($row_proveedor["provee_img_url"] == "") {
                                        echo "img/img_default.png";
                                    } else {
                                        echo $row_proveedor["provee_img_url"];
                                    }
                                    ?>" ></a>
                            </span>
                            <div class="pull-left m-left-sm">
                                <h3 class="m-bottom-xs m-top-xs"><?php echo $row_proveedor["provee_nombre_empresa"]; ?> <a href="#formModal_UpdateNombreProveedor" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h3>
                                <span class="text-muted"><strong>Nombre de Fantasía o Alias:</strong> <?php echo $row_proveedor["provee_nombre_fantasia"]; ?> - <a href="#formModal_UpdateAliasProveedor" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                <span class="text-muted"><strong>Sitio Web: </strong><a href="#" target="_blank" class="btn-link"> <?php echo $row_proveedor["provee_sitio_web"]; ?></a> - <a href="#formModal_UpdateSitioWeb" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                <span class="text-muted"><strong>N° Mesa de Ayuda:</strong> <?php echo $row_proveedor["provee_num_mesa_ayuda"]; ?> - <a href="#formModal_UpdateNumeroMesAyuda" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                <span class="text-muted"><strong>Correo Electrónico:</strong> <?php echo $row_proveedor["provee_email"]; ?> - <a href="#formModal_UpdateEmailProveedores" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                <span class="text-muted"><strong>Dirección:</strong> <?php echo $row_proveedor["provee_direccion"]; ?> - <a href="#formModal_UpdateDireccionProveedor" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></span></br>
                                <span class="text-muted"><strong>RUT:</strong> <?php echo $row_proveedor["provee_rut"]; ?></span>
                                
                            </div>
                        </div>
                        <div class="pull-right"><!--
                            <h5><strong>Código Proveedor: </strong>#82370</h5>
                            <span><strong>Ingreso de Proveedor a Inventario: </strong>9 de Enero del 2015</span>-->
                            <a class="btn btn-danger btn-block btn-sm" href="<?php echo $row_proveedor["provee_contrato_url"]; ?>" download="<?php echo date("d-m-Y")." - Contrato: ".$row_proveedor["provee_nombre_empresa"]; ?>.pdf" target="_blank"><i class="fa fa-download"></i> Descargar Contrato (.pdf)</a>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5>Contacto Administrativo - <a href="#formModal_UpdateContactoAdministrativo" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5>
                                        <?php
                                        $sql_selec_administrativos = $db->query("SELECT * FROM Administrativos WHERE Proveedores_provee_id = '" . $provee_id . "'");
                                        foreach ($sql_selec_administrativos as $row_adminis):
                                            echo '<hr>';
                                            echo '<span class="text-muted"><strong>Nombre: </strong> ' . $row_adminis['administra_nombre'] . '</span></br>'
                                            . '<span class="text-muted"><strong>Cargo: </strong> ' . $row_adminis['administra_cargo'] . '</span></br>'
                                            . '<span class="text-muted"><strong>Correo Electrónico: </strong> ' . $row_adminis['administra_email'] . '</span></br>'
                                            . '<span class="text-muted"><strong>Teléfono Móvil: </strong> ' . $row_adminis['administra_tel_movil'] . '</span></br>'
                                            . '<span class="text-muted"><strong>Teléfono Fijo: </strong> ' . $row_adminis['administra_tel_fijo'] . '</span></br>';
                                        endforeach;
                                        ?>
                                    </div>
                                    <div class="col-md-6">
                                        <h5>Contacto Técnico - <a href="#formModal_UpdateContactoTecnico" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a></h5> 
                                        <?php
                                        $sql_selec_tecnicos = $db->query("SELECT * FROM Tecnicos WHERE Proveedores_provee_id = '" . $provee_id . "'");
                                        foreach ($sql_selec_tecnicos as $row_tecnico):
                                            echo '<hr>';
                                            echo '<span class="text-muted"><strong>Nombre: </strong> ' . $row_tecnico['tecnico_nombre'] . '</span></br>'
                                            . '<span class="text-muted"><strong>Cargo: </strong> ' . $row_tecnico['tecnico_cargo'] . '</span></br>'
                                            . '<span class="text-muted"><strong>Correo Electrónico: </strong> ' . $row_tecnico['tecnico_email'] . '</span></br>'
                                            . '<span class="text-muted"><strong>Teléfono Móvil: </strong> ' . $row_tecnico['tecnico_tel_movil'] . '</span></br>'
                                            . '<span class="text-muted"><strong>Teléfono Fijo: </strong> ' . $row_tecnico['tecnico_tel_fijo'] . '</span></br>';
                                        endforeach;
                                        ?>
                                    </div>
                                </div>                                
                            </div>                            
                        </div>                        
                    </div>
                    <hr>
                    <h4 class="text-center">Servicios Asociados</h4>
                    <table class="table table-striped m-top-md table-hover" id="dataTable">
                        <thead>
                            <tr class="bg-theme">
                                <!--<th class="text-center">Servicios Asociados</th>-->
                                <th class="text-center">Alias Servicios</th>
                                <th class="text-center">Código Proyecto</th>
                                <!--<th class="text-center">Fecha Marcha Blanca</th>
                                <th class="text-center">Tiempo Marcha Blanca</th>-->
                                <th class="text-center">Fecha Producción</th><!--
                                <th class="text-center">Tipo de Soporte</th>
                                <th class="text-center">Información Soporte</th>-->
                                <th class="text-center">Estado</th>
                                <th class="text-center">Información Servicio</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $datos_sql_select_proveedores_and_servicios_asociados = $db->query("SELECT *,date_format(serv_fecha_marcha_blanca,'%d-%m-%Y') as serv_fecha_marcha_blanca,date_format(serv_fecha_produccion,'%d-%m-%Y') as serv_fecha_produccion  FROM Proveedores P, ResponsProveeServicio RPS, Servicios S WHERE S.serv_id = RPS.Servicios_serv_id AND P.provee_id = '$provee_id' AND RPS.Proveedores_provee_id = '$provee_id'");
                            foreach ($datos_sql_select_proveedores_and_servicios_asociados as $row_asociados):
                                $provee_id = $row_asociados["provee_id"];
                                if($row_asociados["serv_status"] == "Activo"){
                                    $Estado = '<td class="text-center"><i class="fa fa-check-circle-o fa-lg fa-2x" style="color: green;"></i> </td>';
                                }else{
                                    $Estado = '<td class="text-center"><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> </td>';
                                }
                            
                                echo '<tr>'
                                . '<td class="text-center">' . $row_asociados["serv_alias"] . '</td>'
                                . '<td class="text-center">' . $row_asociados["serv_cod_proyec"] . '</td>'
                                . '<td class="text-center">' . $row_asociados["serv_fecha_produccion"] . '</td>';
                                echo $Estado;
                                echo '<td class="text-center"><a href="info_servicio.php?serv_id=' . $row_asociados["serv_id"] . '" class="main-link"><i class="fa fa-info fa-lg"></i> Ver Más</a></td>';
                                echo '</tr>';
                            endforeach;
                            ?>                            
                        </tbody>
                    </table>
                    <hr>
                    <h4 class="text-center">Soportes Asociados</h4>
                    <table class="table table-striped m-top-md table-hover" id="dataTable">
                        <thead>
                            <tr class="bg-theme">
                                <!--<th class="text-center">Servicios Asociados</th>-->
                                <th class="text-center">Termino Soporte</th>
                                <th class="text-center">Tipo Horario del Soporte</th>
                                <th class="text-center">Incluye Respuestos</th>
                                <th class="text-center">Sistema de Tickets</th>
                                <th class="text-center">Tipo de Asistencia</th>
                                <th class="text-center">Tipo de Soporte</th>
                                <th class="text-center">Información Soporte</th>
                                <!--<th class="text-center">Información Servicio</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $datos_sql_select_soportes_asociados = $db->query("SELECT *,date_format(soporte_fecha_termino,'%d-%m-%Y') as soporte_fecha_termino FROM Proveedores P, Soportes SOPO WHERE SOPO.Proveedores_provee_id = '$provee_id' AND P.provee_id = '$provee_id'");
                            foreach ($datos_sql_select_soportes_asociados as $row_asociados):
                                $provee_id = $row_asociados["provee_id"];
                                echo '<tr>'
                                . '<td class="text-center">' . $row_asociados["soporte_fecha_termino"] . '</td>'
                                . '<td class="text-center">' . $row_asociados["soporte_tipo_horario"] . '</td>'
                                . '<td class="text-center">' . $row_asociados["soporte_incluye_respuestos"] . '</td>';
                                echo '<td>';
                                if ($row_asociados["soporte_sistema_tickets"] == "") {
                                    echo 'Sin Sistema Tickets';
                                } else {
                                    echo '<a href="' . $row_asociados["soporte_sistema_tickets"] . '" class="btn-link" target="_blank"> ' . $row_asociados["soporte_sistema_tickets"] . '</a>';
                                }
                                echo '</td>';
                                echo '<td class="text-center">';
                                if ($row_asociados["soporte_tipo_asistencia_presencial"] != "" AND $row_asociados["soporte_tipo_asistencia_remoto"] != "") {
                                            $TipoAsitencia = "Presencial - Remoto";
                                        } elseif ($row_asociados["soporte_tipo_asistencia_presencial"] != "") {
                                            $TipoAsitencia = "Presencial";
                                        } elseif ($row_asociados["soporte_tipo_asistencia_remoto"] != "") {
                                            $TipoAsitencia = "Remoto";
                                        }
                                        echo $TipoAsitencia;
                                echo '</td>';
                                echo '<td>';
                                if ($row_asociados["soporte_tipo_reactivo"] != "" AND $row_asociados["soporte_tipo_proactivo"] != "" AND $row_asociados["soporte_tipo_evolutivo"] != "") {
                                            $TipoSoporte = "Reactivo - Proactivo - Evolutivo";
                                        } elseif ($row_asociados["soporte_tipo_reactivo"] != "" AND $row_asociados["soporte_tipo_evolutivo"] != "") {
                                            $TipoSoporte = "Reactivo - Evolutivo";
                                        } elseif ($row_asociados["soporte_tipo_proactivo"] != "" AND $row_asociados["soporte_tipo_evolutivo"] != "") {
                                            $TipoSoporte = "Proactivo - Evolutivo";
                                        } elseif ($row_asociados["soporte_tipo_reactivo"] != "" AND $row_asociados["soporte_tipo_proactivo"] != "") {
                                            $TipoSoporte = "Reactivo - Proactivo";
                                        } elseif ($row_asociados["soporte_tipo_reactivo"] != "") {
                                            $TipoSoporte = $row_asociados["soporte_tipo_reactivo"];
                                        } elseif ($row_asociados["soporte_tipo_proactivo"] != "") {
                                            $TipoSoporte = $row_asociados["soporte_tipo_proactivo"];
                                        } elseif ($row_asociados["soporte_tipo_evolutivo"] != "") {
                                            $TipoSoporte = $row_asociados["soporte_tipo_evolutivo"];
                                        }
                                        echo $TipoSoporte;
                                echo '</td>';
                                echo '<td class="text-center"><a href="info_soporte.php?soporte_id=' . $row_asociados["soporte_id"] . '" class="main-link"><i class="fa fa-info fa-lg"></i> Ver Más</a></td>'
                                . '</tr>';
                            endforeach;
                            ?>                            
                        </tbody>
                    </table>
                    <div class="padding-sm bg-grey">
                        <h5><a href="#formModal_UpdateNotaProveedor" role="button" data-toggle="modal" class="btn-link"><i class="fa fa-edit"></i></a> - Nota </h5> 
                        <textarea class="form-control input-sm"  id="" name="" disabled="" rows="3" placeholder="Solo se Permiten 140 Caracteres" maxlength="141" rows="3"><?php echo $row_proveedor["provee_nota"]; ?></textarea>
                    </div>
                    <!--
                    <div class="" style="margin-bottom: 20px;">
                        </br>
                        <a class="btn btn-success hidden-print" id="invoicePrint"><i class="fa fa-print"></i> Imprimir Ficha</a>
                    </div>-->
                </div><!-- /.padding20 -->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
        <!-- ########################################################### -->
        <div class="modal fade" id="formModal_UpdateImagenProveedor" class="modal fade in" role="dialog" style="">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="form_update_ImagenProveedor" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <!--<div class="hidden"><input id="Update_img_id" name="Update_img_id" type="text"></div>-->
                                <div class="col-md-6 text-center" style="border-right: 1px solid #D2D2D2;"><img style="width: 150px; height: 150px;" id="img_user" class=" img-thumbnail img-responsive" style="" src="<?php echo $db->single("SELECT provee_img_url FROM Proveedores WHERE provee_id = '$provee_id'"); ?>" alt="User Avatar"></div>
                                <div class="col-md-6 text-center" id="div_archivos_img">
                                    <input class="btn btn-sm" type="file" id="archivos_img" name="archivos_img">
                                    </br>
                                    <div class="alert alert-warning">
                                        <strong>Atención!</strong> Imagen optima, 150px x 150px.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">                                        
                                <div class="col-md-8 text-left">
                                    <div id="msj_respuesta_update_ImagenProveedor"></div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button id="btn_cerrar_modal" type="button" class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="btn_update_foto_proveedor" class="btn btn-success btn-sm" type="button">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        <!-- ########################################################### -->
        <form id="form_update_NombreProveedor" method="POST">
            <div class="modal fade" id="formModal_UpdateNombreProveedor">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Nombre de la Empresa</label>
                                        <input type="text" value="<?php echo $row_proveedor["provee_nombre_empresa"]; ?>" placeholder="Razón social" id="proveedor_update_nombre_empresa" name="proveedor_update_nombre_empresa" class="form-control input-sm">
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">                                        
                                <div class="col-md-8 text-left">
                                    <div id="msj_respuesta_update_NombreProveedor"></div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                            
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_AliasProveedor" method="POST">
            <div class="modal fade" id="formModal_UpdateAliasProveedor">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Nombre de Fantasía o Alias</label>
                                        <input type="text" value="<?php echo $row_proveedor["provee_nombre_fantasia"]; ?>" placeholder="Por el cuál es conocida la Empresa" id="proveedor_alias_fantasia_update" name="proveedor_alias_fantasia_update" class="form-control input-sm">
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">                                        
                                <div class="col-md-8 text-left">
                                    <div id="msj_respuesta_update_AliasProveedor"></div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_SitioWebProveedor" method="POST">
            <div class="modal fade" id="formModal_UpdateSitioWeb">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">URL Sitio Web</label>
                                        <input type="text" placeholder="ej: http://www.entel.cl/" value="<?php echo $row_proveedor["provee_sitio_web"]; ?>" id="proveedor_url_sitio_web_update" name="proveedor_url_sitio_web_update" class="form-control input-sm">
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">                                        
                                <div class="col-md-8 text-left">
                                    <div id="msj_respuesta_update_SitioWebProveedor"></div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_NumeroMesaAyuda" method="POST">
            <div class="modal fade" id="formModal_UpdateNumeroMesAyuda">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">N° Mesa de Ayuda</label>                                            
                                        <input type="text" value="<?php echo $row_proveedor["provee_num_mesa_ayuda"]; ?>" placeholder="Teléfono Principal, ej: +56973993813" id="proveedor_n_mesa_ayuda_update" name="proveedor_n_mesa_ayuda_update" class="form-control input-sm">
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">                                        
                                <div class="col-md-8 text-left">
                                    <div id="msj_respuesta_update_NumMesaAyudaProveedor"></div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_EmailProveedore" method="POST">
            <div class="modal fade" id="formModal_UpdateEmailProveedores">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Email Contacto</label>                                            
                                        <input type="text" value="<?php echo $row_proveedor["provee_email"]; ?>" placeholder="" id="proveedor_email_update" name="proveedor_email_update" class="form-control input-sm" data-required="true" data-minlength="8">
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">                                        
                                <div class="col-md-8 text-left">
                                    <div id="msj_respuesta_update_EmailProveedor"></div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_DireccionProveedor" method="POST">
            <div class="modal fade" id="formModal_UpdateDireccionProveedor">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Dirección</label>
                                        <input type="text" value="<?php echo $row_proveedor["provee_direccion"]; ?>" placeholder="Dirección física Oficina Central" id="proveedor_direccion_update" name="proveedor_direccion_update" class="form-control input-sm">
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">                                        
                                <div class="col-md-8 text-left">
                                    <div id="msj_respuesta_update_DireccionProveedor"></div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <!-- ########################################################### -->
        <!-- ########################################################### -->
        <!-- ########################################################### -->
        <!-- ########################################################### -->
        <form id="form_update_ContactoAdministrativo" method="POST">
            <div class="modal fade" id="formModal_UpdateContactoAdministrativo">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Nombre</label>
                                                    <input type="text" placeholder="" value="<?php echo $row_adminis['administra_nombre']; ?>" class="form-control input-sm" id="proveedor_nombre_administrativo_update" name="proveedor_nombre_administrativo_update">
                                                </div><!-- /form-group -->
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Cargo</label>
                                                    <input type="text" placeholder="" value="<?php echo $row_adminis['administra_cargo']; ?>" class="form-control input-sm" id="proveedor_cargo_administrativo_update" name="proveedor_cargo_administrativo_update">
                                                </div><!-- /form-group -->
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Email</label>
                                                    <input type="text" placeholder="" value="<?php echo $row_adminis['administra_email']; ?>" class="form-control input-sm" id="proveedor_email_administrativo_update" name="proveedor_email_administrativo_update">
                                                </div><!-- /form-group -->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Teléfono Móvil</label>
                                                    <input type="text" placeholder="" value="<?php echo $row_adminis['administra_tel_movil']; ?>" class="form-control input-sm" id="proveedor_tel_movil_administrativo_update" name="proveedor_tel_movil_administrativo_update">
                                                </div><!-- /form-group -->
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Teléfono Fijo</label>
                                                    <input type="text" placeholder="" value="<?php echo $row_adminis['administra_tel_fijo']; ?>" class="form-control input-sm" id="proveedor_tel_fijo_administrativo_update" name="proveedor_tel_fijo_administrativo_update">
                                                </div><!-- /form-group -->
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">                                        
                                <div class="col-md-8 text-left">
                                    <div id="msj_respuesta_update_ContactoAdministrativoProveedor"></div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_ContactoTecnico" method="POST">
            <div class="modal fade" id="formModal_UpdateContactoTecnico">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Nombre</label>
                                                    <input type="text" placeholder="" value="<?php echo $row_tecnico['tecnico_nombre']; ?>"class="form-control input-sm" id="proveedor_nombre_tecnico_update" name="proveedor_nombre_tecnico_update" >
                                                </div><!-- /form-group -->
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Cargo</label>
                                                    <input type="text" placeholder="" value="<?php echo $row_tecnico['tecnico_cargo']; ?>"class="form-control input-sm" id="proveedor_cargo_tecnico_update" name="proveedor_cargo_tecnico_update">
                                                </div><!-- /form-group -->
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Email</label>
                                                    <input type="text" placeholder="" value="<?php echo $row_tecnico['tecnico_email']; ?>"class="form-control input-sm" id="proveedor_email_tecnico_update" name="proveedor_email_tecnico_update">
                                                </div><!-- /form-group -->
                                            </div>
                                        </div>
                                        <div class="row">                                    
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Teléfono Móvil</label>
                                                    <input type="text" placeholder="" value="<?php echo $row_tecnico['tecnico_tel_movil']; ?>"class="form-control input-sm" id="proveedor_tel_movil_tecnico_update" name="proveedor_tel_movil_tecnico_update">
                                                </div><!-- /form-group -->
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Teléfono Fijo</label>
                                                    <input type="text" placeholder="" value="<?php echo $row_tecnico['tecnico_tel_fijo']; ?>"class="form-control input-sm" id="proveedor_tel_fijo_tecnico_update" name="proveedor_tel_fijo_tecnico_update">
                                                </div><!-- /form-group -->
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <div class="row">                                        
                                <div class="col-md-8 text-left">
                                    <div id="msj_respuesta_update_ContactoTecnicoProveedor"></div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <form id="form_update_NotaProveedor" method="POST">
            <div class="modal fade" id="formModal_UpdateNotaProveedor">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Nota</label>
                                        <textarea class="form-control input-sm"  id="proveedor_nota_update" name="proveedor_nota_update" rows="3" placeholder="Solo se Permiten 140 Caracteres" maxlength="141" rows="3"><?php echo $row_proveedor["provee_nota"]; ?></textarea>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                            <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- Logout confirmation -->
<?php require_once './includes/confirmation.logout.inc.php'; ?> 
        <!-- ######################################## -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.10.2.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#btn_cerrar_modal').click(function() {
                    location.reload();
                });
            });

            $(function () {
                $('#btn_update_foto_proveedor').click(SubirFotos); //Capturamos el evento click sobre el boton con el id=enviar	y ejecutamos la función seleccionado.
                //var a = $("#archivos_img").attr("src");

            });

            function SubirFotos() {
                var archivos_img = document.getElementById("archivos_img");//Creamos un objeto con el elemento que contiene los archivos_img: el campo input file, que tiene el id = 'archivos_img'
                var archivo = archivos_img.files; //Obtenemos los archivos_img seleccionados en el imput
                //Creamos una instancia del Objeto FormDara.
                var archivos_img = new FormData();
                /* Como son multiples archivos_img creamos un ciclo for que recorra la el arreglo de los archivos_img seleccionados en el input
                 Este y añadimos cada elemento al formulario FormData en forma de arreglo, utilizando la variable i (autoincremental) como 
                 indice para cada archivo, si no hacemos esto, los valores del arreglo se sobre escriben*/
                for (i = 0; i < archivo.length; i++) {
                    archivos_img.append('archivo_img' + i, archivo[i]); //Añadimos cada archivo a el arreglo con un indice direfente
                }
                $.ajax({
                    url: 'modelo/img_proveedor.php', //Url a donde la enviaremos
                    type: 'POST', //Metodo que usaremos
                    contentType: false, //Debe estar en false para que pase el objeto sin procesar
                    data: archivos_img, //Le pasamos el objeto que creamos con los archivos_img
                    processData: false, //Debe estar en false para que JQuery no procese los datos a enviar
                    cache: false //Para que el formulario no guarde cache
                }).done(function (msg) { //Escuchamos la respuesta y capturamos el mensaje msg
                    //$("#msj_respuesta_update_img_usuario_inventario").html(msg);
                    $('#img_user').attr("src", 'img/ajax-loader.gif');
                    $("#img_user").attr("src", msg);
                });
            }
        </script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>
        <script>
            $(document).ready(function () {
                $("textarea").keydown(function (e) {
                        if (e.keyCode === 9) { // tab was pressed
                            // get caret position/selection
                            var start = this.selectionStart;
                            end = this.selectionEnd;

                            var $this = $(this);

                            // set textarea value to: text before caret + tab + text after caret
                            $this.val($this.val().substring(0, start) + "\t" + $this.val().substring(end));

                            // put caret at right position again
                            this.selectionStart = this.selectionEnd = start + 1;

                            // prevent the focus lose
                            return false;
                        }
                    });
            });
        </script>
        <script src="js/menu-active-class.js"></script>
        <!-- Colorbox -->
        <script src='js/jquery.colorbox.min.js'></script>	

        <!-- Sparkline -->
        <script src='js/jquery.sparkline.min.js'></script>

        <!-- Pace -->
        <script src='js/uncompressed/pace.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <!--<script src="js/endless/endless_dashboard.js"></script>-->
        <script src="js/endless/endless.js"></script>
        <script>
            $(function() {
                $('#invoicePrint').click(function() {
                    window.print();
                });
            });
        </script>
    </body>
</html>
