<?php
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
session_start();
$NombrePersona = $_SESSION["datos_usuario_logueado"][0];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][1];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][2];
$TelefonoMovil = $_SESSION["datos_usuario_logueado"][3];
$EmailUsuario = $_SESSION["datos_usuario_logueado"][4];
$Username = $_SESSION["datos_usuario_logueado"][5];
$Contraseña = $_SESSION["datos_usuario_logueado"][6];
$PemisosUsuario = $_SESSION["datos_usuario_logueado"][7];
if (!isset($NombrePersona)) {
    header('Location: login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="es">    
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.php"> Home</a></li>
                        <li class="active">Tablero de Instrumentos <?php ?></li>	 
                    </ul>
                </div><!-- /breadcrumb-->
                <div class="main-header clearfix">
                    <div class="page-title">
                        <h3 class="no-margin">Tablero de Instrumentos</h3>
                        <span>Bienvenido <strong><?php echo $NombrePersona . " " . $ApellidoPaterno;?></strong></span>
                        <span>Su IP: <?php echo $_SERVER['REMOTE_ADDR'];?></span>
                    </div><!-- /page-title -->
                </div><!-- /main-header -->
                <!--<div class="grey-container shortcut-wrapper">
                    <a href="#" class="shortcut-link">
                        <span class="shortcut-icon">
                            <i class="fa fa-bar-chart-o"></i>
                        </span>
                        <span class="text">Statistic</span>
                    </a>
                    <a href="#" class="shortcut-link">
                        <span class="shortcut-icon">
                            <i class="fa fa-envelope-o"></i>
                            <span class="shortcut-alert">
                                5
                            </span>	
                        </span>
                        <span class="text">Messages</span>
                    </a>
                    <?php if($PemisosRol == "ADMINISTRADOR"){?>
                    <a href="register.php" class="shortcut-link"><span class="shortcut-icon"><i class="fa fa-user"></i></span><span class="text">Nuevo Usuario</span></a>
                    <a href="usuarios_inventario.php" class="shortcut-link"><span class="shortcut-icon"><i class="fa fa-users"></i></span><span class="text">Usuarios del Sistema</span></a>
                    <?php } ?>
                    <a href="#" class="shortcut-link">
                        <span class="shortcut-icon">
                            <i class="fa fa-globe"></i>
                            <span class="shortcut-alert">
                                7
                            </span>	
                        </span>
                        <span class="text">Notification</span>
                    </a>
                    <a href="#" class="shortcut-link">
                        <span class="shortcut-icon">
                            <i class="fa fa-list"></i>
                        </span>
                        <span class="text">Activity</span>
                    </a>
                    <a href="#" class="shortcut-link">
                        <span class="shortcut-icon">
                            <i class="fa fa-cog"></i></span>
                        <span class="text">Setting</span>
                    </a>
                </div><!-- /grey-container -->
                <div class="padding-md">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="panel-stat3 bg-primary" style="cursor: default;">
                                <h2 class="m-top-none" id="userCount"><?php echo $db->single("SELECT COUNT(*) FROM Proveedores"); ?></h2>                                
                                <h5><i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">Proveedores Registrados</span></h5>
                                <div class="stat-icon"><i class="fa fa-building-o fa-2x" style="margin-top: 15px;margin-right: 20px;"></i></div>
                                <div class="loading-overlay"><i class="loading-icon fa fa-refresh fa-spin fa-lg"></i></div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-md-4">
                            <div class="panel-stat3 bg-primary" style="cursor: default;">
                                <h2 class="m-top-none"><?php echo $db->single("SELECT COUNT(*) FROM Servicios"); ?></h2>
                                <h5><i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">Servicios Registrados</span></h5>
                                <div class="stat-icon">
                                    <i class="fa fa-hdd-o fa-2x" style="margin-top: 15px;margin-right: 20px;"></i>
                                </div>
                                <div class="loading-overlay">
                                    <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-md-4">
                            <div class="panel-stat3 bg-primary" style="cursor: default;">
                                <h2 class="m-top-none" id="orderCount"><?php echo $db->single("SELECT COUNT(*) FROM EquiposComponentes WHERE equipo_compon_nombre_tipo = 'Servidor'"); ?></h2>
                                <h5><i class="fa fa-arrow-circle-o-up fa-lg"></i><span class="m-left-xs">Servidores Registrados</span></h5>
                                <div class="stat-icon">
                                    <i class="fa fa-server fa-2x" style="margin-top: 15px;margin-right: 20px;"></i>
                                </div>
                                <div class="loading-overlay">
                                    <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="panel panel-default table-responsive">
                                <div class="panel-heading">
                                    Total de Componentes
                                    <span class="badge badge-info pull-right"><?php echo $count_componentes = $db->single("SELECT COUNT(*) FROM Componentes"); ?> Registro(s)</span>
                                </div>
                                <div class="padding-md clearfix">
                                    <table class="table table-striped" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th>Servicio(s) Asociados</th>
                                                <th>Tipo Componente</th>
                                                <th>Nombre Componente</th>                                            
                                                <th>Fecha Termino</th>
                                                <th>% Restante</th>
                                                <th class="text-center">Días</br>Restantes</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_selec_FechasTerminoGarantias = $db->query("SELECT componente_id,equipo_compon_nombre_tipo, componente_id,componente_nombre, date_format(componente_fecha_termino,'%d-%m-%Y') as componente_fecha_termino FROM "
                                                    . "Componentes C, "
                                                    . "EquiposComponentes EC WHERE "
                                                    . "C.EquiposComponentes_equipos_compon_id  = EC.equipos_compon_id");
                                            $fecha_mas_alta = $db->single("SELECT MAX(componente_fecha_termino) FROM Componentes");

                                            $segundos2 = strtotime($fecha_mas_alta) - strtotime('now');
                                            $diferencia_dias2 = intval($segundos2 / 60 / 60 / 24);
                                            $fechas_donut_rojo = array();
                                            $fechas_donut_amarillo = array();
                                            $fechas_donut_verde = array();
                                            foreach ($sql_selec_FechasTerminoGarantias as $row_FinFechaTermino):
                                                $fecha = $row_FinFechaTermino["componente_fecha_termino"];
                                                $componente_id = $row_FinFechaTermino["componente_id"];
                                                $segundos = strtotime($fecha) - strtotime('now');
                                                $diferencia_dias = intval($segundos / 60 / 60 / 24);

                                                echo '<tr>';
                                                echo '<td>';
                                                $servicios_asociados = $db->query("SELECT DISTINCT serv_nombre FROM Servicios_has_Componentes ShC, Servicios S, Componentes C WHERE "
                                                        . "ShC.Componentes_componente_id = '$componente_id' AND "
                                                        . "ShC.Servicios_serv_id = S.serv_id");
                                                if ($servicios_asociados) {
                                                    foreach ($servicios_asociados as $row_serv_asoci):
                                                        echo $row_serv_asoci["serv_nombre"] . "</br>";
                                                    endforeach;
                                                }else {
                                                    echo "Sin Servicio Asociado";
                                                }

                                                echo '</td>';
                                                echo '<td>' . $row_FinFechaTermino['equipo_compon_nombre_tipo'] . '</td>';
                                                echo '<td>' . $row_FinFechaTermino['componente_nombre'] . '</td>';
                                                echo '<td>' . $row_FinFechaTermino['componente_fecha_termino'] . '</td>';
                                                echo '<td>'
                                                . '<div class="progress progress-striped active" style="height:8px; margin:5px 0 0 0;">';
                                                $porcentaje2 = round(($diferencia_dias * 100) / 365);
                                                //$porcentaje2 = 100 - $porcentaje1;
                                                if ($porcentaje2 >= 0 and $porcentaje2 <= 25) {
                                                    echo '<div class="progress-bar progress-bar-danger" style="width:';
                                                    //echo '<div class="progress-bar progress-bar-success" style="width: ';
                                                    echo $porcentaje2;
                                                    array_push($fechas_donut_rojo, $porcentaje2);
                                                    echo '%"><span class="sr-only">' . $porcentaje2 . '% Completado</span></div>';
                                                }
                                                if ($porcentaje2 >= 26 and $porcentaje2 <= 45) {
                                                    echo '<div class="progress-bar progress-bar-danger" style="width:';
                                                    //echo '<div class="progress-bar progress-bar-success" style="width: ';
                                                    echo $porcentaje2;
                                                    array_push($fechas_donut_rojo, $porcentaje2);
                                                    echo '%"><span class="sr-only">' . $porcentaje2 . '% Completado</span></div>';
                                                }
                                                if ($porcentaje2 >= 46 and $porcentaje2 <= 65) {
                                                    echo '<div class="progress-bar progress-bar-warning" style="width:';
                                                    //echo '<div class="progress-bar progress-bar-success" style="width: ';
                                                    echo $porcentaje2;
                                                    array_push($fechas_donut_amarillo, $porcentaje2);
                                                    echo '%"><span class="sr-only">' . $porcentaje2 . '% Completado</span></div>';
                                                }
                                                if ($porcentaje2 >= 66 and $porcentaje2 <= 85) {
                                                    echo '<div class="progress-bar progress-bar-warning" style="width:';
                                                    echo $porcentaje2;
                                                    array_push($fechas_donut_amarillo, $porcentaje2);
                                                    echo '%"><span class="sr-only">' . $porcentaje2 . '% Completado</span></div>';
                                                }
                                                if ($porcentaje2 >= 86 and $porcentaje2 <= 95) {
                                                    //echo '<div class="progress-bar progress-bar-danger" style="width:';
                                                    echo '<div class="progress-bar progress-bar-success" style="width: ';
                                                    echo $porcentaje2;
                                                    array_push($fechas_donut_verde, $porcentaje2);
                                                    echo '%"><span class="sr-only">' . $porcentaje2 . '% Completado</span></div>';
                                                }
                                                if ($porcentaje2 >= 96 and $porcentaje2 <= 100) {
                                                    //echo '<div class="progress-bar progress-bar-danger" style="width:';
                                                    echo '<div class="progress-bar progress-bar-success" style="width: ';

                                                    echo $porcentaje2;
                                                    array_push($fechas_donut_verde, $porcentaje2);
                                                    echo '%"><span class="sr-only">' . $porcentaje2 . '% Completado</span></div>';
                                                }
                                                if ($porcentaje2 >= 100) {
                                                    //echo '<div class="progress-bar progress-bar-danger" style="width:';
                                                    echo '<div class="progress-bar progress-bar-success" style="width: ';

                                                    echo '100';
                                                    echo '%"><span class="sr-only">100% Completado</span></div>';
                                                }
                                                echo '</div>' .
                                                '</td>';
                                                //echo '<td>' . $porcentaje2 . '%</td>';
                                                echo '<td class="text-center"><span class="badge badge-info">' . $diferencia_dias . '</span></td>';
                                                echo '</tr>';
                                            endforeach;
                                            ?>
                                        </tbody>
                                    </table>                            
                                </div><!-- /panel -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="panel-heading">
                                        Global de Fechas por Cudacar
                                        <?php
                                        $logitud_rojo = count($fechas_donut_rojo);
                                        $logitud_amarillo = count($fechas_donut_amarillo);
                                        $logitud_verde = count($fechas_donut_verde);
                                        ?>
                                    </div>

                                    <div id="donutChart_fechas" style="height: 250px;"></div>
                                    <!--<div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                        IN-STORE Sales
                                                        <span class="badge badge-success pull-right">75%</span>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                        DOWMLOAD Sales
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                        MAIL-ORDER Sales 
                                                        <span class="badge badge-danger pull-right"><i class="fa fa-arrow-down"></i> 3%</span>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. 
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->
                                </div>
                            </div><!-- /panel -->
                        </div>
                    </div>
                    <div class="row">
                        <?php /*
                          $sql_selec_FechasTerminoGarantias = $db->query("SELECT componente_id,componente_nombre, componente_fecha_termino FROM Componentes");
                          foreach ($sql_selec_FechasTerminoGarantias as $row_FinFechaTermino):
                          $fecha = $row_FinFechaTermino["componente_fecha_termino"];
                          $segundos = strtotime($fecha) - strtotime('now');
                          $diferencia_dias = intval($segundos / 60 / 60 / 24);
                          //echo $row_FinFechaTermino["componente_nombre"]." ".$diferencia_dias."\n";
                          echo '<div class="col-sm-6 col-md-3">';
                          if ($diferencia_dias >= 0 and $diferencia_dias <= 180) {
                          echo '<div class="panel-stat3 bg-danger">';
                          }
                          if ($diferencia_dias >= 181 and $diferencia_dias <= 270) {
                          echo '<div class="panel-stat3 bg-warning">';
                          }
                          if ($diferencia_dias >= 271) {
                          echo '<div class="panel-stat3 bg-success">';
                          }

                          echo '<h2 class="m-top-none" id="">';
                          echo $diferencia_dias . " Días";
                          echo '</h2>';
                          echo '<h5>Para Caducidad </br>de Garantia</h5>';
                          echo '<span class="m-left-xs">' . $row_FinFechaTermino["componente_nombre"] . ' </br><a href="info_infraestructura.php?id_componente=' . $row_FinFechaTermino["componente_id"] . '" class="main-link"><i class="fa fa-info fa-lg"></i> Ver Más</a></span>'
                          . '<div class="stat-icon"><i class="fa fa-calendar fa-3x"></i></div>'
                          . '<div class="refresh-button"><i class="fa fa-refresh"></i></div>'
                          . '<div class="loading-overlay"><i class="loading-icon fa fa-refresh fa-spin fa-lg"></i></div>';
                          echo '</div>';
                          echo '</div>';
                          endforeach; */
                        ?>
                    </div>
                </div>
            </div><!-- /main-container -->
        <!--############################################ -->
        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/confirmation.logout.inc.php'; ?> 
        <!-- ######################################## -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.js"></script>

        <!-- Datatable -->
        <script src='js/jquery.dataTables.min.js'></script>	
        <script type="text/javascript">
            $(document).ready(function () {
                $('#dataTable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "asStripClasses": null
                });
                $('#chk-all').click(function () {
                    if ($(this).is(':checked')) {
                        $('#responsiveTable').find('.chk-row').each(function () {
                            $(this).prop('checked', true);
                            $(this).parent().parent().parent().addClass('selected');
                        });
                    }
                    else {
                        $('#responsiveTable').find('.chk-row').each(function () {
                            $(this).prop('checked', false);
                            $(this).parent().parent().parent().removeClass('selected');
                        });
                    }
                });
                Morris.Donut({
                    element: 'donutChart_fechas',
                    data: [
                        {label: "Proximas a Caducar", value: <?php echo $logitud_amarillo; ?>},
                        {label: "Tiempo Adecuado", value: <?php echo $logitud_verde; ?>},
                        {label: "Alerta \n Caducidad Cercana", value: <?php echo $logitud_rojo; ?>}
                    ],
                    colors: ['#f3ce85', '#65CEA7', '#FC8675']
                });
            });
        </script>
        <!-- Flot -->
        <script src='js/jquery.flot.min.js'></script>

        <!-- Morris -->
        <script src='js/rapheal.min.js'></script>	
        <script src='js/morris.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {
            });
        </script>
        <script src="js/menu-active-class.js"></script>
        <!-- Colorbox -->
        <script src='js/jquery.colorbox.min.js'></script>	

        <!-- Sparkline -->
        <script src='js/jquery.sparkline.min.js'></script>

        <!-- Pace -->
        <script src='js/uncompressed/pace.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <!--<script src="js/endless/endless_dashboard.js"></script>-->
        <script src="js/endless/endless.js"></script>

    </body>
</html>
