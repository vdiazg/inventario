<?php
/*
  -----------------------------------------------------------------------
  DATOS DE CONTACTO
  -----------------------------------------------------------------------
  Desarrollador: Victor Manuel Diaz Galdames.
  Nivel Profesional: Ingeniero Informático
  Número Contacto (Móvil): +5699491896
  Correo Eletrónico: victordiazgaldames@gmail.com
  Curriculum Vitae: http://victordiaz.nfconnection.cl/
  Nacionalidad: Chileno
  Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
  -----------------------------------------------------------------------
  DATOS DE JEFE A CARGO
  -----------------------------------------------------------------------
  Nombre: Nelson Horacio Guzman Mora.
  Número Contacto (Móvil): +56998289358
  Correo Eletrónico: nguzman@entel.cl
  Cargo: Jefe Operación Plataformas
 */
?>
<?php
require_once './controlador/Db.class.php';
$db = new Db();
//LLENADO DEL SELECT
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->        
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div class="padding-md">
                    <div class="panel panel-default table-responsive">
                        <div class="panel-heading">
                            Usuarios del Sistema
                            <span class="label label-info pull-right"><?php echo $count_soportes = $db->single("SELECT COUNT(*) FROM Usuarios"); ?> Registro(s)</span>
                        </div>
                        <div class="padding-md clearfix">
                            <table class="table table-striped" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>Avatar</th>
                                        <th>Nombre</th>
                                        <th>Apellido Paterno</th>
                                        <th>Apellido Materno</th>
                                        <th>Teléfono Móvil</th>
                                        <th>Correo Electrónico</th>
                                        <th>Username</th>
                                        <th>Permiso</th>
                                        <th>Status</th>
                                        <th>Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $datos_sql_select_proveedor = $db->query("SELECT * FROM Usuarios");
                                    foreach ($datos_sql_select_proveedor as $row_proveedor):
                                        $IDUsuario = $row_proveedor['usuario_id'];
                                        $NombreUsuario = $row_proveedor['usuario_nombre'];
                                        $ApellidoPaterno = $row_proveedor['usuario_apell_paterno'];
                                        $ApellidoMaterno = $row_proveedor['usuario_apell_materno'];
                                        $TelefonoMovil = $row_proveedor['usuario_tel_movil'];
                                        $CorreoElectronico = $row_proveedor['usuario_email'];
                                        $Username = $row_proveedor['usuario_username'];
                                        $PermisoRol = $db->single("SELECT permiso_rol FROM Permisos WHERE permiso_id = '" . $row_proveedor["Permisos_permiso_id"] . "'");
                                        $StatusUsuario = $row_proveedor['usuario_status'];
                                        $UsuarioImgUrl = $row_proveedor['usuario_img_url'];

                                        echo '<tr>'
                                        . '<td><div class="bounceIn"><img class="img-circle img-responsive" style="width: 50px; height: 50px;" src="./' . $UsuarioImgUrl . '" alt="User Avatar"></div></td>'
                                        . '<td>' . $NombreUsuario . '</td>'
                                        . '<td>' . $ApellidoPaterno . '</td>'
                                        . '<td>' . $ApellidoMaterno . '</td>'
                                        . '<td>' . $TelefonoMovil . '</td>'
                                        . '<td>' . $CorreoElectronico . '</td>'
                                        . '<td>' . $Username . '</td>'
                                        . '<td>' . $PermisoRol . '</td>';
                                        if ($StatusUsuario == "Activo") {
                                            echo '<td class=""><i class="fa fa-check-circle-o fa-lg" style="color: green;"></i> Activo</td>';
                                        } else {
                                            echo '<td class=""><i class="fa fa-times-circle-o fa-lg" style="color: red;"></i> Inactivo</td>';
                                        }
                                        echo "<td class='text-center'><a class='main-link' onclick=\"cargarModalModificarUsuarioInventario('$IDUsuario','$NombreUsuario','$ApellidoPaterno','$ApellidoMaterno','$TelefonoMovil','$CorreoElectronico','$Username','$PermisoRol','$StatusUsuario','$UsuarioImgUrl');\"><i class='fa fa-edit fa-lg' ></i></a></td>"
                                        . '</tr>';
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                        </div><!-- /.padding-md -->
                    </div><!-- /panel -->
                </div><!-- /.padding-md -->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/confirmation.logout.inc.php'; ?> 
        <!-- ########################################################### -->
        <div id="Update_usuarios_sistema_inventario" class="modal fade in" role="dialog" style="">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="form_Update_usuarios_sistema_inventario" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4 hidden"><div class=""><input id="Update_id" name="Update_id" type="text"></div></div>
                                <div class="col-md-4"><div class="form-group"><label class="control-label" for="user_nombre">Nombre o Nombres</label><input id="Update_user_nombre" name="Update_user_nombre" type="text" placeholder="Nombre o Nombres" class="form-control input-sm bounceIn " ></div></div>
                                <div class="col-md-4"><div class="form-group"><label class="control-label">Apellido Paterno</label><input id="Update_user_apell_paterno" name="Update_user_apell_paterno" type="text" placeholder="Apellido Paterno" class="form-control input-sm bounceIn " ></div></div>
                                <div class="col-md-4"><div class="form-group"><label class="control-label">Apellido Materno</label><input id="Update_user_apell_materno" name="Update_user_apell_materno" type="text" placeholder="Apellido Materno" class="form-control input-sm bounceIn " ></div></div>
                            </div>
                            <div class="row">
                                <div class="col-md-4"><div class="form-group"><label class="control-label">Teléfono Móvil</label><input id="Update_user_tel_movil" name="Update_user_tel_movil" type="text" placeholder="Teléfono Móvil" class="form-control input-sm bounceIn " ></div></div>
                                <div class="col-md-4"><div class="form-group"><label class="control-label">Correo Electrónico</label><input id="Update_user_email" name="Update_user_email" type="email" placeholder="" class="form-control input-sm bounceIn " ></div></div>
                                <div class="col-md-4"><div class="form-group"><label class="control-label">Nombre de Usuario</label><input id="Update_username" name="Update_username" type="text" placeholder="" class="form-control input-sm bounceIn " ></div></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"><div class="form-group"><label class="control-label">Contraseña</label><input  id="Update_user_contrasena" name="Update_user_contrasena" type="password" placeholder="" class="form-control input-sm bounceIn " ></div></div>
                                <div class="col-md-6"><div class="form-group"><label class="control-label">Confirmar Contraseña</label><input id="Update_user_confirm_contrasena" name="Update_user_confirm_contrasena" type="password" placeholder="" class="form-control input-sm bounceIn " ></div></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Permiso del Usuario </label>
                                        <select class="form-control input-sm" id="select_Permiso_usuario" name="select_Permiso_usuario">
                                            <option value="">Seleccione Permiso</option>
                                            <?php
                                            $datos_sql_select_permisos = $db->query("SELECT permiso_id,permiso_rol FROM Permisos");
                                            foreach ($datos_sql_select_permisos as $row):
                                                echo "<option value=" . $row["permiso_id"] . ">" . $row["permiso_rol"] . "</option>";
                                            endforeach
                                            ?>
                                        </select>
                                    </div>                                
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Status del Usuario </label>
                                        <select class="form-control input-sm" id="select_status_usuario" name="select_status_usuario">
                                            <option value="">Seleccione Status</option>
                                            <option value="Activo">Activo</option>
                                            <option value="Inactivo">Inactivo</option>
                                        </select>
                                    </div>                                
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left" id="msj_respuesta_update_usuario_inventario">
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- ########################################################### -->
        <div id="Update_img_usuario_inventario" class="modal fade in" role="dialog" style="">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="form_Update_img_usuario_inventario" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="hidden"><input id="Update_img_id" name="Update_img_id" type="text"></div>
                                <div class="col-md-6"><img id="img_user" class="img-circle img-responsive" style="" src="./img/user.jpg" alt="User Avatar"></div>
                                <div class="col-md-6"><input type="file"  multiple="multiple" id="archivos_img"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left" id="msj_respuesta_update_img_usuario_inventario">
                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm" type="button">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- ########################################################### -->
        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>
        <script src="js/menu-active-class.js"></script>
        <script src='js/jquery.dataTables.min.js'></script>	
        <script type="text/javascript">
            $(document).ready(function () {
                $('#dataTable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "asStripClasses": null
                });
                $('#chk-all').click(function () {
                    if ($(this).is(':checked')) {
                        $('#responsiveTable').find('.chk-row').each(function () {
                            $(this).prop('checked', true);
                            $(this).parent().parent().parent().addClass('selected');
                        });
                    }
                    else {
                        $('#responsiveTable').find('.chk-row').each(function () {
                            $(this).prop('checked', false);
                            $(this).parent().parent().parent().removeClass('selected');
                        });
                    }
                });

            });
            function cargarModalModificarUsuarioInventario(IDUsuario, NombreUsuario, ApellidoPaterno, ApellidoMaterno, TelefonoMovil, CorreoElectronico, Username, PermisoRol, StatusUsuario, UsuarioImgUrl) {
                $('#Update_id').val(IDUsuario);
                $('#Update_user_nombre').val(NombreUsuario);
                $('#Update_user_apell_paterno').val(ApellidoPaterno);
                $('#Update_user_apell_materno').val(ApellidoMaterno);
                $('#Update_user_tel_movil').val(TelefonoMovil);
                $('#Update_user_email').val(CorreoElectronico);
                $('#Update_username').val(Username);
                //$("#img_user").attr("src","./"+UsuarioImgUrl);
                $('#Update_usuarios_sistema_inventario').modal('show');
            }
            
            
        </script>
        <!-- Colorbox -->
        <script src='js/jquery.colorbox.min.js'></script>	

        <!-- Sparkline -->
        <script src='js/jquery.sparkline.min.js'></script>

        <!-- Pace -->
        <script src='js/uncompressed/pace.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <!--<script src="js/endless/endless_dashboard.js"></script>-->
        <script src="js/endless/endless.js"></script>
    </body>
</html>
