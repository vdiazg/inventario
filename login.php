<?php 
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body>
        <div class="login-wrapper" style="top: 0px;">
            <div class="text-center">
                <h2 class="fadeInUp animation-delay8" style="font-weight:bold">
                    <div class="text-center">
                        <img src="img/logos_proveedores/nf_logo.png" style="width: 10%; margin-bottom: 15px;">
                    </div>
                    <span style="color:#0154a0; text-shadow:0 1px #ffffff">Inventario Red Móvil</span>
                </h2>
            </div>
            <div class="login-widget animation-delay1">	
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <div class="pull-left">
                            <i class="fa fa-lock fa-lg"></i> Inicio Sesión
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-login" name="form_login_usuario" id="form_login_usuario" method="post" action="">
                            <div class="form-group">
                                <label class="control-label">Nombre de Usuario</label>
                                <input type="text" name="username" id="username" placeholder="Nombre de Usuario" class="form-control input-sm bounceIn" >
                            </div>
                            <div class="form-group">
                                <label class="control-label">Contraseña</label>
                                <input type="password" name="password" id="password" placeholder="Contraseña" class="form-control input-sm bounceIn ">
                            </div>
                            <hr/>
                            <div class="row">                                        
                                <div class="col-md-8 text-left">
                                    <div id="msj_respuesta_login_usuario"></div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button type="submit" class="btn btn-info btn-sm bounceIn animation-delay5 login-link pull-right">Entrar <i class="fa fa-arrow-right"></i></button></div>
                            </div>
                        </form>

                    </div>
                </div><!-- /panel -->
            </div><!-- /login-widget -->
        </div><!-- /login-wrapper -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->


        <!-- Endless -->
        <script src="js/endless/endless.js"></script>
    </body>
</html>
