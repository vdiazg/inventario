<?php

    /*
    $sql_insert_proveedores->bindParam(1, $proveedor_alias_fantasia);
    $sql_insert_proveedores->bindParam(2, $proveedor_nombre_empresa);
    $sql_insert_proveedores->bindParam(3, $proveedor_rut);
    $sql_insert_proveedores->bindParam(4, $proveedor_url_sitio_web);
    $sql_insert_proveedores->bindParam(5, $proveedor_direccion);
    $sql_insert_proveedores->bindParam(6, $proveedor_n_mesa_ayuda);
    $sql_insert_proveedores->bindParam(7, $proveedor_nota);
    $sql_insert_proveedores->bindParam(8, $proveedor_documento_oficial);
    $sql_insert_proveedores->bindParam(9, $proveedor_img);
    
    /* Ejecutamos la query de Proveedores
    $stmt_proveedores = $bd->ejecutar($sql_insert_proveedores);*/
    
    //########################################################################
    $sql_select_proveedores = $bd->prepare("SELECT provee_id FROM Proveedores WHERE provee_rut = ?");
    if($sql_select_proveedores->ejecutar(array($_POST['provee_id']))){
        while($fila = $sql_select_proveedores->fetch()){
            $Proveedores_provee_id = $fila;
        }
    }
    //########################################################################
    
    /* Query para Tabla Soporte */
    $sql_insert_soporte = $bd->prepare("INSERT INTO Soportes("
            . "soporte_fecha_inicio, "
            . "soporte_fecha_termino, "
            . "soporte_tipo_horario, "
            . "soporte_hora_inicio_atencion_lunes, "
            . "soporte_hora_inicio_atencion_martes, "
            . "soporte_hora_inicio_atencion_miercoles, "
            . "soporte_hora_inicio_atencion_jueves, "
            . "soporte_hora_inicio_atencion_viernes, "
            . "soporte_hora_inicio_atencion_sabado, "
            . "soporte_hora_inicio_atencion_domingo, "
            . "soporte_tareas_incluidas, "
            . "soporte_incluye_respuestos, "
            . "soporte_sistema_tickets, "
            . "soporte_flujo, "
            . "soporte_tiempo_respuesta, "
            . "soporte_tipo_asistencia, "
            . "soporte_tipo, "
            . "Proveedores_provee_id, "
            . "Servicios_serv_id, "
            . "soporte_hora_termino_atencion_lunes, "
            . "soporte_hora_termino_atencion_martes, "
            . "soporte_hora_termino_atencion_miercoles, "
            . "soporte_hora_termino_atencion_jueves, "
            . "soporte_hora_termino_atencion_viernes, "
            . "soporte_hora_termino_atencion_sabado, "
            . "soporte_hora_termino_atencion_domingo) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $sql_insert_soporte->bindParam(1, $proveedor_fecha_inicio_soporte);
    $sql_insert_soporte->bindParam(2, $proveedor_fecha_termino_soporte);
    $sql_insert_soporte->bindParam(3, $proveedor_tipo_horario_soporte);
    $sql_insert_soporte->bindParam(4, $proveedor_horario_inicio_atencion_lunes);
    $sql_insert_soporte->bindParam(5, $proveedor_horario_inicio_atencion_martes);
    $sql_insert_soporte->bindParam(6, $proveedor_horario_inicio_atencion_miercoles);
    $sql_insert_soporte->bindParam(7, $proveedor_horario_inicio_atencion_jueves);
    $sql_insert_soporte->bindParam(8, $proveedor_horario_inicio_atencion_viernes);
    $sql_insert_soporte->bindParam(9, $proveedor_horario_inicio_atencion_sabado);
    $sql_insert_soporte->bindParam(10,$proveedor_horario_inicio_atencion_domingo);
    $sql_insert_soporte->bindParam(11,$proveedor_tarea_incluida);
    $sql_insert_soporte->bindParam(12,$proveedor_incluye_respuestos);
    $sql_insert_soporte->bindParam(13,$proveedor_sistema_tickets);
    $sql_insert_soporte->bindParam(14,$proveedor_flujo_soporte);
    $sql_insert_soporte->bindParam(15,$proveedor_tiempo_respuesta);
    $sql_insert_soporte->bindParam(16,$proveedor_tipo_asistencia);
    $sql_insert_soporte->bindParam(17,$proveedor_tipo_soporte);
    $sql_insert_soporte->bindParam(18,$Proveedores_provee_id);
    $sql_insert_soporte->bindParam(19,"0");
    $sql_insert_soporte->bindParam(20,$proveedor_horario_termino_atencion_lunes);
    $sql_insert_soporte->bindParam(21,$proveedor_horario_termino_atencion_martes);
    $sql_insert_soporte->bindParam(22,$proveedor_horario_termino_atencion_miercoles);
    $sql_insert_soporte->bindParam(23,$proveedor_horario_termino_atencion_jueves);    
    $sql_insert_soporte->bindParam(24,$proveedor_horario_termino_atencion_viernes);
    $sql_insert_soporte->bindParam(25,$proveedor_horario_termino_atencion_sabado);
    $sql_insert_soporte->bindParam(26,$proveedor_horario_termino_atencion_domingo);
    
    /* Ejecutamos la query de Soporte */
    $stmt_soportes = $bd->ejecutar($sql_insert_soporte);
    //########################################################################
    
    /* Query para Tabla Administrativos */
    $sql_insert_administrativos = $bd->prepare("INSERT INTO Administrativos ("
            . "administra_nombre, "
            . "administra_cargo, "
            . "administra_email, "
            . "administra_tel_fijo, "
            . "administra_tel_movil, "
            . "Proveedores_provee_id) VALUES (?,?,?,?,?,?)");
    $sql_insert_administrativos->bindParam(1,$proveedor_nombre_administrativo);
    $sql_insert_administrativos->bindParam(2,$proveedor_cargo_administrativo);
    $sql_insert_administrativos->bindParam(3,$proveedor_email_administrativo);
    $sql_insert_administrativos->bindParam(4,$proveedor_tel_movil_administrativo);
    $sql_insert_administrativos->bindParam(5,$proveedor_tel_fijo_tecnico);
    $sql_insert_administrativos->bindParam(6,$Proveedores_provee_id);
    
    /* Ejecutamos la query Administrativos */
    $stmt_administrativos = $bd->ejecutar($sql_insert_administrativos);
    //########################################################################
    
    /* Query para Tabla Tecnicos */
    $sql_insert_tecnicos = $bd->prepare("INSERT INTO Tecnicos("
            . "tecnico_nombre, "
            . "tecnico_email, "
            . "tecnico_cargo, "
            . "tecnico_tel_movil, "
            . "tecnico_tel_fijo, "
            . "Proveedores_provee_id) VALUES (?,?,?,?,?,?)");
    $sql_insert_tecnicos->bindParam(1,$proveedor_nombre_tecnico);
    $sql_insert_tecnicos->bindParam(2,$proveedor_cargo_tecnico);
    $sql_insert_tecnicos->bindParam(3,$proveedor_email_tecnico);
    $sql_insert_tecnicos->bindParam(4,$proveedor_tel_movil_tecnico);
    $sql_insert_tecnicos->bindParam(5,$proveedor_tel_fijo_tecnico);
    $sql_insert_tecnicos->bindParam(6,$Proveedores_provee_id);
    
    /* Ejecutamos la query Tecnicos */
    $stmt_tecnicos = $bd->ejecutar($sql_insert_tecnicos);
/*
    echo "true";
}  else {
    echo "false";
}*/

    
    $id_proveedores = $db->single("SELECT provee_id FROM Proveedores WHERE provee_rut = '$proveedor_rut' ");
//########################################################################

$proveedor_fecha_inicio_soporte = "2015-01-14";
$proveedor_fecha_termino_soporte = "2015-01-14";
$proveedor_tipo_horario_soporte = "2015-01-14";
$proveedor_horario_inicio_atencion_lunes = "2015-01-14";
$proveedor_horario_inicio_atencion_martes = "2015-01-14";
$proveedor_horario_inicio_atencion_miercoles = "2015-01-14";
$proveedor_horario_inicio_atencion_jueves = "2015-01-14";
$proveedor_horario_inicio_atencion_viernes = "2015-01-14";
$proveedor_horario_inicio_atencion_sabado = "2015-01-14";
$proveedor_horario_inicio_atencion_domingo = "2015-01-14";
$proveedor_tarea_incluida = "2015-01-14";
$proveedor_incluye_respuestos = "2015-01-14";
$proveedor_sistema_tickets = "2015-01-14";
$proveedor_flujo_soporte = "2015-01-14";
$proveedor_tiempo_respuesta = "2015-01-14";
$proveedor_tipo_asistencia = "2015-01-14";
$proveedor_tipo_soporte = "2015-01-14";
$proveedor_horario_termino_atencion_lunes = "2015-01-14";
$proveedor_horario_termino_atencion_martes = "2015-01-14";
$proveedor_horario_termino_atencion_miercoles = "2015-01-14";
$proveedor_horario_termino_atencion_jueves = "2015-01-14";
$proveedor_horario_termino_atencion_viernes = "2015-01-14";
$proveedor_horario_termino_atencion_sabado = "2015-01-14";
$proveedor_horario_termino_atencion_domingo = "2015-01-14";

$string_sql_soporte = "INSERT INTO Soportes(
        soporte_fecha_inicio,
        soporte_fecha_termino,
        soporte_tipo_horario,
        soporte_hora_inicio_atencion_lunes,
        soporte_hora_inicio_atencion_martes,
        soporte_hora_inicio_atencion_miercoles,
        soporte_hora_inicio_atencion_jueves,
        soporte_hora_inicio_atencion_viernes,
        soporte_hora_inicio_atencion_sabado,
        soporte_hora_inicio_atencion_domingo,
        soporte_tareas_incluidas,
        soporte_incluye_respuestos,
        soporte_sistema_tickets,
        soporte_flujo,
        soporte_tiempo_respuesta,
        soporte_tipo_asistencia,
        soporte_tipo,
        Proveedores_provee_id,
        Servicios_serv_id,
        soporte_hora_termino_atencion_lunes,
        soporte_hora_termino_atencion_martes,
        soporte_hora_termino_atencion_miercoles,
        soporte_hora_termino_atencion_jueves,
        soporte_hora_termino_atencion_viernes,
        soporte_hora_termino_atencion_sabado,
        soporte_hora_termino_atencion_domingo) VALUES (
        '$proveedor_fecha_inicio_soporte',
        '$proveedor_fecha_termino_soporte',
        '$proveedor_tipo_horario_soporte',
        '$proveedor_horario_inicio_atencion_lunes',
        '$proveedor_horario_inicio_atencion_martes',
        '$proveedor_horario_inicio_atencion_miercoles',
        '$proveedor_horario_inicio_atencion_jueves',
        '$proveedor_horario_inicio_atencion_viernes',
        '$proveedor_horario_inicio_atencion_sabado',
        '$proveedor_horario_inicio_atencion_domingo',
        '$proveedor_tarea_incluida',
        '$proveedor_incluye_respuestos',
        '$proveedor_sistema_tickets',
        '$proveedor_flujo_soporte',
        '$proveedor_tiempo_respuesta',
        '$proveedor_tipo_asistencia',
        '$proveedor_tipo_soporte',
        '5',
        '1',
        '$proveedor_horario_termino_atencion_lunes',
        '$proveedor_horario_termino_atencion_martes',
        '$proveedor_horario_termino_atencion_miercoles',
        '$proveedor_horario_termino_atencion_jueves',
        '$proveedor_horario_termino_atencion_viernes',
        '$proveedor_horario_termino_atencion_sabado',
        '$proveedor_horario_termino_atencion_domingo')";
        
        /* Ejecutamos la query Soporte */
        $sql_insert_soporte = $db->query($string_sql_soporte);
//########################################################################

    //INGRESO SOPORTE
    /*$("#form_ingreso_proveedores").validate({
     rules: {
     'proveedor_nombre_empresa':{ required: false },
     'proveedor_rut':{ required: false },
     'proveedor_alias_fantasia':{ required: false },
     'proveedor_url_sitio_web':{ required: false },
     'proveedor_direccion':{ required: false },
     'proveedor_n_mesa_ayuda':{ required: false },
     'proveedor_fecha_inicio_soporte':{ required: false },
     'proveedor_fecha_termino_soporte':{ required: false },
     'proveedor_tarea_incluida':{ required: false },
     'proveedor_sistema_tickets':{ required: false },
     'proveedor_incluye_respuestos':{ required: false },
     'proveedor_flujo_soporte':{ required: false },
     'proveedor_tipo_horario_soporte':{ required: false },
     'proveedor_tiempo_respuesta':{ required: false },
     'proveedor_tipo_asistencia':{ required: false },
     'proveedor_tipo_soporte':{ required: false },
     'proveedor_horario_inicio_atencion_lunes':{ required: false },
     'proveedor_horario_termino_atencion_lunes':{ required: false },
     'proveedor_horario_inicio_atencion_martes':{ required: false },
     'proveedor_horario_termino_atencion_martes':{ required: false },
     'proveedor_horario_inicio_atencion_miercoles':{ required: false },
     'proveedor_horario_termino_atencion_miercoles':{ required: false },
     'proveedor_horario_inicio_atencion_jueves':{ required: false },
     'proveedor_horario_termino_atencion_jueves':{ required: false },
     'proveedor_horario_inicio_atencion_viernes':{ required: false },
     'proveedor_horario_termino_atencion_viernes':{ required: false },
     'proveedor_horario_inicio_atencion_sabado':{ required: false },
     'proveedor_horario_termino_atencion_sabado':{ required: false },
     'proveedor_horario_inicio_atencion_domingo':{ required: false },
     'proveedor_horario_termino_atencion_domingo':{ required: false },
     'proveedor_nombre_administrativo':{ required: false },
     'proveedor_cargo_administrativo':{ required: false },
     'proveedor_email_administrativo':{ required: false },
     'proveedor_tel_movil_administrativo':{ required: false },
     'proveedor_tel_fijo_administrativo':{ required: false },
     'proveedor_nombre_tecnico':{ required: false },
     'proveedor_cargo_tecnico':{ required: false },
     'proveedor_email_tecnico':{ required: false },
     'proveedor_tel_movil_tecnico':{ required: false },
     'proveedor_tel_fijo_tecnico':{ required: false },
     'proveedor_documento_oficial':{ required: false },            
     'proveedor_nota':{ required: false, maxlength: 140 },
     'proveedor_img':{required: false}
     },
     highlight: function(element) {
     $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
     //$('#busqueda_rut').removeClass('disabled');
     },
     success: function(element) {
     $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
     //$('#busqueda_rut').attr('disabled', true);
     },
     errorClass: 'help-block',
     messages: {
     'proveedor_nombre_empresa':{ required: "Campo obligatorio." },
     'proveedor_rut':{ required: "Campo obligatorio." },
     'proveedor_alias_fantasia':{ required: "Campo obligatorio." },
     'proveedor_url_sitio_web':{ required: "Campo obligatorio." },
     'proveedor_direccion':{ required: "Campo obligatorio." },
     'proveedor_n_mesa_ayuda':{ required: "Campo obligatorio." },
     'proveedor_fecha_inicio_soporte':{ required: "Campo obligatorio." },
     'proveedor_fecha_termino_soporte':{ required: "Campo obligatorio." },
     'proveedor_tarea_incluida':{ required: "Campo obligatorio." },
     'proveedor_sistema_tickets':{ required: "Campo obligatorio." },
     'proveedor_incluye_respuestos':{ required: "Campo obligatorio." },
     'proveedor_flujo_soporte':{ required: "Campo obligatorio." },
     'proveedor_tipo_horario_soporte':{ required: "Campo obligatorio." },
     'proveedor_tiempo_respuesta':{ required: "Campo obligatorio." },
     'proveedor_tipo_asistencia':{ required: "Campo obligatorio." },
     'proveedor_tipo_soporte':{ required: "Campo obligatorio." },
     'proveedor_horario_inicio_atencion_lunes':{ required: "Campo obligatorio." },
     'proveedor_horario_termino_atencion_lunes':{ required: "Campo obligatorio." },
     'proveedor_horario_inicio_atencion_martes':{ required: "Campo obligatorio." },
     'proveedor_horario_termino_atencion_martes':{ required: "Campo obligatorio." },
     'proveedor_horario_inicio_atencion_miercoles':{ required: "Campo obligatorio." },
     'proveedor_horario_termino_atencion_miercoles':{ required: "Campo obligatorio." },
     'proveedor_horario_inicio_atencion_jueves':{ required: "Campo obligatorio." },
     'proveedor_horario_termino_atencion_jueves':{ required: "Campo obligatorio." },
     'proveedor_horario_inicio_atencion_viernes':{ required: "Campo obligatorio." },
     'proveedor_horario_termino_atencion_viernes':{ required: "Campo obligatorio." },
     'proveedor_horario_inicio_atencion_sabado':{ required: "Campo obligatorio." },
     'proveedor_horario_termino_atencion_sabado':{ required: "Campo obligatorio." },
     'proveedor_horario_inicio_atencion_domingo':{ required: "Campo obligatorio." },
     'proveedor_horario_termino_atencion_domingo':{ required: "Campo obligatorio." },
     'proveedor_nombre_administrativo':{ required: "Campo obligatorio." },
     'proveedor_cargo_administrativo':{ required: "Campo obligatorio." },
     'proveedor_email_administrativo':{ required: "Campo obligatorio." },
     'proveedor_tel_movil_administrativo':{ required: "Campo obligatorio." },
     'proveedor_tel_fijo_administrativo':{ required: "Campo obligatorio." },
     'proveedor_nombre_tecnico':{ required: "Campo obligatorio." },
     'proveedor_cargo_tecnico':{ required: "Campo obligatorio." },
     'proveedor_email_tecnico':{ required: "Campo obligatorio." },
     'proveedor_tel_movil_tecnico':{ required: "Campo obligatorio." },
     'proveedor_tel_fijo_tecnico':{ required: "Campo obligatorio." },
     'proveedor_documento_oficial':{ required: "Campo obligatorio." },            
     'proveedor_nota':{ required: "Campo obligatorio.", maxlength: "140 Caracteres" },
     'proveedor_img':{ required: "Campo obligatorio."}
     
     },
     submitHandler: function(form) {
     //var input_selec_año = $("#user_anno_ingreso").val();
     //var proveedor_nota = $("#proveedor_nota").val();
     //alert(proveedor_nota);
     //$('#div_resultado_agregar_usuario').html('<img src="../grafica/img/ajax-loader.gif" alt="" />');
     //alert(input_selec_año);
     var dataString = 'busqueda=' + 100;
     $.ajax({
     type: "POST",
     url: "./modelo/ingreso_proveedor_BD.php",
     data: $(form).serialize(),
     //data: dataString,
     success: function(data) {
     //$('#div_resultado_agregar_usuario').fadeIn(1000).html(data);
     alert(data);
     }
     
     });
     }
     });*/

//ID_SERVICIOS
$Servicios_serv_id = $db->single("SELECT serv_id FROM Servicios WHERE serv_nombre = '$servicio_nombre'");
//########################################################################
//JEFES DE PROYECTOS
$string_sql_jefesproyecto = "INSERT INTO JefesProyecto("
        . "jefesproyec_nombre, "
        . "jefesproyec_email, "
        . "Servicios_serv_id) VALUES ("
        . "'$servicio_nombre_jefe_proyecto',"
        . "'$servicio_email_jefe_proyecto',"
        . "'$Servicios_serv_id')";
$sql_insert_jefesproyecto = $db->query($string_sql_jefesproyecto);
//########################################################################
//ARQUITECTOS
$string_sql_arquitectos = "INSERT INTO Arquitectos("
        . "arquitec_nombre, "
        . "arquitec_email, "
        . "Servicios_serv_id) VALUES ("
        . "'$servicio_nombre_arquitecto',"
        . "'$servicio_email_arquitecto',"
        . "'$Servicios_serv_id')";
$sql_insert_arquitectos = $db->query($string_sql_arquitectos);
//########################################################################
//ENCARGADOS
$string_sql_encargados = "INSERT INTO Encargados("
        . "encarga_nombre, "
        . "encarga_email, "
        . "encarga_dpto, "
        . "Servicios_serv_id) VALUES ("
        . "'',"
        . "'',"
        . "'',"
        . "'$Servicios_serv_id')";
$sql_insert_encargados = $db->query($string_sql_encargados);
//########################################################################
//CLIENTES
$string_sql_cliente = "INSERT INTO Clientes("
        . "client_area, "
        . "cliente_nombre_contacto, "
        . "client_motivo, "
        . "client_email) VALUES ("
        . "'',"
        . "'',"
        . "'',"
        . "'')";
$sql_insert_cliente = $db->query($string_sql_cliente);
//########################################################################
//########################################################################
if(isset($servicio_responsabilidad_proveedor2)){
    $string_sql_servicios_N_M_2 = "INSERT INTO Servicios_has_Proveedores("
            . "Servicios_serv_id, "
            . "Proveedores_provee_id) VALUES ("
            . "'$id_servicio_sql',"
            . "'$selec_proveedor_servicio2')";
    /* Ejecutamos la query Servicio */
    $sql_insert_servicios_N_M_2 = $db->query($string_sql_servicios_N_M_2);    
}
if($servicio_responsabilidad_proveedor3 != ""){
    $string_sql_servicios_N_M_2 = "INSERT INTO Servicios_has_Proveedores("
            . "Servicios_serv_id, "
            . "Proveedores_provee_id) VALUES ("
            . "'$id_servicio_sql',"
            . "'$selec_proveedor_servicio3')";
    /* Ejecutamos la query Servicio */
    $sql_insert_servicios_N_M_3 = $db->query($string_sql_servicios_N_M_3);    
}
//########################################################################
$string_sql_servicios_N_M = "INSERT INTO Servicios_has_Proveedores("
        . "Servicios_serv_id, "
        . "Proveedores_provee_id) VALUES ("
        . "'$id_servicio_sql',"
        . "'$selec_proveedor_servicio')";
/* Ejecutamos la query Servicio */
$sql_insert_servicios_N_M = $db->query($string_sql_servicios_N_M);
