<?php
session_start();
//Nombre del usuario que esta logeado
$NombrePersona = $_SESSION["datos_usuario_logueado"][0];

require_once '../controlador/Db.class.php';
$db = new Db();
//Nombre del usuario que selecciona al que le va enviar el mensaje
$nombre_usuario_para = $_POST["input_nombre_usuario"];
$id_usuario = $db->single("SELECT usuario_id FROM Usuarios WHERE usuario_nombre = '$nombre_usuario_para'");

//$string_sql_mensajes_usuarios = "SELECT *,date_format(chat_fecha,'%d-%m-%Y') AS chat_fecha FROM Chat WHERE chat_para_user = '$nombre_usuario_para' AND Usuarios_usuario_id = '$id_usuario' ORDER BY chat_id ASC";
//$string_sql_mensajes_usuarios = "SELECT *,date_format(chat_fecha,'%d-%m-%Y') AS chat_fecha FROM Chat WHERE Usuarios_usuario_id = '$id_usuario' AND chat_para_user = '$NombrePersona' ORDER BY chat_id ASC";
$string_sql_mensajes_usuarios = "SELECT *,date_format(chat_fecha,'%d-%m-%Y') AS chat_fecha FROM Chat ORDER BY chat_id ASC";
//$string_sql_mensajes_usuarios = "SELECT *,date_format(chat_fecha,'%d-%m-%Y') AS chat_fecha FROM Chat WHERE Usuarios_usuario_id = '$id_usuario' AND chat_para_user = '$NombrePersona' ORDER BY chat_id ASC";


echo $string_sql_mensajes_usuarios;

$DATOS_CHAT = $db->query($string_sql_mensajes_usuarios);

date_default_timezone_set("Etc/GMT+3");
$horaFin = date("H:i:s");
function restaHoras($horaIni, $horaFin){
    return (date("i",strtotime("00:00:00")+ strtotime($horaFin) - strtotime($horaIni) ));
}
foreach ($DATOS_CHAT as $row_chat):
    $horaIni = $row_chat["chat_hora"];
    
    $nombre_persona_chat = $db->single("SELECT usuario_nombre FROM Usuarios WHERE usuario_id = " . $row_chat["Usuarios_usuario_id"] . "");
    if ($nombre_persona_chat == $NombrePersona) {
        echo '<li class="left clearfix">
            <span class="chat-img pull-left">
                <img src="img/user3.jpg" alt="User Avatar">
            </span>
            <div class="chat-body clearfix">
                <div class="header">
                    <strong class="primary-font">De: ' . $nombre_persona_chat . '</strong>
                    <small class="pull-right text-muted"><i class="fa fa-clock-o"></i> '.$row_chat["chat_hora"].' - '.$row_chat["chat_fecha"].'</small>
                </div>
                <p>' . $row_chat["chat_mensaje"] . '</p>
            </div>
    </li>';
    } else {
        echo '<li class="right clearfix">
                <span class="chat-img pull-right">
                    <img src="img/user.jpg" alt="User Avatar">
                </span>
                <div class="chat-body clearfix">
                    <div class="header">
                    <div class="pull-right"><strong class="primary-font">De: ' . $nombre_persona_chat . '</strong></div>
                    <div class="pull-left"><small class="text-muted"><i class="fa fa-clock-o"></i> '.$row_chat["chat_hora"].' - '.$row_chat["chat_fecha"].'</small></div>
                </br>
                </div>
                    <p>' . $row_chat["chat_mensaje"] . '</p>
                </div>
            </li>';
    }
endforeach;

