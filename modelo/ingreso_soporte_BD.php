<?php

$proveedor_fecha_inicio_soporte = $_POST["proveedor_fecha_inicio_soporte"];
$proveedor_fecha_inicio_soporte_format = date_format($proveedor_fecha_inicio_soporte, "%Y/%m/%d");
$proveedor_fecha_termino_soporte = $_POST["proveedor_fecha_termino_soporte"];
$proveedor_tipo_horario_soporte = $_POST["proveedor_tipo_horario_soporte"];
$proveedor_horario_inicio_atencion_lunes = $_POST["proveedor_horario_inicio_atencion_lunes"];
$proveedor_horario_inicio_atencion_martes = $_POST["proveedor_horario_inicio_atencion_martes"];
$proveedor_horario_inicio_atencion_miercoles = $_POST["proveedor_horario_inicio_atencion_miercoles"];
$proveedor_horario_inicio_atencion_jueves = $_POST["proveedor_horario_inicio_atencion_jueves"];
$proveedor_horario_inicio_atencion_viernes = $_POST["proveedor_horario_inicio_atencion_viernes"];
$proveedor_horario_inicio_atencion_sabado = $_POST["proveedor_horario_inicio_atencion_sabado"];
$proveedor_horario_inicio_atencion_domingo = $_POST["proveedor_horario_inicio_atencion_domingo"];
$proveedor_tarea_incluida = $_POST["proveedor_tarea_incluida"];
$proveedor_incluye_respuestos = $_POST["proveedor_incluye_respuestos"];
$proveedor_sistema_tickets = $_POST["proveedor_sistema_tickets"];
$proveedor_flujo_soporte = $_POST["proveedor_flujo_soporte"];
$proveedor_tiempo_respuesta = $_POST["proveedor_tiempo_respuesta"];
//$proveedor_tipo_asistencia = $_POST["proveedor_tipo_asistencia"];
//$proveedor_tipo_soporte = $_POST["proveedor_tipo_soporte"];
$proveedor_horario_termino_atencion_lunes = $_POST["proveedor_horario_termino_atencion_lunes"];
$proveedor_horario_termino_atencion_martes = $_POST["proveedor_horario_termino_atencion_martes"];
$proveedor_horario_termino_atencion_miercoles = $_POST["proveedor_horario_termino_atencion_miercoles"];
$proveedor_horario_termino_atencion_jueves = $_POST["proveedor_horario_termino_atencion_jueves"];
$proveedor_horario_termino_atencion_viernes = $_POST["proveedor_horario_termino_atencion_viernes"];
$proveedor_horario_termino_atencion_sabado = $_POST["proveedor_horario_termino_atencion_sabado"];
$proveedor_horario_termino_atencion_domingo = $_POST["proveedor_horario_termino_atencion_domingo"];
$selec_proveedor_soporte_ID = $_POST["selec_proveedor_soporte"];
$selec_servicio_soporte_ID = $_POST["selec_servicio_soporte"];
//##########################################################################
$Presencial = $_POST["proveedor_tipo_asistencia"][0];
$Remoto = $_POST["proveedor_tipo_asistencia"][1];
//##########################################################################
$Reactivo = $_POST["proveedor_tipo_soporte"][0];
$Proactivo = $_POST["proveedor_tipo_soporte"][1];
$Evolutivo = $_POST["proveedor_tipo_soporte"][2];
//##########################################################################
//SE INSTANCIA LA CLASE DE LA BASE DATOS
require_once '../controlador/Db.class.php';
$db = new Db();
//##########################################################################
if ($db->single("SELECT COUNT(*) FROM Soportes WHERE "
        . "Proveedores_provee_id = '$selec_proveedor_soporte_ID' AND "
        . "Servicios_serv_id = '$selec_servicio_soporte_ID' AND "
        . "soporte_sistema_tickets = '$proveedor_sistema_tickets'") == 0) {
    
    $string_sql_soporte = "INSERT INTO Soportes("
            . "soporte_fecha_inicio, "
            . "soporte_fecha_termino, "
            . "soporte_tipo_horario, "
            . "soporte_hora_inicio_atencion_lunes, "
            . "soporte_hora_inicio_atencion_martes, "
            . "soporte_hora_inicio_atencion_miercoles, "
            . "soporte_hora_inicio_atencion_jueves, "
            . "soporte_hora_inicio_atencion_viernes, "
            . "soporte_hora_inicio_atencion_sabado, "
            . "soporte_hora_inicio_atencion_domingo, "
            . "soporte_tareas_incluidas, "
            . "soporte_incluye_respuestos, "
            . "soporte_sistema_tickets, "
            . "soporte_flujo, "
            . "soporte_tiempo_respuesta, "
            . "soporte_tipo_asistencia_presencial,"
            . "soporte_tipo_asistencia_remoto, "
            . "soporte_tipo_reactivo, "
            . "soporte_tipo_proactivo, "
            . "soporte_tipo_evolutivo, "
            . "Proveedores_provee_id, "
            . "Servicios_serv_id, "
            . "soporte_hora_termino_atencion_lunes, "
            . "soporte_hora_termino_atencion_martes, "
            . "soporte_hora_termino_atencion_miercoles, "
            . "soporte_hora_termino_atencion_jueves, "
            . "soporte_hora_termino_atencion_viernes, "
            . "soporte_hora_termino_atencion_sabado, "
            . "soporte_hora_termino_atencion_domingo) VALUES ("
            . "STR_TO_DATE('$proveedor_fecha_inicio_soporte','%d-%m-%Y'),"
            . "STR_TO_DATE('$proveedor_fecha_termino_soporte','%d-%m-%Y'),"
            . "'$proveedor_tipo_horario_soporte',"
            . "'$proveedor_horario_inicio_atencion_lunes',"
            . "'$proveedor_horario_inicio_atencion_martes',"
            . "'$proveedor_horario_inicio_atencion_miercoles',"
            . "'$proveedor_horario_inicio_atencion_jueves',"
            . "'$proveedor_horario_inicio_atencion_viernes',"
            . "'$proveedor_horario_inicio_atencion_sabado',"
            . "'$proveedor_horario_inicio_atencion_domingo',"
            . "'$proveedor_tarea_incluida',"
            . "'$proveedor_incluye_respuestos',"
            . "'$proveedor_sistema_tickets',"
            . "'$proveedor_flujo_soporte',"
            . "'$proveedor_tiempo_respuesta',"
            . "'$Presencial',"
            . "'$Remoto',"
            . "'$Reactivo',"
            . "'$Proactivo',"
            . "'$Evolutivo',"
            . "'$selec_proveedor_soporte_ID',"
            . "'$selec_servicio_soporte_ID',"
            . "'$proveedor_horario_termino_atencion_lunes',"
            . "'$proveedor_horario_termino_atencion_martes',"
            . "'$proveedor_horario_termino_atencion_miercoles',"
            . "'$proveedor_horario_termino_atencion_jueves',"
            . "'$proveedor_horario_termino_atencion_viernes',"
            . "'$proveedor_horario_termino_atencion_sabado',"
            . "'$proveedor_horario_termino_atencion_domingo')";
    /* Ejecutamos la query Servicio */
    $sql_insert_soporte = $db->query($string_sql_soporte);
    if ($sql_insert_soporte == true) {
        echo "1";
    } else {
        echo '2';
    }
} else {
    echo '0';
}
