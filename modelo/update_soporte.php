<?php
session_start();
$soporte_id = $_SESSION["soporte_id"];
require_once '../controlador/Db.class.php';
$db = new Db();
$soporte_update_FechaInicioSoporte = $_POST["soporte_update_FechaInicioSoporte"];
if ($soporte_update_FechaInicioSoporte == $db->single("SELECT date_format(soporte_fecha_inicio,'%d-%m-%Y') as soporte_fecha_inicio FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($soporte_update_FechaInicioSoporte == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Igual al de la BD, No Actualiza";
} else {
    if ($soporte_update_FechaInicioSoporte != "") {
        $update_FechaIncio = $db->query("UPDATE Soportes SET soporte_fecha_inicio = STR_TO_DATE('$soporte_update_FechaInicioSoporte','%d-%m-%Y') WHERE soporte_id = '$soporte_id'");
        if($update_FechaIncio == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$soporte_update_FechaTerminoSoporte = $_POST["soporte_update_FechaTerminoSoporte"];
if ($soporte_update_FechaTerminoSoporte == $db->single("SELECT date_format(soporte_fecha_termino,'%d-%m-%Y') as soporte_fecha_termino FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($soporte_update_FechaTerminoSoporte == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Igual al de la BD, No Actualiza";
} else {
    if ($soporte_update_FechaTerminoSoporte != "") {
        $update_FechaTermino = $db->query("UPDATE Soportes SET soporte_fecha_termino = STR_TO_DATE('$soporte_update_FechaTerminoSoporte','%d-%m-%Y') WHERE soporte_id = '$soporte_id'");
        if($update_FechaTermino == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$soporte_update_TipoHorarioSoporte = $_POST["soporte_update_TipoHorarioSoporte"];
if ($soporte_update_TipoHorarioSoporte == $db->single("SELECT soporte_tipo_horario FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($soporte_update_TipoHorarioSoporte == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Igual al de la BD, No Actualiza";
} else {
    if ($soporte_update_TipoHorarioSoporte != "") {
        $update_TipoHorario = $db->query("UPDATE Soportes SET soporte_tipo_horario = '$soporte_update_TipoHorarioSoporte' WHERE soporte_id = '$soporte_id'");
        if($update_TipoHorario == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$soporte_update_SistemaTickets = $_POST["soporte_update_SistemaTickets"];
if ($soporte_update_SistemaTickets == $db->single("SELECT soporte_sistema_tickets FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($soporte_update_SistemaTickets == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Igual al de la BD, No Actualiza";
} else {
    if ($soporte_update_SistemaTickets != "") {
        $update_SistemaTickets = $db->query("UPDATE Soportes SET soporte_sistema_tickets = '$soporte_update_SistemaTickets' WHERE soporte_id = '$soporte_id'");
        if($update_SistemaTickets == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$soporte_TipoAsistencia_BD = $_POST["soporte_update_TipoAsistencia_BD"];
$Presencial = $_POST["soporte_update_TipoAsistencia_Presencial"];
$Remoto = $_POST["soporte_update_TipoAsistencia_Remoto"];
if ($Presencial != "" AND $Remoto != "") {
    //$TipoAsitencia = "Presencial - Remoto";
    $update_Remoto = $db->query("UPDATE Soportes SET soporte_tipo_asistencia_remoto = 'Remoto' WHERE soporte_id = '$soporte_id'");
    $update_Presencial = $db->query("UPDATE Soportes SET soporte_tipo_asistencia_presencial = 'Presencial' WHERE soporte_id = '$soporte_id'");
    echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
    //echo "Remoto: ".$Remoto." // "."Presencial: ".$Presencial;
} elseif ($Presencial != "") {
    //$TipoAsitencia = "Presencial";
    $update_Remoto = $db->query("UPDATE Soportes SET soporte_tipo_asistencia_remoto = '' WHERE soporte_id = '$soporte_id'");
    $update_Presencial = $db->query("UPDATE Soportes SET soporte_tipo_asistencia_presencial = 'Presencial' WHERE soporte_id = '$soporte_id'");
    echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
    //echo "Remoto: ".$Remoto." // "."Presencial: ".$Presencial;
} elseif ($Remoto != "") {
    //$TipoAsitencia = "Remoto";
    $update_Remoto = $db->query("UPDATE Soportes SET soporte_tipo_asistencia_remoto = 'Remoto' WHERE soporte_id = '$soporte_id'");
    $update_Presencial = $db->query("UPDATE Soportes SET soporte_tipo_asistencia_presencial = '' WHERE soporte_id = '$soporte_id'");
    echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
    //echo "Remoto: ".$Remoto." // "."Presencial: ".$Presencial;
}
######################################################################################################################
$proveedor_tipo_soporte_reactivo = $_POST["proveedor_tipo_soporte_reactivo"];
$proveedor_tipo_soporte_proactivo = $_POST["proveedor_tipo_soporte_proactivo"];
$proveedor_tipo_soporte_evolutivo = $_POST["proveedor_tipo_soporte_evolutivo"];
if ($proveedor_tipo_soporte_reactivo != "" AND $proveedor_tipo_soporte_proactivo != "" AND $proveedor_tipo_soporte_evolutivo != "") {
    $update_Reactivo = $db->query("UPDATE Soportes SET soporte_tipo_reactivo = '$proveedor_tipo_soporte_reactivo' WHERE soporte_id = '$soporte_id'");
    $update_Proactivo = $db->query("UPDATE Soportes SET soporte_tipo_proactivo = '$proveedor_tipo_soporte_proactivo' WHERE soporte_id = '$soporte_id'");
    $update_Evolutivo = $db->query("UPDATE Soportes SET soporte_tipo_evolutivo = '$proveedor_tipo_soporte_evolutivo' WHERE soporte_id = '$soporte_id'");
    echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
}elseif ($proveedor_tipo_soporte_reactivo != "" AND $proveedor_tipo_soporte_proactivo != "") {
    $update_Reactivo = $db->query("UPDATE Soportes SET soporte_tipo_reactivo = '$proveedor_tipo_soporte_reactivo' WHERE soporte_id = '$soporte_id'");
    $update_Proactivo = $db->query("UPDATE Soportes SET soporte_tipo_proactivo = '$proveedor_tipo_soporte_proactivo' WHERE soporte_id = '$soporte_id'");
    $update_Evolutivo = $db->query("UPDATE Soportes SET soporte_tipo_evolutivo = '' WHERE soporte_id = '$soporte_id'");
    echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
}elseif ($proveedor_tipo_soporte_reactivo != "" AND $proveedor_tipo_soporte_evolutivo != "") {
    $update_Reactivo = $db->query("UPDATE Soportes SET soporte_tipo_reactivo = '$proveedor_tipo_soporte_reactivo' WHERE soporte_id = '$soporte_id'");
    $update_Proactivo = $db->query("UPDATE Soportes SET soporte_tipo_proactivo = '' WHERE soporte_id = '$soporte_id'");
    $update_Evolutivo = $db->query("UPDATE Soportes SET soporte_tipo_evolutivo = '$proveedor_tipo_soporte_evolutivo' WHERE soporte_id = '$soporte_id'");
    echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
}elseif ($proveedor_tipo_soporte_proactivo != "" AND $proveedor_tipo_soporte_evolutivo != "") {
    $update_Reactivo = $db->query("UPDATE Soportes SET soporte_tipo_reactivo = '' WHERE soporte_id = '$soporte_id'");
    $update_Proactivo = $db->query("UPDATE Soportes SET soporte_tipo_proactivo = '$proveedor_tipo_soporte_proactivo' WHERE soporte_id = '$soporte_id'");
    $update_Evolutivo = $db->query("UPDATE Soportes SET soporte_tipo_evolutivo = '$proveedor_tipo_soporte_evolutivo' WHERE soporte_id = '$soporte_id'");
    echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
}
######################################################################################################################
$soporte_update_TiemposRespuestas = $_POST["soporte_update_TiemposRespuestas"];
if ($soporte_update_TiemposRespuestas == $db->single("SELECT soporte_tiempo_respuesta FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($soporte_update_TiemposRespuestas == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Igual al de la BD, No Actualiza";
} else {
    if ($soporte_update_TiemposRespuestas != "") {
        $update_TiempoRespuesta = $db->query("UPDATE Soportes SET soporte_tiempo_respuesta = '$soporte_update_TiemposRespuestas' WHERE soporte_id = '$soporte_id'");
        if($update_TiempoRespuesta == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$soporte_update_TareasIncluidas = $_POST["soporte_update_TareasIncluidas"];
if ($soporte_update_TareasIncluidas == $db->single("SELECT soporte_tareas_incluidas FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($soporte_update_TareasIncluidas == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Igual al de la BD, No Actualiza";
} else {
    if ($soporte_update_TareasIncluidas != "") {
        $update_TareasIncluidas = $db->query("UPDATE Soportes SET soporte_tareas_incluidas = '$soporte_update_TareasIncluidas' WHERE soporte_id = '$soporte_id'");
        if($update_TareasIncluidas == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$soporte_update_FlujoSoporte = $_POST["soporte_update_FlujoSoporte"];
if ($soporte_update_FlujoSoporte == $db->single("SELECT soporte_flujo FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($soporte_update_FlujoSoporte == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Igual al de la BD, No Actualiza";
} else {
    if ($soporte_update_FlujoSoporte != "") {
        $update_FlujoSoporte = $db->query("UPDATE Soportes SET soporte_flujo = '$soporte_update_FlujoSoporte' WHERE soporte_id = '$soporte_id'");
        if($update_FlujoSoporte == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$proveedor_horario_inicio_atencion_lunes_update = $_POST["proveedor_horario_inicio_atencion_lunes_update"];
if ($proveedor_horario_inicio_atencion_lunes_update == $db->single("SELECT soporte_hora_inicio_atencion_lunes FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_inicio_atencion_lunes_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_inicio_atencion_lunes_update != "") {        
        $update_inicio_lunes = $db->query("UPDATE Soportes SET soporte_hora_inicio_atencion_lunes = '$proveedor_horario_inicio_atencion_lunes_update' WHERE soporte_id ='$soporte_id'");
        if($update_inicio_lunes == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
$proveedor_horario_termino_atencion_lunes_update = $_POST["proveedor_horario_termino_atencion_lunes_update"];
if ($proveedor_horario_termino_atencion_lunes_update == $db->single("SELECT soporte_hora_termino_atencion_lunes FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_termino_atencion_lunes_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_termino_atencion_lunes_update != "") {        
        $update_termino_lunes = $db->query("UPDATE Soportes SET soporte_hora_termino_atencion_lunes = '$proveedor_horario_termino_atencion_lunes_update' WHERE soporte_id ='$soporte_id'");
        if($update_termino_lunes == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$proveedor_horario_inicio_atencion_martes_update = $_POST["proveedor_horario_inicio_atencion_martes_update"];
if ($proveedor_horario_inicio_atencion_martes_update == $db->single("SELECT soporte_hora_inicio_atencion_martes FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_inicio_atencion_martes_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_inicio_atencion_martes_update != "") {        
        $update_inicio_martes = $db->query("UPDATE Soportes SET soporte_hora_inicio_atencion_martes = '$proveedor_horario_inicio_atencion_martes_update' WHERE soporte_id ='$soporte_id'");
        if($update_inicio_martes == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
$proveedor_horario_termino_atencion_martes_update = $_POST["proveedor_horario_termino_atencion_martes_update"];
if ($proveedor_horario_termino_atencion_martes_update == $db->single("SELECT soporte_hora_termino_atencion_martes FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_termino_atencion_martes_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_termino_atencion_martes_update != "") {        
        $update_termino_martes = $db->query("UPDATE Soportes SET soporte_hora_termino_atencion_martes = '$proveedor_horario_termino_atencion_martes_update' WHERE soporte_id ='$soporte_id'");
        if($update_termino_martes == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$proveedor_horario_inicio_atencion_miercoles_update = $_POST["proveedor_horario_inicio_atencion_miercoles_update"];
if ($proveedor_horario_inicio_atencion_miercoles_update == $db->single("SELECT soporte_hora_inicio_atencion_miercoles FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_inicio_atencion_miercoles_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_inicio_atencion_miercoles_update != "") {        
        $update_inicio_miercoles = $db->query("UPDATE Soportes SET soporte_hora_inicio_atencion_miercoles = '$proveedor_horario_inicio_atencion_miercoles_update' WHERE soporte_id ='$soporte_id'");
        if($update_inicio_miercoles == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
$proveedor_horario_termino_atencion_miercoles_update = $_POST["proveedor_horario_termino_atencion_miercoles_update"];
if ($proveedor_horario_termino_atencion_miercoles_update == $db->single("SELECT soporte_hora_termino_atencion_miercoles FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_termino_atencion_miercoles_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_termino_atencion_miercoles_update != "") {        
        $update_termino_miercoles = $db->query("UPDATE Soportes SET soporte_hora_termino_atencion_miercoles = '$proveedor_horario_termino_atencion_miercoles_update' WHERE soporte_id ='$soporte_id'");
        if($update_termino_miercoles == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$proveedor_horario_inicio_atencion_jueves_update = $_POST["proveedor_horario_inicio_atencion_jueves_update"];
if ($proveedor_horario_inicio_atencion_jueves_update == $db->single("SELECT soporte_hora_inicio_atencion_jueves FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_inicio_atencion_jueves_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_inicio_atencion_jueves_update != "") {        
        $update_inicio_jueves = $db->query("UPDATE Soportes SET soporte_hora_inicio_atencion_jueves = '$proveedor_horario_inicio_atencion_jueves_update' WHERE soporte_id ='$soporte_id'");
        if($update_inicio_jueves == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
$proveedor_horario_termino_atencion_jueves_update = $_POST["proveedor_horario_termino_atencion_jueves_update"];
if ($proveedor_horario_termino_atencion_jueves_update == $db->single("SELECT soporte_hora_termino_atencion_jueves FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_termino_atencion_jueves_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_termino_atencion_jueves_update != "") {        
        $update_termino_jueves = $db->query("UPDATE Soportes SET soporte_hora_termino_atencion_jueves = '$proveedor_horario_termino_atencion_jueves_update' WHERE soporte_id ='$soporte_id'");
        if($update_termino_jueves == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$proveedor_horario_inicio_atencion_viernes_update = $_POST["proveedor_horario_inicio_atencion_viernes_update"];
if ($proveedor_horario_inicio_atencion_viernes_update == $db->single("SELECT soporte_hora_inicio_atencion_viernes FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_inicio_atencion_viernes_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_inicio_atencion_viernes_update != "") {        
        $update_inicio_viernes = $db->query("UPDATE Soportes SET soporte_hora_inicio_atencion_viernes = '$proveedor_horario_inicio_atencion_viernes_update' WHERE soporte_id ='$soporte_id'");
        if($update_inicio_viernes == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
$proveedor_horario_termino_atencion_viernes_update = $_POST["proveedor_horario_termino_atencion_viernes_update"];
if ($proveedor_horario_termino_atencion_viernes_update == $db->single("SELECT soporte_hora_termino_atencion_viernes FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_termino_atencion_viernes_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_termino_atencion_viernes_update != "") {        
        $update_termino_viernes = $db->query("UPDATE Soportes SET soporte_hora_termino_atencion_viernes = '$proveedor_horario_termino_atencion_viernes_update' WHERE soporte_id ='$soporte_id'");
        if($update_termino_viernes == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$proveedor_horario_inicio_atencion_sabado_update = $_POST["proveedor_horario_inicio_atencion_sabado_update"];
if ($proveedor_horario_inicio_atencion_sabado_update == $db->single("SELECT soporte_hora_inicio_atencion_sabado FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_inicio_atencion_sabado_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_inicio_atencion_sabado_update != "") {        
        $update_inicio_sabado = $db->query("UPDATE Soportes SET soporte_hora_inicio_atencion_sabado = '$proveedor_horario_inicio_atencion_sabado_update' WHERE soporte_id ='$soporte_id'");
        if($update_inicio_sabado == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
$proveedor_horario_termino_atencion_sabado_update = $_POST["proveedor_horario_termino_atencion_sabado_update"];
if ($proveedor_horario_termino_atencion_sabado_update == $db->single("SELECT soporte_hora_termino_atencion_sabado FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_termino_atencion_sabado_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_termino_atencion_sabado_update != "") {        
        $update_termino_sabado = $db->query("UPDATE Soportes SET soporte_hora_termino_atencion_sabado = '$proveedor_horario_termino_atencion_sabado_update' WHERE soporte_id ='$soporte_id'");
        if($update_termino_sabado == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$proveedor_horario_inicio_atencion_domingo_update = $_POST["proveedor_horario_inicio_atencion_domingo_update"];
if ($proveedor_horario_inicio_atencion_domingo_update == $db->single("SELECT soporte_hora_inicio_atencion_domingo FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_inicio_atencion_domingo_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_inicio_atencion_domingo_update != "") {        
        $update_inicio_domingo = $db->query("UPDATE Soportes SET soporte_hora_inicio_atencion_domingo = '$proveedor_horario_inicio_atencion_domingo_update' WHERE soporte_id ='$soporte_id'");
        if($update_inicio_domingo == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
$proveedor_horario_termino_atencion_domingo_update = $_POST["proveedor_horario_termino_atencion_domingo_update"];
if ($proveedor_horario_termino_atencion_domingo_update == $db->single("SELECT soporte_hora_termino_atencion_domingo FROM Soportes WHERE soporte_id = '$soporte_id'")) {
    if ($proveedor_horario_termino_atencion_domingo_update == "") {
        //echo "Campo Vacio";
    }
    //echo "Nombre Fantasia Igual al de la BD, No Actualiza";
} else {
    if ($proveedor_horario_termino_atencion_domingo_update != "") {        
        $update_termino_domingo = $db->query("UPDATE Soportes SET soporte_hora_termino_atencion_domingo = '$proveedor_horario_termino_atencion_domingo_update' WHERE soporte_id ='$soporte_id'");
        if($update_termino_domingo == true){
            echo '<div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }else{
            echo '<div class="alert alert-danger"><strong>Well done!</strong> You successfully read this important alert message.</div>';
        }
    }
}
######################################################################################################################
$db->CloseConnection();