<?php
/*
  -----------------------------------------------------------------------
  DATOS DE CONTACTO
  -----------------------------------------------------------------------
  Desarrollador: Victor Manuel Diaz Galdames.
  Nivel Profesional: Ingeniero Informático
  Número Contacto (Móvil): +5699491896
  Correo Eletrónico: victordiazgaldames@gmail.com
  Curriculum Vitae: http://victordiaz.nfconnection.cl/
  Nacionalidad: Chileno
  Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
  -----------------------------------------------------------------------
  DATOS DE JEFE A CARGO
  -----------------------------------------------------------------------
  Nombre: Nelson Horacio Guzman Mora.
  Número Contacto (Móvil): +56998289358
  Correo Eletrónico: nguzman@entel.cl
  Cargo: Jefe Operación Plataformas
 */
?>
<?php
session_start();
$NombrePersona = $_SESSION["datos_usuario_logueado"][0];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][1];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][2];
$TelefonoMovil = $_SESSION["datos_usuario_logueado"][3];
$EmailUsuario = $_SESSION["datos_usuario_logueado"][4];
$Username = $_SESSION["datos_usuario_logueado"][5];
$Contraseña = $_SESSION["datos_usuario_logueado"][6];
$PemisosUsuario = $_SESSION["datos_usuario_logueado"][7];
$PemisosRol = $_SESSION["datos_usuario_logueado"][8];
if (!isset($NombrePersona)) {
    header('Location: login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/header.inc.php'; ?> 
            <!-- ######################################## -->
            <?php require_once './includes/menubar.inc.php'; ?> 
            <!-- ######################################## -->
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.php"> Home</a></li>
                        <li>Page</li>	 
                        <li class="active">Profile</li>	 
                    </ul>
                </div><!--breadcrumb-->
                <ul class="tab-bar grey-tab">
                    <li class="active">
                        <a href="#overview" data-toggle="tab">
                            <span class="block text-center">
                                <i class="fa fa-pencil-square-o fa-2x"></i> 
                            </span>
                            Editar Perfil
                        </a>
                    </li>
                </ul>

                <div class="padding-md">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="row padding-md">
                                <a href="#formModal_UpdateIMG" role="button" data-toggle="modal"><img src="<?php echo $db->single("SELECT usuario_img_url FROM Usuarios WHERE usuario_nombre = '$NombrePersona' AND usuario_username = '$Username'"); ?>" alt="User Avatar" class="img-thumbnail"></a>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <strong class="font-14"><?php echo $NombrePersona . " " . $ApellidoPaterno; ?></strong>
                                    <small class="block text-muted"><?php echo $EmailUsuario; ?></small> 
                                    <small class="block text-muted"><?php echo $TelefonoMovil; ?></small> 
                                    <small class="block text-muted"><?php echo $PemisosRol; ?></small> 
                                    <div class="seperator"></div>
                                    <div class="seperator"></div>
                                    <div class="seperator"></div>
                                    <div class="seperator"></div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.col -->
                        <div class="col-md-9 col-sm-9">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="overview">
                                    <div class="panel panel-default">
                                        <form class="form-horizontal form-border">
                                            <div class="panel-heading">
                                                Modificar Datos Personales
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Nombre de Usuario</label>												
                                                    <div class="col-md-10">
                                                        <input type="text" disabled="" class="form-control input-sm" placeholder="Username" value="<?php echo $Username; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Nombre o Nombres</label>												
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control input-sm" placeholder="Nombre" value="<?php echo $NombrePersona; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Apellido Paterno</label>												
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control input-sm" placeholder="Apellido Paterno" value="<?php echo $ApellidoPaterno; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Apellido Materno</label>												
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control input-sm" placeholder="Apellido Materno" value="<?php echo $ApellidoMaterno; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Contraseña</label>
                                                    <div class="col-md-10">
                                                        <input type="password" class="form-control input-sm" value="">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Reingrese Contraseña</label>
                                                    <div class="col-md-10">
                                                        <input type="password" class="form-control input-sm" value="">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Email</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control input-sm" value="<?php echo $EmailUsuario; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Teléfono Móvil</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control input-sm" value="<?php echo $TelefonoMovil; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                            </div>
                                            <div class="panel-footer">
                                                <div class="text-right">
                                                    <button class="btn btn-sm btn-success">Update</button>
                                                    <button class="btn btn-sm btn-success" type="reset">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div><!-- /panel -->
                                </div><!-- /tab1 -->
                            </div><!-- /tab-content -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->			
                </div><!-- /.padding-md -->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/confirmation.logout.inc.php'; ?> 
        <!-- ########################################################### -->
        <div id="formModal_UpdateIMG" class="modal fade in" role="dialog" style="">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="form_Update_img_usuario_inventario" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <!--<div class="hidden"><input id="Update_img_id" name="Update_img_id" type="text"></div>-->
                                <div class="col-md-6 text-center" style="border-right: 1px solid #D2D2D2;"><img id="img_user" class=" img-thumbnail img-responsive" style="" src="<?php echo $db->single("SELECT usuario_img_url FROM Usuarios WHERE usuario_nombre = '$NombrePersona' AND usuario_username = '$Username'"); ?>" alt="User Avatar"></div>
                                <div class="col-md-6 text-center" id="div_archivos_img"><input class="btn btn-sm" type="file" id="archivos_img" name="archivos_img"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left" id="msj_respuesta_update_img_usuario_inventario">
                                </div>
                                <div class="col-md-4 text-right">
                                    <button id="btn_cerrar_modal" type="button" class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm" type="button">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- ########################################################### -->
        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#btn_cerrar_modal').click(function() {
                    location.reload();
                });
            });

            $(function () {
                $('#enviar').click(SubirFotos); //Capturamos el evento click sobre el boton con el id=enviar	y ejecutamos la función seleccionado.
                //var a = $("#archivos_img").attr("src");

            });

            function SubirFotos() {
                var archivos_img = document.getElementById("archivos_img");//Creamos un objeto con el elemento que contiene los archivos_img: el campo input file, que tiene el id = 'archivos_img'
                var archivo = archivos_img.files; //Obtenemos los archivos_img seleccionados en el imput
                //Creamos una instancia del Objeto FormDara.
                var archivos_img = new FormData();
                /* Como son multiples archivos_img creamos un ciclo for que recorra la el arreglo de los archivos_img seleccionados en el input
                 Este y añadimos cada elemento al formulario FormData en forma de arreglo, utilizando la variable i (autoincremental) como 
                 indice para cada archivo, si no hacemos esto, los valores del arreglo se sobre escriben*/
                for (i = 0; i < archivo.length; i++) {
                    archivos_img.append('archivo_img' + i, archivo[i]); //Añadimos cada archivo a el arreglo con un indice direfente
                }
                $.ajax({
                    url: 'modelo/img_update_avatar_usuario_inventario.php', //Url a donde la enviaremos
                    type: 'POST', //Metodo que usaremos
                    contentType: false, //Debe estar en false para que pase el objeto sin procesar
                    data: archivos_img, //Le pasamos el objeto que creamos con los archivos_img
                    processData: false, //Debe estar en false para que JQuery no procese los datos a enviar
                    cache: false //Para que el formulario no guarde cache
                }).done(function (msg) { //Escuchamos la respuesta y capturamos el mensaje msg
                    //$("#msj_respuesta_update_img_usuario_inventario").html(msg);
                    $('#img_user').attr("src", 'img/ajax-loader.gif');
                    $("#img_user").attr("src", msg);
                });
            }
        </script>
        <script src="js/menu-active-class.js"></script>
        <!-- Colorbox -->
        <script src='js/jquery.colorbox.min.js'></script>	

        <!-- Sparkline -->
        <script src='js/jquery.sparkline.min.js'></script>

        <!-- Pace -->
        <script src='js/uncompressed/pace.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <!--<script src="js/endless/endless_dashboard.js"></script>-->
        <script src="js/endless/endless.js"></script>

    </body>
</html>
