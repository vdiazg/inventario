<?php 
/* 
    -----------------------------------------------------------------------
                            DATOS DE CONTACTO
    -----------------------------------------------------------------------
    Desarrollador: Victor Manuel Diaz Galdames.
    Nivel Profesional: Ingeniero Informático
    Número Contacto (Móvil): +5699491896
    Correo Eletrónico: victordiazgaldames@gmail.com
    Curriculum Vitae: http://victordiaz.nfconnection.cl/
    Nacionalidad: Chileno
    Descripción General: Aplicación Web, realizada con PHP5.0 y MYSQL
    -----------------------------------------------------------------------
                            DATOS DE JEFE A CARGO
    -----------------------------------------------------------------------
    Nombre: Nelson Horacio Guzman Mora.
    Número Contacto (Móvil): +56998289358
    Correo Eletrónico: nguzman@entel.cl
    Cargo: Jefe Operación Plataformas
 */
?>
<?php
session_start();
$NombrePersona = $_SESSION["datos_usuario_logueado"][0];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][1];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][2];
$TelefonoMovil = $_SESSION["datos_usuario_logueado"][3];
$EmailUsuario = $_SESSION["datos_usuario_logueado"][4];
$Username = $_SESSION["datos_usuario_logueado"][5];
$Contraseña = $_SESSION["datos_usuario_logueado"][6];
$PemisosUsuario = $_SESSION["datos_usuario_logueado"][7];
$PemisosRol = $_SESSION["datos_usuario_logueado"][8];
if (!isset($NombrePersona)) {
    header('Location: ./login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?> 
        <!-- ######################################## -->
        <style>
            .help-block {
                display: inherit !important;
            }
        </style>
    </head>

    <body style="background-color:#3a3a3a;">
        <!--Modal-->
        <form class="no-margin" name="form_lock_screen" id="form_lock_screen" method="POST">
            <div class="modal fade lock-screen-wrapper" id="lockScreen">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="lock-screen-img">
                                <img src="<?php echo $db->single("SELECT usuario_img_url FROM Usuarios WHERE usuario_nombre = '$NombrePersona' AND usuario_username = '$Username'"); ?>" alt="">
                            </div>
                            <div class="text-center m-top-sm">

                                <div class="h4 text-white"><?php echo $NombrePersona . " " . $ApellidoPaterno . " " . $ApellidoMaterno; ?></div>
                                <div class="form-group">
                                    <div class="input-group" > 
                                        <input type="password" name="contrasena_lock_screen" id="contrasena_lock_screen" class="form-control text-sm" placeholder="Ingrese su Contraseña"> 
                                        <span class="input-group-btn" style="vertical-align: -webkit-baseline-middle !important;"> 
                                            <button type="submit" class="btn btn-info"><i class="fa fa-arrow-right"></i></button>                                        
                                        </span> 
                                    </div>
                                </div>
                                <div id="msj_respuesta_lock_screen_usuario"></div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/custom_jquery_validate.js"></script>

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

        <script>
            $(function () {
                /*$('#lockScreen').popup({
                 focusdelay: 300,
                 outline: true,
                 background: false,
                 autoopen: true,
                 });*/

                $('#lockScreen').modal({
                    show: true,
                    backdrop: 'static'
                })
            });
        </script>
    </body>
</html>
